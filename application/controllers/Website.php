<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller
{
    var $system_info;

    public function __construct()
    {
        parent::__construct();

        $name = $this->db->where('info_type', 'settings_name')
            ->get('system_settings')->row('description');
        $settings_logo = $this->db->where('info_type', 'settings_logo')
            ->get('system_settings')->row('description');
        $settings_email = $this->db->where('info_type', 'settings_email')
            ->get('system_settings')->row('description');
        $settings_address = $this->db->where('info_type', 'settings_address')
            ->get('system_settings')->row('description');
        $settings_contact = $this->db->where('info_type', 'settings_contact')
            ->get('system_settings')->row('description');
        $settings_working_hours = $this->db->where('info_type', 'settings_working_hours')
            ->get('system_settings')->row('description');

        $this->system_info = array(
            'system_name' => $name,
            'system_logo' => $settings_logo,
            'system_email' => $settings_email,
            'system_address' => $settings_address,
            'system_contact' => $settings_contact,
            'system_working_hours' => $settings_working_hours,
        );

    }


    /*
     * RETURNS MASTER PAGE STRUCTURE TO EVERY PAGE
     */

    public function master($data)
    {
        return $this->load->view('frontend/index', $data);
    }

    /*
     * RETURNS INDEX PAGE
     */

    public function index()
    {
        $data = $this->system_info;


        $data['title'] = 'Home | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/dashboard_content', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS ABOUT US PAGE
     */

    public function about_us()
    {
        $data = $this->system_info;
        $data['title'] = 'About Us | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/about_us', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS OUR TEAM PAGE
     */

    public function our_team()
    {
        $data = $this->system_info;
        $data['title'] = 'Our Team | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/our_team', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS TESTIMONIAL PAGE
     */

    public function testimonials()
    {
        $data = $this->system_info;
        $data['title'] = 'Clients Testimonial | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/testimonials', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS CONTACT US PAGE
     */

    public function contact_us()
    {
        $data = $this->system_info;
        $data['title'] = 'Contact Us | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/contact_us', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS STUDY ABROAD PAGE
     * RECEIVES COUNTRY NAME PARAMETER
     * PARAMETER SHOULD BE STRING
     */

    public function study_abroad($param)
    {
        $data = $this->system_info;
        $data['country_name'] = str_replace('-',' ',$param);
        $country_id = $this->db->get_where('countries_list',array('country_name'=>$data['country_name']))->row('country_id');

        $data['info_by_country'] = $this->db->get_where('information_by_country',array('country_id'=>$country_id))->row();

        $data['title'] = 'Study in ' . $param . ' | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/study_abroad', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS OUR PRICES PAGE
     */

    public function our_prices()
    {
        $data = $this->system_info;
        $data['title'] = 'Our Prices| ';
        $data['main_content'] = $this->load->view('frontend/dynamic/our_prices', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS GALLERY PAGE
     */

    public function gallery()
    {
        $data = $this->system_info;
        $data['title'] = 'Gallery| ';

        /*
         * PAGINATION
         */
        $row = $this->db->get_where('image_gallery',array('image_type'=>2))->result();

        $count = count($row); //returns total row

        $data['total_result'] = $count;
        $data['per_page'] = 9;

        $config = array();
        $config["base_url"] = base_url() . "website/gallery/";
        $config["total_rows"] = $count;
        $config["per_page"] = $data['per_page'];

        $this->load->helper('bs_pagination_config_helper');

        $config = uk_pagination($config);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $str_links = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = explode('&nbsp;', $str_links);


        $data['images'] = $this->db->order_by('image_id','desc')->limit($config["per_page"], $page)
            ->get_where('image_gallery',array('image_type'=>2))->result();

        $data['main_content'] = $this->load->view('frontend/dynamic/gallery', $data, true);

        return $this->master($data);
    }

    /*
     * RETURNS GALLERY PAGE
     */

    public function apply_online()
    {
        $data = $this->system_info;
        $data['title'] = 'Apply Online | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/apply_online', $data, true);

        return $this->master($data);
    }

    /*
     * SAVING SUBSCRIBER TO DATABASE
     */

    public function save_subscriber()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['status'] = 1;

        $check = $this->db->get_where('subscribers', array('email' => $data['email']));

        if ($check->num_rows() > 0) {
            $this->session->set_flashdata('error', '<b>You have already Subscribed!</b>');

            redirect(base_url(), 'refresh');
        } else {
            $this->db->insert('subscribers', $data);

            $this->session->set_flashdata('success', '<b>Thanks for your Subscription!</b>');

            redirect(base_url(), 'refresh');
        }

        return false;
    }

    /*
     * SAVING CLIENT ONLINE FORM DATA
     */

    public function save_client_info()
    {
        $data['name'] = $this->input->post('name');
        $data['phone'] = $this->input->post('phone');
        $data['email'] = $this->input->post('email');
        $data['country'] = $this->input->post('country');
        $data['message'] = $this->input->post('message');

        $check = $this->db->get_where('online_application', array('email' => $data['email']));

        if ($check->num_rows() > 0) {
            $this->session->set_flashdata('error', '<b>You have already Applied on online!</b>');

            redirect(base_url(), 'refresh');
        } else {
            $this->db->insert('online_application', $data);

            $this->session->set_flashdata('success', '<b>Thanks for your Interest, we will contact with you soon!</b>');

            redirect(base_url().'apply-online', 'refresh');
        }

        return false;
    }

    /*
     * RETURNS DETAILS INFORMATION OF SLIDER IMAGE
     */
    public function details_information($caption)
    {
        $data = $this->system_info;

        $caption = str_replace('-',' ',$caption);
        $data['slider_details'] = $this->db->get_where('image_gallery',array('caption'=>$caption))->row();

        $data['title'] = $caption.' | ';
        $data['main_content'] = $this->load->view('frontend/dynamic/details_information', $data, true);

        return $this->master($data);
    }

    /*
     * SENDING MAIL TO OUR MAILING BOX
     */

    public function send_mail_from_contact_page()
    {
        $name = $this->input->post('contact-name');
        $from = $this->input->post('contact-email');
        $subject = $this->input->post('contact-subject');
        $message = $this->input->post('contact-message');

        $to = $this->db->where('info_type', 'settings_email')
            ->get('system_settings')->row('description');

        $protocol = $this->db->where('info_type', 'mail_protocol')
            ->get('system_settings')->row('description');
        $mail_path = $this->db->where('info_type', 'mail_path')
            ->get('system_settings')->row('description');
        $host = $this->db->where('info_type', 'mail_host')
            ->get('system_settings')->row('description');
        $port = $this->db->where('info_type', 'mail_host_port')
            ->get('system_settings')->row('description');
        $password = $this->db->where('info_type', 'settings_email_password')
            ->get('system_settings')->row('description');

        $this->load->helper('send_email_helper');

        $mail_status = send_mail($name, $to, $from, $subject, $message, $protocol, $mail_path, $host, $port, $password);

        if ($mail_status) {
            $this->session->set_flashdata('message', '<b>Thanks for contacting with us!</b>');
        } else {
            $this->session->set_flashdata('error', '<b>Something Went Wrong!</b>');
        }

        redirect(base_url() . 'contact-us', 'refresh');
    }
}