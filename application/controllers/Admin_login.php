<?php
/**
 * Created by PhpStorm.
 * User: Techno-71
 * Date: 10/10/2017
 * Time: 12:37 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    //Default function, redirects to logged in user area
    public function index()
    {

        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'admin_panel', 'refresh');

        $message = $this->session->flashdata('success');

        if ($message)
        {
            return $this->load->view('backend/login',$message);
        }
        return $this->load->view('backend/login');

    }

    //Ajax login function
    function ajax_login() {
        $response = array();

        //Recieving post input of email, password from ajax request
        $email = $_POST["email"];
        $password = $_POST["password"];

        //Validating login
        $login_status = $this->validate_login($email, $password);

        if ($login_status == 'success') {
            $this->session->set_flashdata('success', 'Welcome to Admin Panel!');
            redirect('admin_panel','refresh');
        }
        else{
            $this->session->set_flashdata('error', 'Invalid login credentials. Please Try again!');
            redirect('admin_login','refresh');
        }

    }

    //Validating login from ajax request
    function validate_login($email = '', $password = '') {
        $credential = array('email' => $email, 'password' => $password);


        // Checking login credential for admin
        $query = $this->db->get_where('admin_staff', $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->session->set_userdata('admin_login', '1');
            $this->session->set_userdata('admin_id', $row->admin_staff_id);
            $this->session->set_userdata('admin_name', $row->name);
            $this->session->set_userdata('image', $row->image);

            $this->session->set_userdata('login_type', 'admin');
            return 'success';
        }

        return 'invalid';
    }

    function logout() {
        $this->session->unset_userdata('admin_login');
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_name');
        $this->session->unset_userdata('login_type');
        $this->session->unset_userdata('image');
        $s_data = array();
        $s_data['success'] = 'Successfully logged out from system!';
        $this->session->set_flashdata($s_data);

        redirect('admin_login','refresh');
    }
}