<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_panel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('admin_id') == NULL) {
            redirect('admin_login', 'refresh');
        }

        $id = $this->session->userdata('admin_id');

        $module_list = $this->db->order_by('module_id', 'asc')->get_where('access_control', array('admin_staff_id' => $id))->result_array();

        if (count($module_list) > 0) {
            global $admin_module;
            $admin_module = $module_list[0]['module_access'];

            global $client_module;
            $client_module = $module_list[1]['module_access'];

            global $accounting_module;
            $accounting_module = $module_list[2]['module_access'];

            global $salary_module;
            $salary_module = $module_list[3]['module_access'];

            global $attendance_module;
            $attendance_module = $module_list[4]['module_access'];

            global $employee_module;
            $employee_module = $module_list[5]['module_access'];
        }

    }

//    public $this->admin_module;


    function index()
    {

        $data = array();
        $data['page_name'] = 'Admin Dashboard';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/dashboard_content', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function module_list()
    {
        if (isset($admin_module)) {
            global $admin_module;

            if ($admin_module != 1) {
                redirect('admin_panel');
            }
        }

        $data = array();
        $data['page_name'] = 'MODULE LIST';
        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/acl/module_list', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function access_control()
    {


        $data = array();
        $data['page_name'] = 'ACCESS CONTROL';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/acl/access_control', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function manage_access_control($param = '', $param2 = '')
    {
        if (isset($admin_module)) {
            global $admin_module;

            if ($admin_module != 1) {
                redirect('admin_panel');
            }
        }

        $data = array();

        if ($param == 'manage_access') {
            $id = $this->input->post('admin_staff_id');

            $access_info_by_user = $this->db->get_where('access_control', array('admin_staff_id' => $id))->result();


            $modules = $this->db->order_by('module_id', 'asc')
                ->get_where('module_list')->result();
            $count = count($modules);

            if (!$access_info_by_user) {

                $data = array();

                for ($i = 0; $i < $count; $i++) {
                    $data[$i] = array(
                        'admin_staff_id' => $id,
                        'module_id' => $i + 1,
                        'module_access' => 0,
                    );

                }

                $this->db->insert_batch('access_control', $data);

            }


            $data['admin_name'] = $this->db->get_where('admin_staff', array('admin_staff_id' => $id))->row('name');
            $data['admin_id'] = $id;

        }


        $data['page_name'] = 'ACCESS CONTROL';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/acl/manage_access_control', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function change_access_settings()
    {
        if (isset($admin_module)) {
            global $admin_module;

            if ($admin_module != 1) {
                redirect('admin_panel');
            }
        }

        $data['module_access'] = $_POST['module_access'];
        $this->db->where('control_id', $_POST['control_id']);
        $this->db->update('access_control', $data);

        echo $_POST['control_id'];
    }

    /** Image Upload **/
    function do_upload()
    {

        if (file_exists($_FILES['image']['tmp_name'])) {

            $this->load->library('image_lib');
            $config['upload_path'] = './uploads/users_image/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1024;
            $config['max_width'] = 1600;
            $config['max_height'] = 1600;
            $config['maintain_ratio'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('error', $error['error']);

                redirect(base_url() . 'admin_panel/system_users', 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        }
        return false;


    }

    function agents_image()
    {

        if (file_exists($_FILES['image']['tmp_name'])) {

            $this->load->library('image_lib');
            $config['upload_path'] = './uploads/agents/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1024;
            $config['max_width'] = 1600;
            $config['max_height'] = 1600;
            $config['maintain_ratio'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('error', $error['error']);

                redirect(base_url() . 'admin_panel/agents', 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        }
        return false;


    }

    /**! Image Upload **/

    function system_users($param = '', $param2 = '')
    {
        if (isset($admin_module)) {
            global $admin_module;

            if ($admin_module != 1) {
                redirect('admin_panel');
            }
        }

        $data = array();

        if ($param == 'create') {
            $data['name'] = $this->input->post('name');

            $data['image'] = $this->do_upload();
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['access_level'] = $this->input->post('access_level');
            $data['status'] = $this->input->post('status');

            $this->db->insert('admin_staff', $data);

            $this->session->set_flashdata('success', '<b>New User Added to this system</b>');
            redirect(base_url() . 'admin_panel/system_users', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('admin_staff_id', $param2);
            $this->db->delete('admin_staff');

            $this->session->set_flashdata('error', '<b>System User deleted!</b>');

            redirect(base_url() . 'admin_panel/system_users', 'refresh');
        } elseif ($param == 'do_update') {
            $data['name'] = $this->input->post('name');


            if (file_exists($_FILES['image']['tmp_name'])) {
                $data['image'] = $this->do_upload();
            }

            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['access_level'] = $this->input->post('access_level');
            $data['status'] = $this->input->post('status');

            $this->db->where('admin_staff_id', $param2);
            $this->db->update('admin_staff', $data);

            $this->session->set_flashdata('success', '<b>Users information updated!</b>');

            redirect(base_url() . 'admin_panel/system_users', 'refresh');
        }

        $module_name = 'admin_module';

        $data['admin_module'] = $this->db->get_where('module_list', array('module_name' => $module_name))->row('module_id');

        $data['page_name'] = 'SYSTEM USERS';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/system_users', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function get_emp_info($id)
    {
        $info = $this->db->get_where('employees', array('employee_id' => $id))->row();

        echo json_encode($info);
    }

    /*
     * AGENTS
     */

    public function agents($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();


        if ($param == 'create') {
            $data['name'] = $this->input->post('name');

            $data['image'] = $this->agents_image();
            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('phone');
            $data['code'] = $this->input->post('code');
            $data['status'] = $this->input->post('status');

            $this->db->insert('agents', $data);

            $this->session->set_flashdata('success', '<b>New Agent Added to this system</b>');
            redirect(base_url() . 'admin_panel/agents', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('agents');

            $this->session->set_flashdata('error', '<b>Agent Information deleted!</b>');

            redirect(base_url() . 'admin_panel/agents', 'refresh');
        } elseif ($param == 'do_update') {
            $data['name'] = $this->input->post('name');


            if (file_exists($_FILES['image']['tmp_name'])) {
                $data['image'] = $this->agents_image();
            }

            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('phone');
            $data['code'] = $this->input->post('code');
            $data['status'] = $this->input->post('status');

            $this->db->where('id', $param2);
            $this->db->update('agents', $data);

            $this->session->set_flashdata('success', '<b>Agent information updated!</b>');

            redirect(base_url() . 'admin_panel/agents', 'refresh');
        }


        $data['page_name'] = 'AGENTS';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/agents', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     *
            select COUNT(client_id), country
            from clients
            where agent_status = 1 AND added_by = 1
            GROUP BY country
            ORDER BY COUNT(client_id) DESC;
     */
    public function agents_country($id)
    {
        $data = array();

        $data['page_name'] = 'AGENT: ' . $this->db->get_where('agents', array('id' => $id))->row('name');

        $data['info_country'] = $this->db->select('count(client_id) as country_no')->select('country')
            ->where('agent_status', 1)->where('added_by', $id)
            ->group_by('country')
            ->order_by('country_no', 'desc')->get('clients')->result();

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/agents_country', $data, true);

        return $this->load->view('backend/index', $data);

    }

    /*
     * CLIENT MODULES
     */

    function client_type($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        if ($param == 'create') {
            $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('client_type', $data);

            $this->session->set_flashdata('success', '<b>New client type to this system</b>');
            redirect(base_url() . 'admin_panel/client_type', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('client_type');

            $this->session->set_flashdata('error', '<b>Client Type deleted!</b>');

            redirect(base_url() . 'admin_panel/client_type', 'refresh');
        } elseif ($param == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            $data['status'] = $this->input->post('status');

            $this->db->where('id', $param2);
            $this->db->update('client_type', $data);

            $this->session->set_flashdata('success', '<b>Client Type information updated!</b>');

            redirect(base_url() . 'admin_panel/client_type', 'refresh');
        }

        $data['page_name'] = 'CLIENT TYPE';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/client_type', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function visa_type($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        if ($param == 'create') {
            $data['visa_name'] = $this->input->post('visa_name');
            $data['visa_description'] = $this->input->post('visa_description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('visa_type', $data);

            $this->session->set_flashdata('success', '<b>New Visa type to this system</b>');
            redirect(base_url() . 'admin_panel/visa_type', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('visa_type_id', $param2);
            $this->db->delete('visa_type');

            $this->session->set_flashdata('error', '<b>Visa Type deleted!</b>');

            redirect(base_url() . 'admin_panel/client_type', 'refresh');
        } elseif ($param == 'do_update') {
            $data['visa_name'] = $this->input->post('visa_name');
            $data['visa_description'] = $this->input->post('visa_description');
            $data['status'] = $this->input->post('status');

            $this->db->where('visa_type_id', $param2);
            $this->db->update('visa_type', $data);

            $this->session->set_flashdata('success', '<b>Visa Type information updated!</b>');

            redirect(base_url() . 'admin_panel/visa_type', 'refresh');
        }

        $data['page_name'] = 'VISA TYPE';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/visa_type', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function clients($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        if ($param == 'create') {
            $data['client_type'] = $this->input->post('client_type');
            $data['client_name'] = $this->input->post('client_name');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['profession'] = $this->input->post('profession');
            $data['passport_status'] = $this->input->post('passport_status');
            $data['visa_type'] = $this->input->post('visa_type');
            $data['country'] = $this->input->post('country');
            $data['sex'] = $this->input->post('sex');
            $data['activity_status'] = $this->input->post('activity_status');

            date_default_timezone_set('Asia/Dhaka');
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['modify_date'] = date('Y-m-d H:i:s');

            $data['agent_status'] = $this->input->post('agent_status');
            $data['added_by'] = $this->input->post('added_by');

            $this->db->insert('clients', $data);

            $this->session->set_flashdata('success', '<b>New Client added to this system</b>');
            redirect(base_url() . 'admin_panel/clients', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('client_id', $param2);
            $this->db->delete('clients');

            $doc = $this->db->get_where('client_document', array('client_id' => $param2));
            if ($doc->num_rows() > 0) {
                $this->db->where('client_id', $param2);
                $this->db->delete('client_document');
            }

            $contract = $this->db->get_where('client_contract', array('client_id' => $param2));

            if ($contract->num_rows() > 0) {
                $this->db->where('client_id', $param2);
                $this->db->delete('client_contract');
            }

            $bill = $this->db->get_where('client_billing', array('client_id' => $param2));

            if ($bill->num_rows() > 0) {
                $this->db->where('client_id', $param2);
                $this->db->delete('client_billing');
            }

            $this->session->set_flashdata('error', '<b>Client\' All Info deleted!</b>');

            redirect(base_url() . 'admin_panel/clients', 'refresh');
        } elseif ($param == 'do_update') {
            $data['client_type'] = $this->input->post('client_type');
            $data['client_name'] = $this->input->post('client_name');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['profession'] = $this->input->post('profession');
            $data['passport_status'] = $this->input->post('passport_status');
            $data['visa_type'] = $this->input->post('visa_type');
            $data['country'] = $this->input->post('country');
            $data['sex'] = $this->input->post('sex');
            $data['activity_status'] = $this->input->post('activity_status');

            date_default_timezone_set('Asia/Dhaka');
            $data['modify_date'] = date('Y-m-d H:i:s');

            $data['agent_status'] = $this->input->post('agent_status');
            $data['added_by'] = $this->input->post('added_by');

            $this->db->where('client_id', $param2);
            $this->db->update('clients', $data);

            $this->session->set_flashdata('success', '<b>Client information updated!</b>');

            redirect(base_url() . 'admin_panel/clients', 'refresh');
        }

        $data['page_name'] = 'CLIENT MANAGEMENT';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/clients', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function company_clients($param = '', $param2 = '')
    {
        if ($param == 'create') {
            $data['name'] = $this->input->post('name');
            $data['client_type'] = $this->input->post('client_type');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['date'] = $this->input->post('date');

            $entries = array();
            $new_entry = array();

            $title = $this->input->post('input_title');
            $description = $this->input->post('input_description');
            $number_of_entries = sizeof($title);

            for ($i = 0; $i < $number_of_entries; $i++) {
                if ($title[$i] != '' && $description[$i] != '') {

                    $new_entry = array(
                        'title' => $title[$i],
                        'description' => $description[$i]
                    );

                    array_push($entries, $new_entry);
                }

            }

            $data['description'] = json_encode($entries);

            $this->db->insert('company_clients', $data);

            $this->session->set_flashdata('success', '<b>Client Information Saved to this system</b>');
            redirect(base_url() . 'admin_panel/company_clients', 'refresh');
        }

        elseif($param == 'delete')
        {
            $this->db->where('c_client_id', $param2);
            $this->db->delete('company_clients');

            $this->session->set_flashdata('error', '<b>Company Client Information deleted!</b>');

            redirect(base_url() . 'admin_panel/client_list', 'refresh');
        }

        elseif ($param == 'do_update')
        {
            $data['name'] = $this->input->post('name');
            $data['client_type'] = $this->input->post('client_type');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['date'] = $this->input->post('date');

            $entries = array();
            $new_entry = array();

            $title = $this->input->post('input_title');
            $description = $this->input->post('input_description');
            $number_of_entries = sizeof($title);

            for ($i = 0; $i < $number_of_entries; $i++) {
                if ($title[$i] != '' && $description[$i] != '') {

                    $new_entry = array(
                        'title' => $title[$i],
                        'description' => $description[$i]
                    );

                    array_push($entries, $new_entry);
                }

            }

            $data['description'] = json_encode($entries);

            $this->db->where('c_client_id', $param2);
            $this->db->update('company_clients', $data);

            $this->session->set_flashdata('success', '<b>Clients information updated!</b>');

            redirect(base_url() . 'admin_panel/client_list', 'refresh');
        }
        $data['page_name'] = 'COMPANY CLIENT';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/company_clients', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function client_list()
    {
        $data['page_name'] = 'CLIENT LIST';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/client_list', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function profile($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['page_name'] = 'CLIENT Profile';
        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/profile', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function get_emp_data($status)
    {
        if ($status == 0) {
            $result = $this->db->get_where('employees', array('employee_status' => 1))->result();

            echo json_encode($result);
        } else {
            $result = $this->db->get_where('agents', array('status' => 1))->result();

            echo json_encode($result);
        }
    }

    function document_process($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        if ($param == 'create') {
            $data['process_name'] = $this->input->post('process_name');
            $data['status'] = $this->input->post('status');
            $this->db->insert('document_process', $data);

            $this->session->set_flashdata('success', '<b>Process Information to this system</b>');
            redirect(base_url() . 'admin_panel/document_process', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('process_id', $param2);
            $this->db->delete('document_process');

            $this->session->set_flashdata('error', '<b>Process Information deleted!</b>');

            redirect(base_url() . 'admin_panel/document_process', 'refresh');
        } elseif ($param == 'do_update') {
            $data['process_name'] = $this->input->post('process_name');
            $data['status'] = $this->input->post('status');

            $this->db->where('process_id', $param2);
            $this->db->update('document_process', $data);

            $this->session->set_flashdata('success', '<b>Process Information updated!</b>');

            redirect(base_url() . 'admin_panel/document_process', 'refresh');
        }

        $data['page_name'] = 'DOCUMENT PROCESS';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/document_process', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function collect_documents($id, $param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        $data['client_id'] = $id;

        if ($param == 'create') {
            $data['client_id'] = $id;
            $data['client_visit_place'] = $this->input->post('client_visit_place');
            $data['client_code'] = $this->input->post('client_code');

            if (file_exists($_FILES['formal_image']['tmp_name'])) {
                $data['client_image'] = $this->formal_image($id);
            }

            $data['document_status'] = $this->input->post('document_status');
            $data['reviewed_by'] = $this->session->userdata('admin_id');

            date_default_timezone_set('Asia/Dhaka');
            $data['last_update'] = date('Y-m-d H:i:s');

            $this->db->insert('client_document', $data);

            $this->db->set('activity_status', 'document process');
            $this->db->where('client_id', $id);
            $this->db->update('clients');

            $this->session->set_flashdata('success', '<b>Clients Document Has been Received</b>');
            redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');
        } elseif ($param == 'do_update') {

            $data['client_visit_place'] = $this->input->post('client_visit_place');
            $data['client_code'] = $this->input->post('client_code');

            if (file_exists($_FILES['formal_image']['tmp_name'])) {
                $data['client_image'] = $this->formal_image($id);
            }

            $data['document_status'] = $this->input->post('document_status');
            date_default_timezone_set('Asia/Dhaka');
            $data['last_update'] = date('Y-m-d H:i:s');

            $document_id = $this->db->get_where('client_document', array('client_id' => $id))->row('document_id');

            $this->db->where('document_id', $document_id);
            $this->db->update('client_document', $data);

            $this->session->set_flashdata('success', '<b>Document Information updated!</b>');

            redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');
        }

        $data['page_name'] = 'COLLECT DOCUMENT';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/collect_document', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function store_document($id='',$param='')
    {

        if ($param == 'passport_visa')
        {
            if (file_exists($_FILES['passport']['tmp_name'])) {
                $data['client_passport'] = $this->passport($id);
            }
            if (file_exists($_FILES['visa']['tmp_name'])) {
                $data['client_visa'] = $this->visa($id);
            }
        }
        elseif ($param == 'transcript')
        {
            if (file_exists($_FILES['file']['tmp_name'])) {
                $data['academic_transcript'] = $this->academic_transcript($id);
            }
        }
        elseif ($param == 'medical')
        {
            if (file_exists($_FILES['medical']['tmp_name'])) {
                $data['medical_report'] = $this->medical_report($id);
            }
        }
        elseif ($param == 'sponsor')
        {
            if (file_exists($_FILES['sponsor']['tmp_name'])) {
                $data['sponsor_document'] = $this->sponsor_document($id);
            }
        }


        date_default_timezone_set('Asia/Dhaka');
        $data['last_update'] = date('Y-m-d H:i:s');

        $document_id = $this->db->get_where('client_document', array('client_id' => $id))->row('document_id');

        $this->db->where('document_id', $document_id);
        $this->db->update('client_document', $data);

        $this->db->set('activity_status', 'document process');
        $this->db->where('client_id', $id);
        $this->db->update('clients');

        $this->session->set_flashdata('success', '<b>Document Information updated!</b>');

        redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');
    }

    /** FORMAL IMAGE Upload **/
    function formal_image($id)
    {

        if (file_exists($_FILES['formal_image']['tmp_name'])) {

            $this->load->library('image_lib');
            $config['upload_path'] = './uploads/clients/formal_image/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['max_width'] = 1600;
            $config['max_height'] = 1600;
            $config['maintain_ratio'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('formal_image')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('error', $error['error']);

                redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        } else {
            return false;
        }


    }

    /** PASSPORT Upload **/
    function passport($id)
    {

        if (file_exists($_FILES['passport']['tmp_name'])) {

            $this->load->library('image_lib');
            $config['upload_path'] = './uploads/clients/passport/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['max_width'] = 1600;
            $config['max_height'] = 1600;
            $config['maintain_ratio'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('passport')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('error', $error['error']);

                redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        } else {
            return false;
        }


    }

    /** VISA Upload **/
    function visa($id)
    {

        if (file_exists($_FILES['visa']['tmp_name'])) {

            $this->load->library('image_lib');
            $config['upload_path'] = './uploads/clients/visa/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['max_width'] = 1600;
            $config['max_height'] = 1600;
            $config['maintain_ratio'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('visa')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('error', $error['error']);

                redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        } else {
            return false;
        }


    }

    /** ACADEMIC TRANSCRIPT Upload **/
    function academic_transcript($id)
    {
        if (file_exists($_FILES['file']['tmp_name'])) {

            $config['upload_path'] = './uploads/clients/academic_transcript/';
            $config['allowed_types'] = 'xlsx|docx|doc|pdf|JPG|JPEG|PNG';
            $config['max_size'] = 5000;

            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('message', $error['error']);
                redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        }
        return false;
    }

    /** Medical Report Upload **/
    function medical_report($id)
    {
        if (file_exists($_FILES['medical']['tmp_name'])) {

            $config['upload_path'] = './uploads/clients/medical/';
            $config['allowed_types'] = 'xlsx|docx|doc|pdf|JPG|JPEG|PNG';
            $config['max_size'] = 5000;

            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('medical')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('message', $error['error']);
                redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        }
        return false;
    }

    /** Sponsor Document Upload **/

    function sponsor_document($id)
    {
        if (file_exists($_FILES['sponsor']['tmp_name'])) {

            $config['upload_path'] = './uploads/clients/sponsor/';
            $config['allowed_types'] = 'xlsx|docx|doc|pdf|JPG|JPEG|PNG';
            $config['max_size'] = 5000;

            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('sponsor')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('message', $error['error']);
                redirect(base_url() . 'admin_panel/collect_documents/' . $id, 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        }
        return false;
    }


    /*
     * Document Check Function
     */

    function document_check($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        if ($param == 'delete') {

            $this->db->where('document_id', $param2);
            $this->db->delete('client_document');

            $this->session->set_flashdata('success', '<b>Clients Document Has been Received</b>');
            redirect(base_url() . 'admin_panel/document_check/', 'refresh');
        }

        $data['page_name'] = 'DOCUMENT CHECK';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/document_check', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function client_profile($id)
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data['page_name'] = 'CLIENT PROFILE';

        $data['client_data'] = $this->db->join('clients c', 'c.client_id = cd.client_id')
            ->get_where('client_document cd', array('cd.client_id' => $id))->row();

        if ($data['client_data'] == NULL) {
            redirect(base_url() . 'admin_panel/collect_documents/' . $id);
        }

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/client_profile', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function cash_memo_type($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        if ($param == 'create') {
            $data['m_t_title'] = $this->input->post('m_t_title');
            $data['m_t_description'] = $this->input->post('m_t_description');
            $data['m_t_tk'] = $this->input->post('m_t_tk');
            $data['m_t_status'] = $this->input->post('m_t_status');

            $this->db->insert('cash_memo_type', $data);

            $this->session->set_flashdata('success', '<b>New Cash Memo type Added to this system</b>');
            redirect(base_url() . 'admin_panel/cash_memo_type', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('m_t_id', $param2);
            $this->db->delete('cash_memo_type');

            $this->session->set_flashdata('error', '<b>Cash Memo Type deleted!</b>');

            redirect(base_url() . 'admin_panel/cash_memo_type', 'refresh');
        } elseif ($param == 'do_update') {
            $data['m_t_title'] = $this->input->post('m_t_title');
            $data['m_t_description'] = $this->input->post('m_t_description');
            $data['m_t_tk'] = $this->input->post('m_t_tk');
            $data['m_t_status'] = $this->input->post('m_t_status');

            $this->db->where('m_t_id', $param2);
            $this->db->update('cash_memo_type', $data);

            $this->session->set_flashdata('success', '<b>Cash Memo Type information updated!</b>');

            redirect(base_url() . 'admin_panel/cash_memo_type', 'refresh');
        }

        $data['page_name'] = 'CASH MEMO TYPE';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/cash_memo_type', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function client_contract($id = NULL, $param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        $data['id'] = $id;

        if ($param == 'create') {

            $con['contract_amount'] = $this->input->post('contract_amount');
            $con['contract_description'] = $this->input->post('contract_description');
            $con['client_id'] = $this->input->post('client_id');
            $con['consultant_id'] = $this->input->post('consultant_id');

            date_default_timezone_set('Asia/Dhaka');
            $con['contract_date'] = date('Y-m-d H:i:s');

            $this->db->insert('client_contract', $con);

            $this->db->where('client_id', $id)->update('clients', array('contract_status' => 1));

            $this->session->set_flashdata('success', '<b>Client Contract Signed Successfully!</b>');
            redirect(base_url() . 'admin_panel/client_contract', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('client_id', $param2);
            $this->db->delete('client_contract');

            $this->session->set_flashdata('error', '<b>Client Contract deleted!</b>');

            redirect(base_url() . 'admin_panel/client_contract', 'refresh');
        } elseif ($param == 'do_update') {
            $con['contract_amount'] = $this->input->post('contract_amount');
            $con['contract_description'] = $this->input->post('contract_description');

            $this->db->where('client_id', $param2);
            $this->db->update('client_contract', $con);

            $this->session->set_flashdata('success', '<b>Client Contract information updated!</b>');

            redirect(base_url() . 'admin_panel/client_contract', 'refresh');
        }

        $data['page_name'] = 'CLIENT CONTRACT';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/client_contract', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function cash_memo($param = '', $param2 = '')
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }

        $data = array();

        if ($param == 'create') {
            $data['client_id'] = $this->input->post('client_id');
            $data['bill_code'] = $this->input->post('bill_code');
            $data['bill_type'] = $this->input->post('bill_type');
            $data['bill_amount'] = $this->input->post('bill_amount');
            $data['bill_status'] = $this->input->post('bill_status');

            date_default_timezone_set('Asia/Dhaka');
            $data['bill_date'] = date('Y-m-d H:i:s');

            $data['billed_by'] = $this->session->userdata('admin_id');

            $this->db->insert('client_billing', $data);

            $this->session->set_flashdata('success', '<b>New Cash Memo type Added to this system</b>');
            redirect(base_url() . 'admin_panel/cash_memo', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('billing_id', $param2);
            $this->db->delete('client_billing');

            $this->session->set_flashdata('error', '<b>Cash Memo Type deleted!</b>');

            redirect(base_url() . 'admin_panel/cash_memo_type', 'refresh');
        } elseif ($param == 'do_update') {
            $data['client_id'] = $this->input->post('client_id');
            $data['bill_code'] = $this->input->post('bill_code');
            $data['bill_type'] = $this->input->post('bill_type');
            $data['bill_amount'] = $this->input->post('bill_amount');
            $data['bill_status'] = $this->input->post('bill_status');

            $this->db->where('billing_id', $param2);
            $this->db->update('client_billing', $data);

            $this->session->set_flashdata('success', '<b>Cash Memo Type information updated!</b>');

            redirect(base_url() . 'admin_panel/cash_memo', 'refresh');
        }

        $data['page_name'] = 'CASH MEMO';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/cash_memo', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function print_cash_memo($billing_id)
    {
        global $client_module;
        if ($client_module != 1) {
            redirect('admin_panel');
        }


        $data['bill_info'] = $this->db->get_where('client_billing', array('billing_id' => $billing_id))->row();

        return $this->load->view('backend/dynamic_files/print_cash_memo', $data);
    }

    /*
     * ACCOUNTING MODULE
     */

    /***INCOME MANAGEMENT***/
    function income_category($param1 = '', $param2 = '')
    {
        global $accounting_module;
        if ($accounting_module != 1) {
            redirect('admin_panel');
        }


        if ($param1 == 'create') {
            $data['income_cat_name'] = $this->input->post('income_cat_name');
            $data['income_cat_description'] = $this->input->post('income_cat_description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('income_category', $data);
            $this->session->set_flashdata('success', '<b>Income category information saved!</b>');
            redirect(base_url() . 'admin_panel/income_category', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['income_cat_name'] = $this->input->post('income_cat_name');
            $data['income_cat_description'] = $this->input->post('income_cat_description');
            $data['status'] = $this->input->post('status');

            $this->db->where('income_cat_id', $param2);
            $this->db->update('income_category', $data);
            $this->session->set_flashdata('success', '<b>Income Category information updated!</b>');
            redirect(base_url() . 'admin_panel/income_category', 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('income_cat_id', $param2);
            $this->db->delete('income_category');
            $this->session->set_flashdata('error', '<b>Income Category information deleted!</b>');
            redirect(base_url() . 'admin_panel/income_category', 'refresh');
        }

        $data['page_name'] = 'INCOME CATEGORY';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/income_category', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function income_record($param1 = '', $param2 = '')
    {
        global $accounting_module;
        if ($accounting_module != 1) {
            redirect('admin_panel');
        }


        if ($param1 == 'create') {
            $data['income_title'] = $this->input->post('income_title');
            $data['income_description'] = $this->input->post('income_description');
            $data['income_amount'] = $this->input->post('income_amount');
            $data['income_method'] = $this->input->post('income_method');
            $data['cheque_no'] = $this->input->post('cheque_no');
            $data['income_date'] = strtotime($this->input->post('income_date'));
            $data['income_status'] = $this->input->post('income_status');

            $this->db->insert('income', $data);
            $this->session->set_flashdata('success', '<b>Income information saved!</b>');
            redirect(base_url() . 'admin_panel/income_record', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['income_title'] = $this->input->post('income_title');
            $data['income_description'] = $this->input->post('income_description');
            $data['income_amount'] = $this->input->post('income_amount');
            $data['income_method'] = $this->input->post('income_method');
            $data['cheque_no'] = $this->input->post('cheque_no');
            $data['income_date'] = strtotime($this->input->post('income_date'));
            $data['income_status'] = $this->input->post('income_status');

            $this->db->where('income_id', $param2);
            $this->db->update('income', $data);
            $this->session->set_flashdata('success', '<b>Income information updated!</b>');
            redirect(base_url() . 'admin_panel/income_record', 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('income_id', $param2);
            $this->db->delete('income');
            $this->session->set_flashdata('error', '<b>Income information deleted!</b>');
            redirect(base_url() . 'admin_panel/income_record', 'refresh');
        }

        $data['page_name'] = 'INCOME RECORD';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/income', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /***EXPENSE MANAGEMENT***/
    function expense_category($param1 = '', $param2 = '')
    {
        global $accounting_module;
        if ($accounting_module != 1) {
            redirect('admin_panel');
        }


        if ($param1 == 'create') {
            $data['expense_cat_name'] = $this->input->post('expense_cat_name');
            $data['expense_cat_description'] = $this->input->post('expense_cat_description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('expense_category', $data);
            $this->session->set_flashdata('success', '<b>Expense category information saved!</b>');
            redirect(base_url() . 'admin_panel/expense_category', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['expense_cat_name'] = $this->input->post('expense_cat_name');
            $data['expense_cat_description'] = $this->input->post('expense_cat_description');
            $data['status'] = $this->input->post('status');

            $this->db->where('expense_cat_id', $param2);
            $this->db->update('expense_category', $data);
            $this->session->set_flashdata('success', '<b>Expense Category information updated!</b>');
            redirect(base_url() . 'admin_panel/expense_category', 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('expense_cat_id', $param2);
            $this->db->delete('expense_category');
            $this->session->set_flashdata('error', '<b>Expense Category information deleted!</b>');
            redirect(base_url() . 'admin_panel/expense_category', 'refresh');
        }

        $data['page_name'] = 'EXPENSE CATEGORY';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/expense_category', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function expense_record($param1 = '', $param2 = '')
    {
        global $accounting_module;
        if ($accounting_module != 1) {
            redirect('admin_panel');
        }


        if ($param1 == 'create') {
            $data['expense_title'] = $this->input->post('expense_title');
            $data['expense_description'] = $this->input->post('expense_description');
            $data['expense_amount'] = $this->input->post('expense_amount');
            $data['expense_method'] = $this->input->post('expense_method');
            $data['cheque_no'] = $this->input->post('cheque_no');
            $data['expense_date'] = strtotime($this->input->post('expense_date'));
            $data['expense_status'] = $this->input->post('expense_status');

            $this->db->insert('expense', $data);
            $this->session->set_flashdata('success', '<b>Expense information saved!</b>');
            redirect(base_url() . 'admin_panel/expense_record', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['expense_title'] = $this->input->post('expense_title');
            $data['expense_description'] = $this->input->post('expense_description');
            $data['expense_amount'] = $this->input->post('expense_amount');
            $data['expense_method'] = $this->input->post('expense_method');
            $data['cheque_no'] = $this->input->post('cheque_no');
            $data['expense_date'] = strtotime($this->input->post('expense_date'));
            $data['expense_status'] = $this->input->post('expense_status');

            $this->db->where('expense_id', $param2);
            $this->db->update('expense', $data);
            $this->session->set_flashdata('success', '<b>Expense information updated!</b>');
            redirect(base_url() . 'admin_panel/expense_record', 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('expense_id', $param2);
            $this->db->delete('expense');
            $this->session->set_flashdata('error', '<b>Expense information deleted!</b>');
            redirect(base_url() . 'admin_panel/expense_record', 'refresh');
        }

        $data['page_name'] = 'EXPENSE RECORD';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/expense', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function transaction_report($date1 = '', $date2 = '')
    {
        global $accounting_module;
        if ($accounting_module != 1) {
            redirect('admin_panel');
        }


        $data['page_name'] = 'TRANSACTION REPORT';

        $data['date1'] = $date1;
        $data['date2'] = $date2;

        if ($_GET) {

            $date1 = $this->input->get('date1');
            $date2 = $this->input->get('date2');

            $income_data = $this->db->where('income_date >=', strtotime($date1))
                ->where('income_date <=', strtotime($date2))
                ->order_by('income_date', 'desc')
                ->select('ic.income_cat_name, i.*')
                ->join('income_category ic', 'ic.income_cat_id = i.income_title')
                ->get('income i')->result();

            $expense_date = $this->db->where('expense_date >=', strtotime($date1))
                ->where('expense_date <=', strtotime($date2))
                ->order_by('expense_date', 'desc')
                ->select('ec.expense_cat_name, e.*')
                ->join('expense_category ec', 'ec.expense_cat_id = e.expense_title')
                ->get('expense e')->result();

            $data['total_income'] = $this->db->where('income_date >=', strtotime($date1))
                ->where('income_date <=', strtotime($date2))
                ->select_sum('income_amount')
                ->get('income')->result();

            $data['total_expense'] = $this->db->where('expense_date >=', strtotime($date1))
                ->where('expense_date <=', strtotime($date2))
                ->select_sum('expense_amount')
                ->get('expense')->result();

            $data['income'] = $income_data;
            $data['expense'] = $expense_date;

            $data['date1'] = $date1;
            $data['date2'] = $date2;

        }


        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/transaction_report', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function employee_designation($param1 = '', $param2 = '')
    {
        global $employee_module;
        if ($employee_module != 1) {
            redirect('admin_panel');
        }


        if ($param1 == 'create') {
            $data['designation_name'] = $this->input->post('designation_name');
            $data['role_description'] = $this->input->post('role_description');
            $data['visibility_status'] = $this->input->post('visibility_status');

            $this->db->insert('employee_designation', $data);
            $this->session->set_flashdata('success', '<b>Employee Designation information saved!</b>');
            redirect(base_url() . 'admin_panel/employee_designation', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['designation_name'] = $this->input->post('designation_name');
            $data['role_description'] = $this->input->post('role_description');
            $data['visibility_status'] = $this->input->post('visibility_status');

            $this->db->where('designation_id', $param2);
            $this->db->update('employee_designation', $data);
            $this->session->set_flashdata('success', '<b>Employee Designation information updated!</b>');
            redirect(base_url() . 'admin_panel/employee_designation', 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('designation_id', $param2);
            $this->db->delete('employee_designation');
            $this->session->set_flashdata('error', '<b>Employee Designation information deleted!</b>');
            redirect(base_url() . 'admin_panel/employee_designation', 'refresh');
        }

        $data['page_name'] = 'DESIGNATION';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/employee_designation', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function employees($param1 = '', $param2 = '')
    {
        global $employee_module;
        if ($employee_module != 1) {
            redirect('admin_panel');
        }


        if ($param1 == 'create') {
            $data['employee_name'] = $this->input->post('employee_name');
            $data['designation'] = $this->input->post('designation');
            $data['email'] = $this->input->post('email');
            $data['join_date'] = strtotime($this->input->post('join_date'));
            $data['phone'] = $this->input->post('phone');
            $data['employee_code'] = $this->input->post('employee_code');
            $data['employee_status'] = $this->input->post('employee_status');

            $this->db->insert('employees', $data);
            $this->session->set_flashdata('success', '<b>Employee information saved!</b>');
            redirect(base_url() . 'admin_panel/employees', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['employee_name'] = $this->input->post('employee_name');
            $data['designation'] = $this->input->post('designation');
            $data['email'] = $this->input->post('email');
            $data['join_date'] = strtotime($this->input->post('join_date'));
            $data['phone'] = $this->input->post('phone');
            $data['employee_code'] = $this->input->post('employee_code');
            $data['employee_status'] = $this->input->post('employee_status');

            $this->db->where('employee_id', $param2);
            $this->db->update('employees', $data);
            $this->session->set_flashdata('success', '<b>Employee information updated!</b>');
            redirect(base_url() . 'admin_panel/employees', 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('employee_id', $param2);
            $this->db->delete('employees');
            $this->session->set_flashdata('error', '<b>Employee information deleted!</b>');
            redirect(base_url() . 'admin_panel/employees', 'refresh');
        }

        $data['page_name'] = 'EMPLOYEES';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/employees', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * EMPLOYEE ATTENDANCE
     */

    /****MANAGE EMPLOYEE ATTENDANCE*****/
    function employee_attendance($date = '', $month = '', $year = '')
    {
        global $attendance_module;
        if ($attendance_module != 1) {
            redirect('admin_panel');
        }

        /*
         * This will check if any date has been chosen for attendance
         */

        if ($date) {
            $data['date'] = $date;
            $data['month'] = $month;
            $data['year'] = $year;
            $data['full_date'] = $year . '-' . $month . '-' . $date;

            $data['take_attendance'] = $data['full_date'];
        }


        if ($_POST) {

            $number_of_rows = count($this->input->post('employee_id'));
            $data = array();
            for ($i = 0; $i < $number_of_rows; $i++) {
                $data[$i] = array(
                    'employee_id' => $this->input->post('employee_id')[$i],
                    'employee_name' => $this->input->post('employee_name')[$i],
                    'status' => $this->input->post('status')[$i],
                    'date' => strtotime($this->input->post('date')[$i])
                );

            }

            $this->db->insert_batch('attendance', $data);

            $this->session->set_flashdata('success', '<b>Employee attendance taken!</b>');

            redirect(base_url() . 'admin_panel/employee_attendance_report/'
                . strtotime($year . '-' . $month . '-' . $date), 'refresh');

        }

        $data['page_name'] = 'EMPLOYEES ATTENDANCE';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/employees_attendance', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function emp_attendance_selector()
    {

        $today = $this->input->post('date');
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $date = strtotime($year . '-' . $month . '-' . $today);

        $check_attendance = $this->db->get_where('attendance', array('date' => $date));

        if ($check_attendance->num_rows() == 0) {

            $this->session->set_flashdata('info', '<b>Please Employee attendance carefully!</b>');

            redirect(base_url() . 'admin_panel/employee_attendance/' . $today . '/' .
                $month . '/' .
                $year);
        } else {
            $this->session->set_flashdata('warning', '<b>Employee attendance taken already, check the report!</b>');
            redirect(base_url() . 'admin_panel/employee_attendance_report/' . $date);
        }

    }

    function attendance_report($date1 = '', $date2 = '')
    {
        global $attendance_module;
        if ($attendance_module != 1) {
            redirect('admin_panel');
        }


        if ($_POST) {
            $date1 = strtotime($this->input->post('date1'));
            $data['date1'] = $date1;

            $date2 = strtotime($this->input->post('date2'));
            $data['date2'] = $date2;

            $data['working_days'] = $this->db->select('date')->group_by('date')
                ->get_where('attendance',
                    array(
                        'date >=' => $date1,
                        'date <=' => $date2
                    )
                )->result();


        }

        $data['page_name'] = 'ATTENDANCE REPORT';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/attendance_report', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function employee_attendance_report($date)
    {
        global $attendance_module;
        if ($attendance_module != 1) {
            redirect('admin_panel');
        }


        $data['page_name'] = 'EMPLOYEES ATTENDANCE REPORT';

        $data['attendance_info'] = $this->db->get_where('attendance', array('date' => $date))->result();

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/employee_attendance_report', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * SALARY MANAGEMENT
     */

    public function salary_grades()
    {
        global $salary_module;
        if ($salary_module != 1) {
            redirect('admin_panel');
        }

        $this->load->model('crud_model');

        if ($_POST) {
            $this->crud_model->save_salary_template_info();

            $this->session->set_flashdata('success', '<b>Salary Grade information saved!</b>');
            redirect(base_url() . 'admin_panel/salary_grades');
        }

        $data['salary_template_info'] = $this->crud_model->select_salary_template_info();

        $data['page_name'] = 'SALARY GRADES';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/salary_grades', $data, true);

        return $this->load->view('backend/index', $data);
    }

    public function salary_template_delete($id)
    {

        $this->crud_model->delete_salary_template($id);
        $this->session->set_flashdata('success', '<b>Salary Grade information Deleted!</b>');
        redirect(base_url() . 'admin_panel/salary_grades');
    }

    public function manage_salary()
    {
        global $salary_module;
        if ($salary_module != 1) {
            redirect('admin_panel');
        }


        $data['page_name'] = 'MANAGE SALARY';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/manage_salary', $data, true);

        return $this->load->view('backend/index', $data);
    }

    public function update_salary_grade()
    {
        $data['salary_template_id'] = $_POST['salary_template_id'];
        $this->db->where('employee_id', $_POST['employee_id']);
        $this->db->update('employees', $data);

        return true;

    }

    public function employee_salary_list()
    {
        global $salary_module;
        if ($salary_module != 1) {
            redirect('admin_panel');
        }


        $data['page_name'] = 'EMPLOYEE SALARY LIST';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/employee_salary_list', $data, true);

        return $this->load->view('backend/index', $data);
    }

    public function salary_payment()
    {
        global $salary_module;
        if ($salary_module != 1) {
            redirect('admin_panel');
        }

        if (isset($_POST['submit'])) {
            $data['employees'] = $this->db->get_where('employees', array('employee_status' => 1))->result_array();
            $data['date'] = $_POST['date'];
        }

        $data['page_name'] = 'SALARY PAYMENT';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/salary_payment', $data, true);

        return $this->load->view('backend/index', $data);
    }

    public function make_salary_payment($employee_id = '', $month = '', $year = '')
    {
        global $salary_module;
        if ($salary_module != 1) {
            redirect('admin_panel');
        }

        $date = $month . '/' . $year;

        if (isset($_POST['pay'])) {
            $pay_salary = $this->crud_model->pay_salary($employee_id, $date);

            if ($pay_salary) {
                $this->session->set_flashdata('success', 'Salary paid successfully.');
                redirect(base_url() . 'admin_panel/salary_payment');

            } else {
                $this->session->set_flashdata('error', 'Salary can not be paid.');
            }
        }

        if ($employee_id !== '') {

            $data['salary_template_id'] = $this->db->get_where('employees', array('employee_id' => $employee_id))
                ->row()->salary_template_id;

            $data['employee_name'] = $this->db->get_where('employees', array('employee_id' => $employee_id))
                ->row()->employee_name;

            $data['salary_history'] = $this->db->limit(12)->get_where('salary', array('employee_id' => $employee_id))->result_array();
            $data['date'] = $date;
        }

        $data['page_name'] = 'SALARY PAYMENT';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/make_salary_payment', $data, true);

        return $this->load->view('backend/index', $data);
    }

    public function salary_payslip($employee_id = '', $month = '', $year = '')
    {
        global $salary_module;
        if ($salary_module != 1) {
            redirect('admin_panel');
        }


        if ($_POST) {
            $employee_id = $this->input->post('employee_id');
            $date = $this->input->post('date');
            $data['employee_id'] = $employee_id;

            $data['date'] = $date;

            $data['salary_payslip'] = $this->db->get_where('salary', array('employee_id' => $employee_id, 'date' => $date))->result_array();

            $data['salary_template_id'] = $this->db->get_where('employees', array('employee_id' => $employee_id))->row()->salary_template_id;

        }
        if ($month != '') {
            $date = $month . '/' . $year;

            $data['salary_payslip'] = $this->db->get_where('salary', array('employee_id' => $employee_id, 'date' => $date))->result_array();
            $data['salary_template_id'] = $this->db->get_where('employees', array('employee_id' => $employee_id))->row()->salary_template_id;
            $data['employee_id'] = $employee_id;
            $data['date'] = $date;
        }

        $data['page_name'] = 'PAYMENT SLIP';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/salary_payslip', $data, true);

        return $this->load->view('backend/index', $data);
    }

    public function payroll_summary()
    {
        global $salary_module;
        if ($salary_module != 1) {
            redirect('admin_panel');
        }


        if ($_POST) {
            $date = $this->input->post('date');
            $data['date'] = $date;
            $get_report = $this->db->get_where('salary', array('date' => $date))->result();
            $data['salary_report'] = $get_report;
            $total_paid = $this->db->select('sum(payment_amount) as paid')->get_where('salary', array('date' => $date))->row();
            $data['paid'] = $total_paid->paid;

        }

        $data['page_name'] = 'PAYROLL SUMMARY';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/payroll_summary', $data, true);

        return $this->load->view('backend/index', $data);
    }


    /*
     * System Settings
     */

    public function system_settings($param = '')
    {
        global $admin_module;
        if ($admin_module != 1) {
            redirect('admin_panel');
        }


        if ($param == 'do_update') {
            $name = $this->input->post('settings_name');
            $this->db->where('info_type', 'settings_name')->set('description', $name)->update('system_settings');

            $email = $this->input->post('settings_email');
            $this->db->where('info_type', 'settings_email')->set('description', $email)->update('system_settings');

            $address = $this->input->post('settings_address');
            $this->db->where('info_type', 'settings_address')->set('description', $address)->update('system_settings');

            $contact = $this->input->post('settings_contact');
            $this->db->where('info_type', 'settings_contact')->set('description', $contact)->update('system_settings');

            $working_hours = $this->input->post('settings_working_hours');
            $this->db->where('info_type', 'settings_working_hours')->set('description', $working_hours)->update('system_settings');

            $location = $this->input->post('settings_location');
            $this->db->where('info_type', 'location')->set('description', $location)->update('system_settings');

            $settings_facebook = $this->input->post('settings_facebook');
            $this->db->where('info_type', 'facebook')->set('description', $settings_facebook)->update('system_settings');

            $settings_google = $this->input->post('settings_google');
            $this->db->where('info_type', 'google')->set('description', $settings_google)->update('system_settings');

            $settings_twitter = $this->input->post('settings_twitter');
            $this->db->where('info_type', 'twitter')->set('description', $settings_twitter)->update('system_settings');

            $linkedin = $this->input->post('settings_linkedin');
            $this->db->where('info_type', 'linkedin')->set('description', $linkedin)->update('system_settings');


            if (file_exists($_FILES['image']['tmp_name'])) {

                $this->load->library('image_lib');
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = 1024;
                $config['max_width'] = 1600;
                $config['max_height'] = 1600;
                $config['maintain_ratio'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('image')) {
                    $error = array('error' => $this->upload->display_errors());

                    $this->session->set_flashdata('error', $error['error']);

                    redirect(base_url() . 'admin_panel/system_settings', 'refresh');

                } else {
                    $image = $this->upload->data();
                    $image_path = $config['upload_path'] . $image['file_name'];

                    $this->db->where('info_type', 'settings_logo')->set('description', $image_path)->update('system_settings');
                }

            }

            $this->session->set_flashdata('success', '<b>System Settings information saved!</b>');
            redirect(base_url() . 'admin_panel/system_settings', 'refresh');

        }

        $data['page_name'] = 'System Settings';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);
        $data['modal'] = $this->load->view('backend/modal', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/system_settings', $data, true);

        return $this->load->view('backend/index', $data);
    }

}