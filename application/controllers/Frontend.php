<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Frontend extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('admin_id') == NULL) {
            redirect('admin_login', 'refresh');
        }

        $this->load->library('image_lib');
        $this->load->library('user_agent');
    }

    /*
     * SLIDER & GALLERY IMAGE
     * PAGINATION WITH HELPER FUNCTION
     */

    public function index()
    {

        $data['page_name'] = 'Slider Image';

        /*
         * Data Manipulation of this page
         */

        $row = $this->db->get('image_gallery')->result();

        $count = count($row); //returns total row

        $data['total_result'] = $count;
        $data['per_page'] = 6;

        $config = array();
        $config["base_url"] = base_url() . "frontend/index/";
        $config["total_rows"] = $count;
        $config["per_page"] = $data['per_page'];

        $this->load->helper('bs_pagination_config_helper');

        $config = bootstrap_config($config);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $str_links = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = explode('&nbsp;', $str_links);

        $data['all_images'] = $this->crud_model->select_all_images($config["per_page"], $page);

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/slider_image', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * STORING SLIDER & GALLERY IMAGE INFORMATION
     * STORING IMAGE & RETURNS IMAGE PATH BY HELPER FUNCTION
     * AGENT LIBRARY RETURNS PREVIOUS/REFERRAL URL
     */

    public function store_image()
    {
        $data = array();
        $data['caption'] = $this->input->post('caption');
        $data['description'] = $this->input->post('description');
        $data['image_type'] = $this->input->post('image_type');
        $data['visibility_status'] = $this->input->post('visibility_status');

        $this->load->helper('file_upload_helper');

        if ($this->agent->is_referral()) {
            $refer = $this->agent->referrer();
        }
        $upload_path = './image_upload/';
        $data['image'] = image_upload($refer, $upload_path);

        $this->db->insert('image_gallery', $data);

        $this->session->set_flashdata('success', '<b>Image Uploaded and Information Saved!</b>');

        redirect($refer, 'refresh');
    }

    /*
     * UPDATING IMAGE INFORMATION ONLY
     */

    public function update_image()
    {
        $data = array();
        $id = $this->input->post('image_id');

        $data['caption'] = $this->input->post('caption');
        $data['description'] = $this->input->post('description');
        $data['image_type'] = $this->input->post('image_type');
        $data['visibility_status'] = $this->input->post('visibility_status');

        $this->db->where('image_id', $id)->update('image_gallery', $data);

        $this->session->set_flashdata('success', '<b>Image Information Updated!</b>');

        redirect(base_url() . 'frontend/index', 'refresh');
    }

    /*
     * DELETING IMAGE INFO
     * UNLINK IMAGE FROM STORED PATH
     */

    public function delete_image_info($id)
    {
        $path = $this->db->get_where('image_gallery', array('image_id' => $id))->row('image');
        $image_path = substr($path, 2);

        unlink($image_path); // REMOVES 1st 2 character "./" from image name

        $this->db->where('image_id', $id);
        $this->db->delete('image_gallery');

        $this->session->set_flashdata('error', '<b>Image Information deleted!</b>');

        redirect(base_url() . 'frontend/index', 'refresh');
    }


    /*
     * MESSAGE FROM ORGANIZATION
     */

    function message_from_organization($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'logo_upload') {
            $this->load->helper('file_upload_helper');
            $this->load->library('user_agent');

            $refer = $this->agent->referrer();

            $upload_path = './image_upload/';
            $data['description'] = image_upload($refer, $upload_path);


            $this->db->where('info_type', 'image');
            $this->db->update('message_from_organization', $data);

            $this->session->set_flashdata('message', '<b>Organization\'s Image updated!</b>');

            redirect(base_url() . 'frontend/message_from_organization', 'refresh');
        } elseif ($param == 'do_update') {
            $data['description'] = $this->input->post('message');
            $this->db->where('info_type', 'message');
            $this->db->update('message_from_organization', $data);


            $this->session->set_flashdata('message', '<b>Organization\'s Message updated!</b>');

            redirect(base_url() . 'frontend/message_from_organization', 'refresh');
        }

        $data['page_name'] = 'Message From Organization';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/message_from_organization', $data, true);

        return $this->load->view('backend/index', $data);
    }


    /*
     * MESSAGE FROM CEO
     */

    function message_from_ceo($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'logo_upload') {
            $this->load->helper('file_upload_helper');
            $this->load->library('user_agent');

            $refer = $this->agent->referrer();

            $upload_path = './image_upload/';
            $data['description'] = image_upload($refer, $upload_path);


            $this->db->where('info_type', 'image');
            $this->db->update('message_from_ceo', $data);

            $this->session->set_flashdata('message', '<b>CEO\'s Image updated!</b>');

            redirect(base_url() . 'frontend/message_from_ceo', 'refresh');
        } elseif ($param == 'do_update') {
            $data['description'] = $this->input->post('message');
            $this->db->where('info_type', 'message');
            $this->db->update('message_from_ceo', $data);


            $this->session->set_flashdata('message', '<b>CEO\'s Message updated!</b>');

            redirect(base_url() . 'frontend/message_from_ceo', 'refresh');
        }

        $data['page_name'] = 'Message From CEO';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/message_from_ceo', $data, true);

        return $this->load->view('backend/index', $data);
    }


    /*
     * MESSAGE FROM CEO
     */

    function why_choose_us($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $data['title'] = $this->input->post('title');
            $data['message'] = $this->input->post('message');

            $this->db->insert('why_choose_us', $data);

            $this->session->set_flashdata('message', '<b>Why Choose Us Data Saved!</b>');

            redirect(base_url() . 'frontend/why_choose_us', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('desc_id', $param2);
            $this->db->delete('why_choose_us');

            $this->session->set_flashdata('error', '<b>Why Choose Us Information deleted!</b>');

            redirect(base_url() . 'frontend/why_choose_us', 'refresh');
        } elseif ($param == 'do_update') {
            $data['title'] = $this->input->post('title');
            $data['message'] = $this->input->post('message');

            $this->db->where('desc_id', $param2);
            $this->db->update('why_choose_us', $data);

            $this->session->set_flashdata('message', '<b>Why Choose Us Data updated!</b>');

            redirect(base_url() . 'frontend/why_choose_us', 'refresh');
        }

        $data['page_name'] = 'Why Choose Us?';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/why_choose_us', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * RETURNS TESTIMONIALS PAGE
     * UPLOADS RESIZED IMAGE BY HELPER FUNCTION
     */

    function testimonials($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $this->load->helper('file_upload_helper');
            $this->load->library('user_agent');

            $refer = $this->agent->referrer();

            $data['image'] = resized_image_upload($refer);

            $data['name'] = $this->input->post('name');
            $data['message'] = $this->input->post('message');
            $data['status'] = $this->input->post('status');

            $this->db->insert('testimonials', $data);

            $this->session->set_flashdata('message', '<b>Client Testimonial Data Saved!</b>');

            redirect(base_url() . 'frontend/testimonials', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('testimonial_id', $param2);
            $this->db->delete('testimonials');

            $this->session->set_flashdata('error', '<b>Client Testimonial Information deleted!</b>');

            redirect(base_url() . 'frontend/testimonials', 'refresh');
        } elseif ($param == 'do_update') {
            if (file_exists($_FILES['image_upload']['tmp_name'])) {
                $this->load->helper('file_upload_helper');
                $this->load->library('user_agent');
                $refer = $this->agent->referrer();

                $data['image'] = resized_image_upload($refer);
            }

            $data['name'] = $this->input->post('name');
            $data['message'] = $this->input->post('message');
            $data['status'] = $this->input->post('status');

            $this->db->where('testimonial_id', $param2);
            $this->db->update('testimonials', $data);

            $this->session->set_flashdata('message', '<b>Client Testimonial Data updated!</b>');

            redirect(base_url() . 'frontend/testimonials', 'refresh');
        }

        $data['page_name'] = 'Testimonials';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/testimonials', $data, true);

        return $this->load->view('backend/index', $data);
    }


    /*
     * RETURNS ABOUT PAGE SETTINGS
     */

    function about_us($param = '')
    {
        if ($param == 'do_update') {
            $data['description'] = $this->input->post('message');
            $this->db->where('info_type', 'about_us');
            $this->db->update('system_settings', $data);


            $this->session->set_flashdata('message', '<b>About Us Text updated!</b>');

            redirect(base_url() . 'frontend/about_us', 'refresh');
        }

        $data['page_name'] = 'About Us';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/about_us', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * RETURNS CRUD COUNTRY
     */
    function countries($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {

            $data['country_name'] = str_replace(' ', '-', $this->input->post('country_name'));
            $data['status'] = $this->input->post('status');

            $this->db->insert('countries_list', $data);

            $this->session->set_flashdata('message', '<b>Countries Data Saved!</b>');

            redirect(base_url() . 'frontend/countries', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('country_id', $param2);
            $this->db->delete('countries_list');

            $this->session->set_flashdata('error', '<b>Countries Information deleted!</b>');

            redirect(base_url() . 'frontend/countries', 'refresh');
        } elseif ($param == 'do_update') {

            $data['country_name'] = str_replace(' ', '-', $this->input->post('country_name'));
            $data['status'] = $this->input->post('status');

            $this->db->where('country_id', $param2);
            $this->db->update('countries_list', $data);

            $this->session->set_flashdata('message', '<b>Countries Data updated!</b>');

            redirect(base_url() . 'frontend/countries', 'refresh');
        }

        $data['page_name'] = 'Countries';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/countries', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * RETURNS STUDY INFO BY COUNTRY
     */

    function information_by_country($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {

            $data['country_id'] = $this->input->post('country_id');
            $data['why_study'] = $this->input->post('why_study');
            $data['required_documents'] = $this->input->post('required_documents');
            $data['application_procedure'] = $this->input->post('application_procedure');

            $this->db->insert('information_by_country', $data);

            $this->session->set_flashdata('message', '<b>Information By Countries Data Saved!</b>');

            redirect(base_url() . 'frontend/information_by_country', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('info_id', $param2);
            $this->db->delete('information_by_country');

            $this->session->set_flashdata('error', '<b>Information By Countries data deleted!</b>');

            redirect(base_url() . 'frontend/information_by_country', 'refresh');
        } elseif ($param == 'do_update') {

            $data['country_id'] = $this->input->post('country_id');
            $data['why_study'] = $this->input->post('why_study');
            $data['required_documents'] = $this->input->post('required_documents');
            $data['application_procedure'] = $this->input->post('application_procedure');

            $this->db->where('info_id', $param2);
            $this->db->update('information_by_country', $data);

            $this->session->set_flashdata('message', '<b>Information By Countries Data updated!</b>');

            redirect(base_url() . 'frontend/information_by_country', 'refresh');
        }

        $data['page_name'] = 'Information By Country';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/information_by_country', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * RETURNS STUDY INFO BY COUNTRY
     */

    function our_prices($param = '')
    {
        if ($param == 'do_update') {
            $data['description'] = $this->input->post('courses');
            $this->db->where('info_type', 'courses');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('service_charges');
            $this->db->where('info_type', 'service_charges');
            $this->db->update('system_settings', $data);

            $this->session->set_flashdata('message', '<b>Our Prices Text updated!</b>');

            redirect(base_url() . 'frontend/our_prices', 'refresh');
        }

        $data['page_name'] = 'Our Prices';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/our_prices', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * RETURNS OUR TEAM MEMBER
     */

    function our_team_members($param = '', $param2 = '')
    {

        $data = array();

        if ($param == 'create') {
            $this->load->helper('file_upload_helper');
            $this->load->library('user_agent');

            $refer = $this->agent->referrer();

            $data['emp_id'] = $this->input->post('emp_id');
            $upload_path = './image_upload/employees/';
            $data['emp_image'] = image_upload($refer, $upload_path);

            $this->db->insert('emp_images', $data);

            $this->session->set_flashdata('message', '<b>Employee\'s Image Saved!</b>');

            redirect(base_url() . 'frontend/our_team_members', 'refresh');
        } elseif ($param == 'do_update') {

            $this->load->helper('file_upload_helper');
            $this->load->library('user_agent');

            $refer = $this->agent->referrer();

            $data['emp_id'] = $this->input->post('emp_id');
            if (file_exists($_FILES['image_upload']['tmp_name'])) {
                $path = $this->db->get_where('emp_images', array('image_id' => $param2))->row('emp_image');
                $image_path = substr($path, 2);

                unlink($image_path); // REMOVES 1st 2 character "./" from image name

                $upload_path = './image_upload/employees/';
                $data['emp_image'] = image_upload($refer, $upload_path);
            }

            $this->db->where('image_id', $param2);
            $this->db->update('emp_images', $data);


            $this->session->set_flashdata('message', '<b>Employee\'s Message updated!</b>');

            redirect(base_url() . 'frontend/our_team_members', 'refresh');
        }

        $data['page_name'] = 'Employees Images';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/our_team_members', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * RETURNS SUBSCRIBERS LIST
     */

    function subscribed_members()
    {
        $data['page_name'] = 'Subscriber Members';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/subscribed_members', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * SENDING MAILS TO THE SUBSCRIBER
     * VIA EMAIL HELPER FUNCTION
     */

    function send_mail()
    {
        $this->load->library('email');

        $to = $this->input->post('to_mail');

        $from = $this->db->where('info_type', 'settings_email')
            ->get('system_settings')->row('description');

        $subject = $this->input->post('subject');
        $data['mail_body'] = $this->input->post('mail_body');

        $name = $this->db->where('info_type', 'system_name')
            ->get('system_settings')->row('description');
        $protocol = $this->db->where('info_type', 'mail_protocol')
            ->get('system_settings')->row('description');
        $mail_path = $this->db->where('info_type', 'mail_path')
            ->get('system_settings')->row('description');
        $host = $this->db->where('info_type', 'mail_host')
            ->get('system_settings')->row('description');
        $port = $this->db->where('info_type', 'mail_host_port')
            ->get('system_settings')->row('description');
        $password = $this->db->where('info_type', 'settings_email_password')
            ->get('system_settings')->row('description');


        $mail_body = $this->load->view('mail_templates/subscribers_mail', $data, TRUE);

        $this->load->helper('send_email_helper');

        $mail_status = send_mail($name, $to, $from, $subject, $mail_body, $protocol, $mail_path, $host, $port, $password);

        if ($mail_status) {
            $this->session->set_flashdata('message', '<b>Your Mail Has been sent to this Employee!</b>');
        } else {
            $this->session->set_flashdata('error', '<b>Something Went Wrong!</b>');
        }

        redirect(base_url() . 'frontend/subscribed_members', 'refresh');

    }

    /*
     * MAIL HOST, PROTOCOL SETUP
     */
    function mail_settings($param = '')
    {
        if ($param == 'do_update') {
            $data['description'] = $this->input->post('mail_protocol');
            $this->db->where('info_type', 'mail_protocol');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('mail_path');
            $this->db->where('info_type', 'mail_path');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('mail_host');
            $this->db->where('info_type', 'mail_host');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('mail_host_port');
            $this->db->where('info_type', 'mail_host_port');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('settings_email');
            $this->db->where('info_type', 'settings_email');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('settings_email_password');
            $this->db->where('info_type', 'settings_email_password');
            $this->db->update('system_settings', $data);

            $this->session->set_flashdata('message', '<b>Mail Setup updated!</b>');

            redirect(base_url() . 'frontend/mail_settings', 'refresh');
        }

        $data['page_name'] = 'Mail Settings';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/mail_settings', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /*
     * ONLINE APPLICATION FROM CLIENT FROM WEBSITE
     *
     */
    function online_application()
    {
        $data['page_name'] = 'Online Application';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/website_navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/website_backend/online_application', $data, true);

        return $this->load->view('backend/index', $data);
    }
}