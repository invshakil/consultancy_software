<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model
{

    function select_salary_template_info(){
        return $this->db->get('salary_template')->result_array();
    }


    function save_salary_template_info()
    {

        $data['salary_grades'] = $this->input->post('salary_grades');
        $data['basic_salary'] = $this->input->post('basic_salary');
        $data['over_time'] = $this->input->post('over_time');
        $data['gross_salary'] = $this->input->post('gross_salary');
        $data['total_deduction'] = $this->input->post('total_deduction');
        $data['net_salary'] = $this->input->post('net_salary');


        $allowance_entries = array();
        $new_allowance_entry = array();
        $new_deduction_entry = array();
        $allowance_descriptions = $this->input->post('entry_allowance_description');
        $allowance_amounts = $this->input->post('entry_allowance_amount');
        $number_of_allowance_entries = sizeof($allowance_descriptions);

        for ($i = 0; $i < $number_of_allowance_entries; $i++) {
            if ($allowance_descriptions[$i] != "" && $allowance_amounts[$i] != "") {
                $new_allowance_entry = array('allowance_description' =>$allowance_descriptions[$i], 'allowance_amount' =>$allowance_amounts[$i]);
                array_push($allowance_entries, $new_allowance_entry);

            }
        }
        $data['allowance_entries'] = json_encode($allowance_entries);


        $deduction_entries = array();
        $deduction_descriptions = $this->input->post('entry_deduction_description');
        $deduction_amounts = $this->input->post('entry_deduction_amount');
        $number_of_deduction_entries = sizeof($deduction_descriptions);

        for ($i = 0; $i < $number_of_deduction_entries; $i++) {
            if ($deduction_descriptions[$i] != "" && $deduction_amounts[$i] != "") {
                $new_deduction_entry = array('deduction_description' =>$deduction_descriptions[$i], 'deduction_amount' =>$deduction_amounts[$i]);
                array_push($deduction_entries, $new_deduction_entry);
            }
        }
        $data['deduction_entries'] = json_encode($deduction_entries);


        $this->db->insert('salary_template', $data);
    }

    function delete_salary_template($id){
        $this->db->where('salary_template_id',$id);
        $this->db->delete('salary_template');
    }

    function pay_salary($employee_id,$date){

        $data['employee_id']=$employee_id;
        $data['date']= $date;
        $data['gross_salary'] = $this->input->post('gross_salary');
        $data['total_deduction']=$this->input->post('total_deduction');
        $data['net_salary'] = $this->input->post('net_salary');
        $data['fine_deduction'] = $this->input->post('fine_deduction');
        $data['payment_amount'] = $this->input->post('payment_amount');

        date_default_timezone_set('asia/dhaka');
        $today = date('d/m/Y');
        $data['paying_date'] = $today;

        $inserted_id=$this->db->insert('salary',$data);

        if ($inserted_id){
            return true;
        }else{
            return false;
        }

    }

    function select_all_images($limit, $start)
    {
        $this->db->select('*');
        $this->db->from('image_gallery');
        $this->db->limit($limit, $start);
        $this->db->order_by('image_id','desc');
        $query = $this->db->get();
        if ($query->num_rows()>0) {
            $result = $query->result();
            return $result;
        }
        return false;
    }

}