<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('bootstrap_config')) {
    function send_mail($name, $to, $from, $get_subject, $mail_body, $protocol, $mail_path, $host, $port, $password)
    {
        $ci =& get_instance();
        $ci->load->library('email');

        $from_email = $from; // $from
        $message = $mail_body;
        $subject = $get_subject;



        //configure email settings
        $config['protocol'] = $protocol;
        $config['mailpath'] = $mail_path;
        $config['smtp_host'] = $host; //smtp host name
        $config['smtp_port'] = $port; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = $password; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $config['starttls'] = true;
        $config['smtp_crypto'] = 'tls';
        $config['smpt_ssl'] = 'auto';

        $ci->email->set_mailtype("html");

        $ci->email->initialize($config);

        //send mail
        $ci->email->from($from_email, $name);
        $ci->email->to($to);
        $ci->email->subject($subject);
        $ci->email->message($message);

        $send_status = $ci->email->send();

        if ( ! $send_status )
        {
            return False;
        }

        return TRUE;
//        return $ci->email->send();

    }
}