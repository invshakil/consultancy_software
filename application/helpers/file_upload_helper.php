<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    /*
     * HELPER FUNCTION OF RESIZED IMAGE UPLOAD
     * UPLOADS IMAGE TO SERVER & RETURNS IMAGE PATH
     */

if (!function_exists('image_upload')) {
    function image_upload($url,$image_path)
    {

        $ci =& get_instance();

        $config['upload_path'] = $image_path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 1024;
        $config['max_width'] = 1600;
        $config['max_height'] = 1600;
        $config['maintain_ratio'] = TRUE;
        $config['remove_spaces'] = TRUE;

        $ci->load->library('upload', $config);
        $ci->upload->initialize($config);

        if (!$ci->upload->do_upload('image_upload')) {
            $error = array('error' => $ci->upload->display_errors());

            $ci->session->set_flashdata('error', $error['error']);
            redirect($url, 'refresh');

        } else {
            $image = $ci->upload->data();
            return $config['upload_path'] . $image['file_name'];
        }

        return FALSE;

    }

    /*
     * HELPER FUNCTION OF RESIZED IMAGE UPLOAD
     * WIDTH 250 PIXEL
     * UPLOADS IMAGE TO SERVER & RETURNS IMAGE PATH
     */
    if (!function_exists('resized_image_upload')) {

        function resized_image_upload($url)
        {
            $ci =& get_instance();

            //this is the folder where we will place the  uploaded files
            $config['upload_path'] = './image_upload/';
            //array of allowed file formats
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_filename'] = '255';

            //whether file name should be encrypted or not
            $config['encrypt_name'] = TRUE;

            //store image info once uploaded
            $config['overwrite'] = TRUE;
            $config['remove_spaces'] = true;

            $config['max_size'] = 1024;//sets max size
            $config['max_width'] = 2048;//max width
            $config['max_height'] = 2048;//max height

            //this loads the image upload library which actually does the upload
            $ci->load->library('upload', $config);
            if (!$ci->upload->do_upload('image_upload')) {
                $error = array('error' => $ci->upload->display_errors());

                $ci->session->set_flashdata('error', $error['error']);
                redirect($url, 'refresh');

            } else {
                //this loads the library for image resize where upload is successful
                $config['image_library'] = 'gd2';
                //path to the image we want to resize which is the image we just uploaded
                $config['source_image'] = $ci->upload->upload_path . $ci->upload->file_name;
                $config['new_image'] = './image_upload/thumb/';
                $config['create_thumb'] = TRUE;
                //$config['thumb_marker'] = false;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 250;//the width to resize to;
                $ci->load->library('image_lib', $config);//this loads the image resize library
                $ci->image_lib->initialize($config);
                $ci->image_lib->resize();//the resize function
                //check if the resize succeeds
                if (!$ci->image_lib->resize()) {

                    $error = array('error' => $ci->image_lib->display_errors());
                    $ci->session->set_flashdata('error', $error['error']);
                    redirect($url, 'refresh');
                } else {
                    // assign a variable to the image we just resized
                    $image = $ci->upload->file_name;

                    //this is to get the name of the newly created thumbnail as that is what we will be uploading to database;
                    $image = str_ireplace('.', '_thumb.', $image);
                    //this is to get upload and newly created thumb image name
                    $image_path = $config['new_image'] . $image;

                    return $image_path;
                }
            }

            return FALSE;

        }
    }
}