<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('bootstrap_config')) {
    function bootstrap_config($config)
    {
        /* Pagination Config */

        $config["num_links"] = false;

        /* BootStrap pagination Button Settings */
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* !-BootStrap pagination Button Settings */

        /* !- Pagination Config */

        return $config;
    }

    function uk_pagination($config){
        /* Pagination Config */

        $config["num_links"] = false;

        /* BootStrap pagination Button Settings */
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = "<ul class='uk-pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='uk-disabled'><li class='uk-active'><a href='#'><span>";
        $config['cur_tag_close'] = "</span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* !-BootStrap pagination Button Settings */

        /* !- Pagination Config */

        return $config;
    }
}