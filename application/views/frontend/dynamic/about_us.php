<!-- spotlight -->
<div id="tm-spotlight" class="tm-block-spotlight uk-block uk-block-default tm-block-fullwidth tm-grid-collapse">
    <div class="uk-container uk-container-center">
        <section class="tm-spotlight uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
            <div class="uk-width-1-1">
                <div class="uk-panel uk-contrast tm-overlay-primary">
                    <div class="tm-background-cover uk-cover-background uk-flex uk-flex-center uk-flex-middle"
                         style="background-position: 50% 0px; background-image:url(<?=base_url();?>assets/frontend/images/background/bg-image-1.jpg)"
                         data-uk-parallax="{bg: '-200'}">
                        <div class="uk-position-relative uk-container tm-inner-container">
                            <h2 class="uk-module-title-alt uk-margin-remove"><?php echo $system_name;?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- breadcrumbs -->
<div class="tm-breadcrumbs">
    <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
            <ul class="uk-breadcrumb">
                <li><a href="<?=base_url();?>">Home</a></li>
                <li class="uk-active"><span>About Us</span></li>
            </ul>
        </div>
    </div>
</div>

<!-- main content -->
<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
            <div class="tm-main uk-width-medium-7-10" data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:500}">
                <main id="tm-content" class="tm-content">
                    <article class="uk-article tm-article">
                        <div class="tm-article-wrapper">
                            <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                                <div class="tm-article" style="text-align: justify">
                                    <h1 class="uk-article-title">About Us</h1>
                                    <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'about_us'))->row(); ?>

                                    <?php echo $msg->description; ?>
                                    <hr>
                                    <br>

                                </div>
                            </div>
                        </div>
                    </article>
                </main>
            </div>

            <aside class="tm-sidebar-b uk-width-medium-3-10" data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:800}">
                <div class="uk-panel uk-panel-box tm-panel-box-primary-light uk-border-rounded">
                    <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="{}">
                        <li class="uk-active"><a href="<?php echo base_url()?>about-us">About Us</a></li>
                        <li><a href="<?php echo base_url()?>our-team">Our Team</a></li>
                        <li><a href="<?php echo base_url()?>client-testimonials">Client Testimonials</a></li>
                        <li><a href="<?php echo base_url()?>contact-us">Contact Us</a></li>
                    </ul>
                </div>

                <div class="uk-panel uk-panel-box uk-panel-box-primary uk-border-rounded tm-background-icon">
                    <h3 class="uk-panel-title"><i class="uk-icon-at uk-margin-small-right"></i> Talk to us</h3>
                    <p>Would you like to speak to one of our financial advisers over the phone? <a
                            href="tel:<?php echo $system_contact;?>">Call us</a> and we'll get back to you in the course of the day.
                    </p><a class="uk-button-default uk-button" href="<?php echo base_url()?>contact-us" target="_blank">Get in
                        touch</a>
                </div>

            </aside>
        </div>
    </div>
</div>

<script>
    $( ".tm-article" ).find( "blockquote" ).addClass('tm-highlight-block tm-highlight-left');
</script>