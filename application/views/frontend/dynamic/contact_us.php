<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
            <div class="tm-main uk-width-medium-7-10">
                <main id="tm-content" class="tm-content">
                    <article class="uk-article tm-article">
                        <div class="tm-article-wrapper">
                            <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                                <div class="tm-article">
                                    <!-- contact form -->
                                    <div class="contact">
                                        <h3 class="uk-panel-title">Email Us</h3>
                                        <form role="form" method="post" action="<?php echo base_url()?>website/send_mail_from_contact_page" class="uk-form uk-form-stacked">
                                            <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-1-1">


                                                <!-- name field -->
                                                <div class="uk-form-icon uk-grid-margin">
                                                    <i class="uk-icon-user"></i>
                                                    <input type="text" class="uk-width-1-1" name="contact-name"
                                                           placeholder="Name" size="30" required="required">
                                                </div>

                                                <!-- email field -->
                                                <div class="uk-form-icon uk-grid-margin">
                                                    <i class="uk-icon-envelope"></i>
                                                    <input type="email" class="uk-width-1-1" name="contact-email"
                                                           placeholder="Your email" size="30" required="required">
                                                </div>

                                                <!-- subject field -->
                                                <div class="uk-form-icon uk-grid-margin">
                                                    <i class="uk-icon-bookmark"></i>
                                                    <input type="text" class="uk-width-1-1 optional"
                                                           name="contact-subject" placeholder=" Subject" size="60">
                                                </div>

                                                <!-- message field -->
                                                <div class="uk-grid-margin">
                                                    <textarea class="uk-width-1-1" name="contact-message" cols="30"
                                                              rows="3" placeholder="Your message"
                                                              required="required"></textarea>
                                                </div>

                                                <!-- submit button -->
                                                <div class="form-actions">
                                                    <button class="uk-button uk-button-primary"
                                                            type="submit">Send mail
                                                    </button>
                                                </div>

                                                <div id="message"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </main>
            </div>

            <!-- sidebar-a -->
            <aside class="tm-sidebar-a uk-width-medium-3-10">
                <div class="uk-panel uk-panel-box">
                    <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-large-1-1" data-uk-grid-margin="">

                        <div class="uk-panel uk-panel-space">
                            <h3 class="uk-module-title">Contact</h3>
                            <p><?php echo $system_name;?><br><br>
                                <?php echo $system_address;?></p>
                            <ul class="uk-list list-icons">
                                <li><i class="uk-icon-envelope-o"></i> <?php echo $system_email;?></li>
                                <li><i class="uk-icon-phone"></i> <?php echo $system_contact;?></li>
                                <li><i class="uk-icon-clock-o"></i> <?php echo $system_working_hours;?></li>
                                <li><i class="uk-icon-map-marker"></i> Dhaka, Bangladesh</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>

        </div>
    </div>
</div>

<!-- bottom-b -->
<div class="tm-block-bottom-b uk-block uk-block-primary-light tm-block-fullwidth tm-grid-collapse" id="tm-bottom-b">
    <div class="uk-container uk-container-center">
        <section class="tm-bottom-a uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">

            <div class="uk-width-1-1">
                <div class="uk-panel">

                    <!-- map -->
                    <script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyAzb6gw9PpTif03dK4Bm_pUYmCO6zRgw4s"></script>
                    <div style="overflow:hidden;height:400px;width:100%;">
                        <div id="gmap_canvas" style="height:400px;width:100%;"></div>
                        <style>#gmap_canvas img {
                                max-width: none !important;
                                background: none !important
                            }</style>


                        <?php $settings_location = $this->db->where('info_type','location')
                            ->get('system_settings')->row('description');?>

                        <a class="google-map-code" href="http://www.map-embed.com" id="get-map-data">map embed</a></div>
                    <script type="text/javascript"> function init_map() {
                            var latlng = new google.maps.LatLng(<?php echo $settings_location;?>);
                            var myOptions = {
                                zoom: 15,
                                styles: [{
                                    "featureType": "water",
                                    "elementType": "geometry",
                                    "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
                                }, {
                                    "featureType": "landscape",
                                    "elementType": "geometry",
                                    "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
                                }, {
                                    "featureType": "road.highway",
                                    "elementType": "geometry.fill",
                                    "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
                                }, {
                                    "featureType": "road.highway",
                                    "elementType": "geometry.stroke",
                                    "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
                                }, {
                                    "featureType": "road.arterial",
                                    "elementType": "geometry",
                                    "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
                                }, {
                                    "featureType": "road.local",
                                    "elementType": "geometry",
                                    "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
                                }, {
                                    "featureType": "poi",
                                    "elementType": "geometry",
                                    "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
                                }, {
                                    "featureType": "poi.park",
                                    "elementType": "geometry",
                                    "stylers": [{"color": "#dedede"}, {"lightness": 21}]
                                }, {
                                    "elementType": "labels.text.stroke",
                                    "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
                                }, {
                                    "elementType": "labels.text.fill",
                                    "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
                                }, {
                                    "elementType": "labels.icon",
                                    "stylers": [{"visibility": "off"}]
                                }, {
                                    "featureType": "transit",
                                    "elementType": "geometry",
                                    "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
                                }, {
                                    "featureType": "administrative",
                                    "elementType": "geometry.fill",
                                    "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
                                }, {
                                    "featureType": "administrative",
                                    "elementType": "geometry.stroke",
                                    "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
                                }],
                                center: latlng,
                                navigationControl: true,
                                scrollwheel: false,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };
                            map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
                            marker = new google.maps.Marker({
                                map: map,
                                position: new google.maps.LatLng(<?php echo $settings_location;?>)
                            });
                            infowindow = new google.maps.InfoWindow({content: "<b><?php echo $system_name;?></b><br/><?php echo $system_name;?>"});
                            google.maps.event.addListener(marker, "click", function () {
                                infowindow.open(map, marker);
                            });
                            infowindow.open(map, marker);
                        }
                        google.maps.event.addDomListener(window, 'load', init_map);</script>

                    <!-- generate your own map -> http://www.map-embed.com/ -->

                </div>
            </div>
        </section>
    </div>
</div>
