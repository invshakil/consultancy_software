<div class="tm-breadcrumbs">
    <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
            <ul class="uk-breadcrumb">
                <li><a href="<?=base_url()?>">Home</a></li>
                <li><span><a href="<?=base_url()?>about-us">About Us</a></span></li>
                <li class="uk-active"><span>Our Team</span></li>
            </ul>
        </div>
    </div>
</div>

<!-- main content -->
<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
            <div class="tm-main uk-width-medium-1-1">
                <main id="tm-content" class="tm-content">
                    <article class="uk-article tm-article">
                        <div class="tm-article-wrapper">
                            <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                                <div class="tm-article">
                                    <div class="uk-grid" data-uk-grid-margin="">

                                        <?php $members = $this->db->get_where('employees',array(''))->result();

                                        foreach ($members as $key=>$member) {?>
                                        <!-- block -->
                                        <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:<?php echo $key*200;?>}">
                                                <div class="uk-panel uk-panel-box tm-panel-card">
                                                    <div class="uk-panel-teaser">
                                                        <?php
                                                        $img = $this->db->get_where('emp_images', array('emp_id' => $member->employee_id))
                                                            ->row('emp_image');
                                                        if ($img != '') {?>

                                                        <img alt="Diane Moreno - CEO" class="tm-card-avatar"
                                                             src="<?php echo base_url().$img?>">
                                                        <?php } else {?>
                                                        <img alt="Diane Moreno - CEO" class="tm-card-avatar"
                                                             src="<?php echo base_url()?>assets/frontend/images/demo/avatar/team-1.jpg">
                                                        <?php } ?>
                                                        <div class="tm-social-icons">
                                                            <a class="uk-icon-button uk-icon-facebook"
                                                               href="#"
                                                               target="_blank"></a> <a
                                                                class="uk-icon-button uk-icon-twitter"
                                                                href="#"
                                                                target="_blank"></a> <a
                                                                class="uk-icon-button uk-icon-vk" href="#"
                                                                target="_self"></a> <a
                                                                class="uk-icon-button uk-icon-behance"
                                                                href="#"
                                                                target="_blank"></a>
                                                        </div>
                                                    </div>
                                                    <div class="tm-card-content">
                                                        <h3 class="uk-panel-title"><?php echo $member->employee_name?></h3>
                                                        <h4 class="tm-card-title">
                                                            <?php
                                                            echo $this->db->get_where('employee_designation', array('designation_id' => $member->designation))
                                                                ->row('designation_name');
                                                            ?>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </main>
            </div>
        </div>
    </div>
</div>
