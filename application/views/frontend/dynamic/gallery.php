<div class="tm-breadcrumbs">
    <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
            <ul class="uk-breadcrumb">
                <li><a href="<?= base_url() ?>">Home</a></li>
                <li class="uk-active"><span>Image Gallery</span></li>
            </ul>
        </div>
    </div>
</div>

<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-3" data-uk-grid-margin="">

            <?php foreach ($images as $key => $image) { ?>
                <a data-uk-lightbox="{group:'gallery1'}"
                   data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:<?php echo $key * 200; ?>}"
                   href="<?= base_url() . $image->image ?>" title="<?php echo $image->caption; ?>">
                    <figure class="uk-overlay uk-overlay-hover">
                        <img alt="<?php echo $image->caption; ?>" class="uk-overlay-scale" height="605"
                             src="<?= base_url() . $image->image ?>"
                             width="605">
                        <div class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-overlay-icon"></div>
                        <div class="tm-overlay-box"></div>
                    </figure>
                </a>

            <?php } ?>
            <div class="uk-width-1-1 uk-grid-margin uk-row-first">

                <?php
                foreach ($links as $link) {
                    echo $link;
                }
                ?>
            </div>


        </div>
    </div>
</div>

<style>
    .uk-pagination > li > a, .uk-pagination > li > span {
        background: #ffffff;
        color: #258bce;
        border: 1px solid #258bce;
        padding: 5px 10px 5px 10px!important;
    }
    .uk-pagination > li > a:hover{
        background: #258bce;
        color: #ffffff;
    }
</style>