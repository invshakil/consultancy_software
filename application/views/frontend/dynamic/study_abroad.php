<div class="tm-breadcrumbs">
    <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
            <ul class="uk-breadcrumb">
                <li><a href="<?= base_url() ?>">Home</a></li>
                <li class="uk-active"><span>Study in <?php echo $country_name;?></span></li>
            </ul>
        </div>
    </div>
</div>

<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-medium-3-10">
                <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#tabs_example2', animation: 'fade'}">
                    <li class="uk-active"><a href="#">Why Study in <?php echo $country_name;?>?</a></li>
                    <li><a href="#">Required Documents</a></li>
                    <li><a href="#">Application Procedure</a></li>
                </ul>
            </div>

            <div class="uk-width-medium-7-10">
                <ul id="tabs_example2" class="uk-switcher uk-margin uk-tab-content">
                    <li><?php
                        if (!$info_by_country)
                        {
                            echo 'Data will be added soon. Please comeback later!';

                        }else{
                            echo $info_by_country->why_study;
                        }
                         ?></li>
                    <li><?php
                        if (!$info_by_country)
                        {
                            echo 'Data will be added soon. Please comeback later!';

                        }else{
                            echo $info_by_country->required_documents;
                        }
                        ?></li>
                    <li><?php
                        if (!$info_by_country)
                        {
                            echo 'Data will be added soon. Please comeback later!';

                        }else{
                            echo $info_by_country->application_procedure;
                        }
                         ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<style>
    .uk-tab-content{
        padding:10px!important;
    }
</style>