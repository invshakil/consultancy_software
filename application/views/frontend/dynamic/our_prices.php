<div class="tm-breadcrumbs">
    <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
            <ul class="uk-breadcrumb">
                <li><a href="<?= base_url() ?>">Home</a></li>
                <li class="uk-active"><span>Our Prices</span></li>
            </ul>
        </div>
    </div>
</div>

<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
            <div class="tm-main uk-width-medium-7-10" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:500}">
                <main id="tm-content" class="tm-content">
                    <h2 class="uk-module-title">Courses & Fees</h2>
                    <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'courses'))->row(); ?>
                    <?php echo $msg->description; ?>

                    <h2 class="uk-module-title">Charges for Services</h2>
                    <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'service_charges'))->row(); ?>
                    <?php echo $msg->description; ?>
                </main>
            </div>

            <div class="tm-main uk-width-medium-3-10" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:1000}">
                <main id="tm-content" class="tm-content">
                    <div class="uk-panel uk-panel-box tm-panel-box-primary-light uk-border-rounded">
                        <h2 class="uk-module-title">Quick Links</h2>
                        <ul class="uk-nav uk-nav-parent-icon uk-nav-side" id="hyperlink" data-uk-nav="{}">
                            <?php $countries = $this->db->get_where('countries_list', array('status' => 1))->result();
                            foreach ($countries as $country) { ?>
                                <li>
                                    <a href="<?php echo base_url() ?>study-abroad/<?php echo str_replace(' ', '-', $country->country_name) ?>">Study
                                        in <?php echo $country->country_name ?></a></li>
                            <?php } ?>

                        </ul>
                    </div>
                    <br>

                    <div class="uk-panel uk-panel-box uk-panel-box-primary uk-border-rounded tm-background-icon">
                        <h3 class="uk-panel-title"><i class="uk-icon-at uk-margin-small-right"></i> Talk to us</h3>
                        <p>Would you like to speak to one of our financial advisers over the phone? <a
                                href="<?php echo base_url()?>contact-us">Contact us</a> and we'll get back to you in the course of the day.
                        </p><a class="uk-button-default uk-button" href="<?php echo base_url()?>contact-us" target="_blank">Get in
                            touch</a>
                    </div>


                </main>


            </div>
        </div>


    </div>
</div>

<style>
    #hyperlink li a{
        border-left: 10px solid #258bce;
        margin: 10px;
        padding-left: 20px;
        text-transform: uppercase;
        font-weight: bolder;
        font-size: 12px;
    }

    #hyperlink li a:hover{
        background-color: #00b3ee;
        color: whitesmoke;
        padding-left: 40px;
    }
</style>

<script>
    $ = jQuery;
    $('table').addClass('uk-table uk-table-hover uk-table-striped').css('width','100%');

</script>