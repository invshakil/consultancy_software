
<!-- breadcrumbs -->
<div class="tm-breadcrumbs">
    <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
            <ul class="uk-breadcrumb">
                <li><a href="<?=base_url();?>">Home</a></li>
                <li><span><a href="<?=base_url()?>about-us">About Us</a></span></li>
                <li class="uk-active"><span>Testimonials</span></li>
            </ul>
        </div>
    </div>
</div>


<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
            <div class="tm-main uk-width-medium-7-10">
                <main id="tm-content" class="tm-content">
                    <article class="uk-article tm-article">
                        <div class="tm-article-wrapper">
                            <h1 class="uk-article-title">Testimonials</h1>
                            <div class="tm-article-content uk-margin-large-bottom uk-margin-top-remove">
                                <div class="tm-article">

                                    <!-- blockquote -->
                                    <blockquote class="tm-testimonial uk-clearfix uk-text-left">
                                        <i class="quote-icon uk-icon-bullhorn"></i>
                                        <p><img alt="John Doe" class="tm-testimonial-avatar uk-align-left uk-margin-top"
                                                height="64" src="<?php echo base_url()?>assets/frontend/images/demo/avatar/client-3-thumb.png" width="64"> Do
                                            not dwell in the past, do not dream of the future, concentrate the mind on
                                            the present moment. <strong>You are what you are.</strong> Accept no one's
                                            definition of your life; define yourself.</p>
                                        <span class="author uk-margin-top">John Doe</span>
                                        <span class="jobtitle">Head of sales, arrowthemes</span>
                                    </blockquote>
                                    <br>
                                    <hr>

                                    <!-- blockquote -->
                                    <blockquote class="tm-testimonial uk-clearfix uk-text-left">
                                        <i class="quote-icon uk-icon-trophy"></i>
                                        <p><img alt="Jane Doe" class="tm-testimonial-avatar uk-align-left uk-margin-top"
                                                height="64" src="<?php echo base_url()?>assets/frontend/images/demo/avatar/client-2-thumb.png" width="64">
                                            Believe in yourself! Have faith in your abilities! Without a humble but
                                            reasonable confidence in your own powers <strong>you cannot be
                                                successful</strong> or happy.</p>
                                        <span class="author uk-margin-top">Jane Doe</span>
                                        <span class="jobtitle">Marketing Director, arrowthemes</span>
                                    </blockquote>


                                </div>
                            </div>
                        </div>
                    </article>
                </main>
            </div>

            <aside class="tm-sidebar-b uk-width-medium-3-10">
                <div class="uk-panel uk-panel-box tm-panel-box-primary-light uk-border-rounded">
                    <ul class="uk-nav uk-nav-parent-icon uk-nav-side" data-uk-nav="{}">
                        <li ><a href="<?php echo base_url()?>about-us">About Us</a></li>
                        <li><a href="<?php echo base_url()?>our-team">Our Team</a></li>
                        <li class="uk-active"><a href="<?php echo base_url()?>client-testimonials">Client Testimonials</a></li>
                        <li><a href="<?php echo base_url()?>contact-us">Contact Us</a></li>
                    </ul>
                </div>

                <div class="uk-panel uk-panel-box uk-panel-box-primary uk-border-rounded tm-background-icon">
                    <h3 class="uk-panel-title"><i class="uk-icon-at uk-margin-small-right"></i> Talk to us</h3>
                    <p>Would you like to speak to one of our financial advisers over the phone? <a
                            href="<?php echo base_url()?>contact-us">Contact us</a> and we'll get back to you in the course of the day.
                    </p><a class="uk-button-default uk-button" href="<?php echo base_url()?>contact-us" target="_blank">Get in
                        touch</a>
                </div>

            </aside>
        </div>
    </div>
</div>
