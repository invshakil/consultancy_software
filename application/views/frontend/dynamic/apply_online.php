<div class="tm-breadcrumbs">
    <div class="uk-container uk-container-center">
        <div class="uk-hidden-small">
            <ul class="uk-breadcrumb">
                <li><a href="<?= base_url() ?>">Home</a></li>
                <li class="uk-active"><span>Apply Online</span></li>
            </ul>
        </div>
    </div>
</div>

<div id="tm-main" class="tm-block-main uk-block uk-block-default">
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
            <div class="tm-main uk-width-medium-7-10" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:500}">
                <main id="tm-content" class="tm-content">
                    <h2 class="uk-module-title">Apply Online</h2>
                    <form role="form" action="<?php echo base_url()?>website/save_client_info" method="post" class="uk-form uk-form-stacked">
                        <div class="uk-grid uk-grid-width-medium-1-1 uk-grid-width-1-1">

                            <div>
                                <div id="alert-msg-contact"
                                     class="alert-msg uk-margin-bottom"></div>
                            </div>

                            <!-- name field -->
                            <div class="uk-form-icon uk-grid-margin">
                                <i class="uk-icon-user"></i>
                                <input type="text" class="uk-width-1-1" name="name"
                                       placeholder="Name" size="30" required="required">
                            </div>

                            <!-- name field -->
                            <div class="uk-form-icon uk-grid-margin">
                                <i class="uk-icon-user"></i>
                                <input type="text" class="uk-width-1-1" name="phone"
                                       placeholder="Phone Number" size="30" required="required">
                            </div>

                            <!-- email field -->
                            <div class="uk-form-icon uk-grid-margin">
                                <i class="uk-icon-envelope"></i>
                                <input type="email" class="uk-width-1-1" name="email"
                                       placeholder="Your email" size="30" required="required">
                            </div>

                            <!-- subject field -->
                            <div class="uk-form-icon uk-grid-margin">
                                <i class="uk-icon-bookmark"></i>
                                <input type="text" class="uk-width-1-1 optional"
                                       name="country" placeholder="Interested Country" size="60">
                            </div>

                            <!-- message field -->
                            <div class="uk-grid-margin">
                                                    <textarea class="uk-width-1-1" name="message" cols="30"
                                                              rows="3" placeholder="Your message"
                                                              required="required"></textarea>
                            </div>

                            <!-- submit button -->
                            <div class="form-actions">
                                <button class="uk-button uk-button-primary"
                                        type="submit">Submit
                                </button>
                            </div>

                            <div id="message"></div>
                        </div>
                    </form>
                </main>
            </div>

            <div class="tm-main uk-width-medium-3-10" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:1000}">
                <main id="tm-content" class="tm-content">
                    <div class="uk-panel uk-panel-box tm-panel-box-primary-light uk-border-rounded">
                        <h2 class="uk-module-title">Quick Links</h2>
                        <ul class="uk-nav uk-nav-parent-icon uk-nav-side" id="hyperlink" data-uk-nav="{}">
                            <?php $countries = $this->db->get_where('countries_list', array('status' => 1))->result();
                            foreach ($countries as $country) { ?>
                                <li>
                                    <a href="<?php echo base_url() ?>study-abroad/<?php echo str_replace(' ', '-', $country->country_name) ?>">Study
                                        in <?php echo $country->country_name ?></a></li>
                            <?php } ?>

                        </ul>
                    </div>
                    <br>

                    <div class="uk-panel uk-panel-box uk-panel-box-primary uk-border-rounded tm-background-icon">
                        <h3 class="uk-panel-title"><i class="uk-icon-at uk-margin-small-right"></i> Talk to us</h3>
                        <p>Would you like to speak to one of our financial advisers over the phone? <a
                                href="<?php echo base_url() ?>contact-us">Contact us</a> and we'll get back to you in
                            the course of the day.
                        </p><a class="uk-button-default uk-button" href="<?php echo base_url() ?>contact-us"
                               target="_blank">Get in
                            touch</a>
                    </div>


                </main>


            </div>
        </div>


    </div>
</div>

<style>
    #hyperlink li a {
        border-left: 10px solid #258bce;
        margin: 10px;
        padding-left: 20px;
        text-transform: uppercase;
        font-weight: bolder;
        font-size: 12px;
    }

    #hyperlink li a:hover {
        background-color: #00b3ee;
        color: whitesmoke;
        padding-left: 40px;
    }
</style>