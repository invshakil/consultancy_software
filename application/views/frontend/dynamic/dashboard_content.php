<!-- SLIDER -->
<div class="tm-block-hero uk-block uk-block-default tm-block-fullwidth tm-grid-collapse" id="tm-hero">
    <div class="uk-container uk-container-center">
        <section class="tm-hero uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
            <div class="uk-width-1-1">
                <div class="uk-panel">

                    <!-- slideshow -->
                    <div class="tm-slideshow-sandal uk-slidenav-position"
                         data-uk-slideshow="{autoplay:true, animation: 'random-fx', pauseOnHover: true, duration: 300, autoplayInterval: 15000, kenburns: false, slices: 25}">
                        <ul class="uk-slideshow uk-overlay-active">

                            <?php $sliders = $this->db->limit(4)->order_by('image_id', 'desc')
                                ->get_where('image_gallery', array('image_type' => 1, 'visibility_status' => 1))
                                ->result();
                            foreach ($sliders as $slider) {
                                ?>
                                <!-- slide 1 -->
                                <li>
                                    <img alt="<?php echo $slider->caption ?>" src="<?= base_url() . $slider->image; ?>"
                                         width="1400" height="600">
                                    <div class="uk-overlay-panel uk-flex uk-flex-middle uk-overlay-slide-left">
                                        <div>
                                            <h2 id="caption"><?php echo $slider->caption ?></h2>
                                            <div class="uk-margin" id="details">
                                                <p><?php echo substr($slider->description, 0, 300) . '...' ?>
                                                </p>
                                            </div>
                                            <a class="uk-button-primary uk-button-large uk-button"
                                               href="<?= base_url() ?>details-information/<?php echo str_replace(' ', '-', $slider->caption) ?>"
                                               target="_self">Learn more <i
                                                    class="uk-icon-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </li>

                            <?php } ?>


                        </ul>

                        <!-- slide navigation buttons -->
                        <div class="uk-margin">
                            <ul class="uk-dotnav uk-hidden-touch">
                                <li data-uk-slideshow-item="0"><a href=""></a></li>
                                <li data-uk-slideshow-item="1"><a href=""></a></li>
                                <li data-uk-slideshow-item="2"><a href=""></a></li>
                                <li data-uk-slideshow-item="3"><a href=""></a></li>
                            </ul>
                        </div>

                        <!-- slide navigation arrows -->
                        <div class="tm-slidenav">
                            <a class="uk-slidenav uk-slidenav-previous uk-hidden-touch"
                               data-uk-slideshow-item="previous" href=""></a>
                            <a class="uk-slidenav uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next"
                               href=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- ABOUT OUR SERVICE -->
<div class="tm-block-top-b uk-block uk-block-secondary tm-block-fullwidth tm-grid-collapse" id="tm-top-b">
    <div class="uk-container uk-container-center">
        <section class="tm-top-b uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
            <div class="uk-width-1-1">
                <div class="uk-panel">

                    <!-- slideshow panel -->
                    <div class="tm-slideshow-panel-sandal" id="wk-1">
                        <div class="uk-panel uk-panel uk-padding-remove uk-overflow-hidden">
                            <div class="uk-grid uk-grid-collapse uk-flex-middle" data-uk-grid-margin="">

                                <!-- image slideshow -->
                                <div class="uk-width-medium-1-2 uk-text-center uk-row-first">
                                    <div class="uk-slidenav-position" data-uk-slideshow="{animation: 'swipe'}">
                                        <ul class="uk-slideshow">
                                            <li>
                                                <!-- ORGANIZATION IMAGE-->
                                                <?php
                                                $org = $this->db->get_where('message_from_organization', array('info_type' => 'image'))->row();
                                                if ($org->description != '') { ?>
                                                    <img src="<?php echo base_url() . $org->description ?>"
                                                         alt="message from organization"
                                                         width="650" height="420">
                                                <?php } else { ?>
                                                    <img alt="demo"
                                                         src="<?= base_url(); ?>assets/frontend/images/demo/default/content/content-1.jpg"
                                                         width="650" height="420">
                                                <?php } ?>
                                            </li>

                                            <li>
                                                <!-- ORGANIZATION IMAGE-->
                                                <?php
                                                $ceo = $this->db->get_where('message_from_ceo', array('info_type' => 'image'))->row();
                                                if ($ceo->description != '') { ?>
                                                    <img src="<?php echo base_url() . $ceo->description ?>"
                                                         alt="message from organization"
                                                         width="650" height="420">
                                                <?php } else { ?>
                                                    <img alt="demo"
                                                         src="<?= base_url(); ?>assets/frontend/images/demo/default/content/content-1.jpg"
                                                         width="650" height="420">
                                                <?php } ?>
                                            </li>
                                        </ul>

                                        <!-- slideshow navigation arrows -->
                                        <a class="uk-slidenav uk-slidenav-previous uk-hidden-touch"
                                           data-uk-slideshow-item="previous" href="#"></a>
                                        <a class="uk-slidenav uk-slidenav-next uk-hidden-touch"
                                           data-uk-slideshow-item="next" href="#"></a>

                                        <div
                                            class="uk-position-bottom uk-margin-large-bottom uk-margin-large-left uk-margin-large-right">

                                            <!-- slideshow navigation buttons -->
                                            <ul class="uk-dotnav uk-dotnav-contrast uk-flex-center uk-margin-bottom-remove">
                                                <li data-uk-slideshow-item="0"><a href=""></a></li>
                                                <li data-uk-slideshow-item="1"><a href=""></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <!-- content slideshow -->
                                <div class="uk-width-medium-1-2">
                                    <div class="uk-panel-body uk-margin-left uk-text-left" data-uk-slideshow="">
                                        <ul class="uk-slideshow">

                                            <!-- slide 1 -->
                                            <li>
                                                <div class="uk-width-xlarge-9-10">
                                                    <h3 class="uk-module-title uk-h2 uk-margin-top-remove"><a
                                                            class="uk-link-reset" href="#">Message From Organization</a></h3>
                                                    <div class=" uk-margin-top">
                                                        <?php echo $org_msg = $this->db->get_where('message_from_organization', array('info_type' => 'message'))->row('description'); ?>

                                                    </div>

                                                </div>
                                            </li>

                                            <!-- slide 2 -->
                                            <li>
                                                <div class="uk-width-xlarge-9-10">
                                                    <h3 class="uk-module-title uk-h2 uk-margin-top-remove"><a
                                                            class="uk-link-reset" href="#">Message From CEO</a></h3>
                                                    <div class=" uk-margin-top">
                                                        <?php echo $ceo_msg = $this->db->get_where('message_from_ceo', array('info_type' => 'message'))->row('description'); ?>

                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<!-- Why Choose Us? -->
<div class="tm-block-top-d uk-block uk-block-default" id="tm-top-d">
    <div class="uk-container uk-container-center">
        <section class="tm-top-d uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'> div > .uk-panel'}">
            <div class="uk-width-1-1">
                <div class="uk-panel">
                    <div class="uk-text-center">
                        <h2 class="uk-module-title-alt">Why choose us?</h2>
                    </div>
                    <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-4"
                         data-uk-grid-margin="">

                        <?php $why_choose_us = $this->db->order_by('desc_id','desc')->get('why_choose_us')->result();
                        $i = 0;
                        foreach ($why_choose_us as $row) {?>
                        <!-- block -->
                        <div class="uk-block">
                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:<?php echo $i+100?>}">
                                <h2 class="fa fa-clock uk-module-title uk-h2"><?php echo $row->title;?></h2>
                                <div class="uk-flex">
                                    <div class="tm-block-icon uk-icon-7s-alarm"></div>
                                    <div class="tm-block-content">
                                        <?php echo $row->message;?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php } ?>


                    </div>
                </div>
            </div>
        </section>
    </div>
</div>




<!-- APPLY ONLINE -->
<div class="tm-block-spotlight uk-block uk-block-primary tm-float-box tm-overlay-5" id="tm-spotlight">
    <div class="uk-container uk-container-center">
        <section class="tm-spotlight uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}">
            <div class="uk-width-1-1">
                <div class="uk-panel">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-8-10">
                            <blockquote class="uk-clearfix">
                                <p>We offer world-class consultancy and un-matched support. Try us.</p><span
                                    class="author"></span>
                            </blockquote>
                        </div>
                        <div class="uk-width-medium-2-10 uk-flex uk-flex-middle uk-flex-center">
                            <a class="uk-button-default uk-button-large uk-button"
                               href="<?php echo base_url() ?>apply-online"
                               target="_blank">Apply Online <i class="uk-icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>



<!-- TESTIMONIALS -->
<div class="tm-block-main uk-block uk-block-default" id="tm-main">
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-margin="" data-uk-grid-match="">
            <div class="tm-main uk-width-medium-1-1">
                <div class="tm-content-grid" id="tm-content-grid">
                    <div class="tm-grid">
                        <div class="">
                            <div class="uk-text-center">
                                <h3 class="uk-module-title-alt uk-margin-large-bottom">Testimonials</h3><br>

                                <!-- slideshow scroller -->
                                <div class="tm-slideshow-scroller uk-slidenav-position"
                                     data-uk-slideshow="{autoplay:true, animation: 'fade', pauseOnHover: true, duration: 500, autoplayInterval: 10000, kenburns: false, slices: 25}">
                                    <ul class="uk-slideshow uk-overlay-active">

                                        <?php $testimonials = $this->db->get_where('testimonials',array('status'=>1))->result();
                                        foreach ($testimonials as $testimonial) {?>
                                        <!-- testimonial -->
                                        <li>
                                            <blockquote class="tm-testimonial uk-clearfix uk-text-left">
                                                <i class="quote-icon uk-icon-7s-chat"></i>
                                                <p><img alt="Gary Long"
                                                        class="tm-testimonial-avatar uk-align-left uk-margin-top"
                                                        height="64"
                                                        src="<?= base_url().$testimonial->image; ?>"
                                                        width="64">
                                                <?php echo $testimonial->message; ?></p>
                                                <span class="author uk-margin-top"><?php echo $testimonial->name; ?></span>
                                            </blockquote>
                                        </li>
                                        <?php } ?>


                                    </ul>

                                    <!-- slideshow navigation button -->
                                    <div class="uk-margin">
                                        <ul class="uk-dotnav uk-flex-center uk-hidden-touch">
                                            <li data-uk-slideshow-item="0"><a href=""></a></li>
                                            <li data-uk-slideshow-item="1"><a href=""></a></li>
                                            <li data-uk-slideshow-item="2"><a href=""></a></li>
                                        </ul>
                                    </div>

                                    <!-- slideshow navigation arrows -->
                                    <div class="tm-slidenav uk-flex uk-flex-right uk-flex-middle">
                                        <a class="uk-slidenav uk-slidenav-previous uk-hidden-touch"
                                           data-uk-slideshow-item="previous" href=""></a>
                                        <a class="uk-slidenav uk-slidenav-next uk-hidden-touch"
                                           data-uk-slideshow-item="next" href=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #caption, #details {
        padding: 7px;
        background: rgba(0, 0, 0, 0.6)!important;
        color: #fff!important;
        padding-left: 15px;
    }
</style>