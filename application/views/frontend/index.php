<!DOCTYPE html>
<html dir="ltr">
<head>
    <!--  HEADER SCRIPT  -->
    <?php include_once 'includes/header_script.php';?>
</head>

<body id="tm-container">
<div class="tm-container">

    <!-- HEADER, Navigation -->
    <?php include_once 'includes/header_nav.php';?>

    <!-- PRE-LOADER -->
    <div class="tm-preload">
        <div class="spinner"></div>
    </div>

    <?php
    $this->session->flashdata('message');
    ?>

    <!--  MAIN CONTENT  -->
    <?php echo $main_content; ?>
    <!--  MAIN CONTENT  -->

    <!-- FOOTER, NEWSLETTER, MODAL -->
    <?php include_once 'includes/bottom_content.php'?>
</div>
    <!--  FOOTER SCRIPT  -->
    <?php include_once 'includes/footer_script.php'; ?>
</body>
</html>