<!-- to top scroller -->
<div class="uk-sticky-placeholder">
    <div data-uk-smooth-scroll data-uk-sticky="{top:-500}">
        <a class="tm-totop-scroller uk-animation-slide-bottom" href="#"></a>
    </div>
</div>

<!-- toolbar -->
<div id="tm-toolbar" class="tm-toolbar uk-hidden-small">
    <div class="uk-container uk-container-center uk-clearfix">

        <!-- toolbar left -->
        <div class="uk-float-left">
            <div>
                <ul class="uk-subnav uk-subnav-line">
                    <li><a href="<?php base_url() ?>apply-online" target="_blank">Apply Online</a></li>
                    <li><a class="tm-modal-link" href="#modal-a" data-uk-modal="{center:true}">Login</a></li>
                </ul>
            </div>
        </div>

        <!-- toolbar right -->
        <div class="uk-float-right">

            <div>

                <?php $settings_facebook = $this->db->where('info_type', 'facebook')
                    ->get('system_settings')->row('description'); ?>
                <a target="_blank" href="<?php echo $settings_facebook; ?>"><i id="fa" class="fa fa-facebook"></i> </a>

                <?php $settings_google = $this->db->where('info_type', 'google')
                    ->get('system_settings')->row('description'); ?>
                <a target="_blank" href="<?php echo $settings_google; ?>"><i id="fa" class="fa fa-google-plus"></i> </a>

                <?php $settings_twitter = $this->db->where('info_type', 'twitter')
                    ->get('system_settings')->row('description'); ?>
                <a target="_blank" href="<?php echo $settings_twitter; ?>"><i id="fa" class="fa fa-twitter"></i> </a>

                <?php $settings_linkedin = $this->db->where('info_type', 'linkedin')
                    ->get('system_settings')->row('description'); ?>
                <a target="_blank" href="<?php echo $settings_linkedin; ?>"><i id="fa" class="fa fa-linkedin"></i> </a>

            </div>

            <div class="uk-hidden-small">
                <ul class="uk-list list-icons">
                    <li><i class="uk-icon-clock-o"></i> <?php echo $system_working_hours ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- LOGO AND NAVIGATION -->
<div class="tm-header tm-header-right" data-uk-sticky>
    <div class="uk-container uk-container-center">
        <div class="uk-flex uk-flex-middle uk-flex-space-between">

            <!-- logo -->
            <a class="tm-logo uk-hidden-small" href="<?= base_url() ?>">
                <img src="<?= base_url() . $system_logo; ?>" style="max-height: 50px" class="img-responsive" alt="demo">
            </a>

            <!-- small logo -->
            <a class="tm-logo-small uk-visible-small" href="<?= base_url() ?>">
                <img src="<?= base_url() . $system_logo; ?>" style="max-height: 50px" class="img-responsive" alt="demo">
            </a>

            <!-- main menu -->
            <div class="uk-flex uk-flex-right">
                <div class="uk-hidden-small">
                    <nav class="tm-navbar uk-navbar tm-navbar-transparent">
                        <div class="uk-container uk-container-center">
                            <ul class="uk-navbar-nav uk-hidden-small">

                                <!-- home menu  -->
                                <li class="uk-parent <?php if (current_url() == base_url()) {
                                    echo 'uk-active';
                                } ?>" data-uk-dropdown>
                                    <a href="<?= base_url() ?>">Home</a>
                                </li>

                                <!-- About menu  -->
                                <li class="uk-parent <?php if (current_url() == base_url() . 'about-us') {
                                    echo 'uk-active';
                                } ?>" data-uk-dropdown>
                                    <a href="<?= base_url() ?>about-us">About Us</a>
                                </li>

                                <!-- Study Abroad -->
                                <li class="uk-parent <?php if (current_url() == base_url() . 'study-abroad/') {
                                    echo 'uk-active';
                                } ?>" data-uk-dropdown><a href="#">Study Abroad <i class="uk-icon-caret-down"></i></a>
                                    <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1">
                                        <div class="uk-grid uk-dropdown-grid">
                                            <div class="uk-width-1-1">
                                                <ul class="uk-nav uk-nav-navbar">
                                                    <?php $countries = $this->db->get_where('countries_list', array('status' => 1))->result();
                                                    foreach ($countries as $country) { ?>
                                                        <li>
                                                            <a href="<?php echo base_url() ?>study-abroad/<?php echo str_replace(' ', '-', $country->country_name) ?>">Study
                                                                in <?php echo $country->country_name ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <!-- Our Prices -->
                                <li><a href="<?php echo base_url() ?>our-prices">Our Prices</a></li>

                                <!-- Gallery -->
                                <li><a href="<?= base_url() ?>gallery">Gallery</a></li>

                                <!-- Apply Online -->
                                <li><a href="<?= base_url() ?>apply-online">Apply Online</a></li>

                                <!-- Contact -->
                                <li><a href="<?= base_url() ?>contact-us">Contact</a></li>

                            </ul>
                        </div>
                    </nav>
                </div>

                <!-- offcanvas nav icon -->
                <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                <!-- search button -->
                <div class="uk-navbar-content tm-navbar-more uk-visible-large uk-margin-left"
                     data-uk-dropdown="{mode:'click'}">
                    <a class="uk-link-reset"></a>
                    <div class="uk-dropdown uk-dropdown-flip">
                        <form action="#" class="uk-search" data-uk-search="" id="search-page" method="post"
                              name="search-box">
                            <input class="uk-search-field" name="searchword" placeholder="search..." type="text">
                            <input name="task" type="hidden" value="search">
                            <input name="option" type="hidden" value=""> <input name="Itemid" type="hidden"
                                                                                value="502">
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
