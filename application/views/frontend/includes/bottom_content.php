
<!-- CONTACT AND OFFICE HOURS -->
<div class="tm-headerbar-container">
    <div class="uk-container uk-container-center">
        <div class="tm-headerbar">
            <div class="">
                <div class="uk-grid" data-uk-grid-margin="">
                    <div class="uk-width-medium-1-4 uk-width-small-1-2">
                        <div class="uk-flex">
                            <div class="tm-block-icon uk-icon-comments-o"></div>
                            <div class="tm-block-content">
                                <h3>Talk to us now</h3>
                                <p><?php echo $system_contact; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-4 uk-width-small-1-2">
                        <div class="uk-flex">
                            <div class="tm-block-icon uk-icon-map-signs"></div>
                            <div class="tm-block-content">
                                <h3>Visit our offices</h3>
                                <p><?php echo $system_address; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-4 uk-width-small-1-2">
                        <div class="uk-flex">
                            <div class="tm-block-icon uk-icon-envelope-o"></div>
                            <div class="tm-block-content">
                                <h3>Send us an email</h3>
                                <p><?php echo $system_email; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-4 uk-width-small-1-2">
                        <div class="uk-flex">
                            <div class="tm-block-icon uk-icon-clock-o"></div>
                            <div class="tm-block-content">
                                <h3>Our Office Hours</h3>
                                <p><?php echo $system_working_hours ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- NEWSLETTER -->
<div class="tm-block-bottom-c uk-block uk-block-primary" id="tm-bottom-c">
    <div class="uk-container uk-container-center">
        <section class="tm-bottom-c uk-grid" data-uk-grid-margin=""
                 data-uk-grid-match="{target:'> div > .uk-panel'}">
            <div class="uk-width-1-1">
                <div class="uk-panel">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-1-2">
                            <h3 class="uk-margin-small-top">Sign up for our newsletter to get the latest news</h3>
                        </div>
                        <div class="uk-width-medium-1-2">

                            <!-- subscription form -->
                            <form action="<?php echo base_url() ?>website/save_subscriber" method="post"
                                  class="uk-form">

                                <div class="uk-grid uk-grid-small" data-uk-grid-margin>

                                    <div class="uk-width-medium-2-5">
                                        <div><input type="text" placeholder="Your name" name="name"
                                                    class="uk-width-1-1" required="required"></div>
                                    </div>

                                    <div class="uk-width-medium-2-5">
                                        <div><input type="email" placeholder="Email address" name="email"
                                                    class="uk-width-1-1" required="required"></div>
                                    </div>

                                    <div class="uk-width-medium-1-5">
                                        <div class="form-group uk-margin-remove">
                                            <button type="submit"
                                                    class="uk-button uk-button-default">Subscribe
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- FOOTER -->
<div class="tm-block-bottom-d uk-block uk-block-secondary tm-overlay-6" id="tm-bottom-d">
    <div class="uk-container uk-container-center">
        <section class="tm-bottom-d uk-grid" data-uk-grid-margin=""
                 data-uk-grid-match="{target:'> div > .uk-panel'}">

            <!-- block -->
            <div class="uk-width-1-1 uk-width-medium-1-4">
                <div class="uk-panel">
                    <img alt="logo" height="40" src="<?= base_url() . $system_logo; ?>" width="140">
                    <p><?php echo $system_name; ?><br><br>
                        <?php
                        echo $system_address;
                        ?><br/>
                        <?php echo $system_contact; ?>
                        <br>
                        <?php echo $system_email; ?>
                        <br>
                </div>
            </div>

            <!-- block -->
            <div class="uk-width-1-1 uk-width-medium-1-4">
                <div class="uk-panel">
                    <h3 class="uk-h3 uk-module-title uk-margin-bottom">Social Links</h3>
                    <div>
                        <ul class="uk-list list-icons">
                            <li style="min-height: 50px!important;">
                                <?php $settings_facebook = $this->db->where('info_type', 'facebook')
                                    ->get('system_settings')->row('description'); ?>
                                <a target="_blank" href="<?php echo $settings_facebook; ?>"><i id="fa"
                                                                                               class="fa fa-facebook"></i>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Follow on Facebook</a>
                            </li>
                            <li style="min-height: 50px!important;">
                                <?php $settings_google = $this->db->where('info_type', 'google')
                                    ->get('system_settings')->row('description'); ?>
                                <a target="_blank" href="<?php echo $settings_google; ?>"><i id="fa"
                                                                                             class="fa fa-google-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Follow on Google Plus</a>
                            </li>
                            <li style="min-height: 50px!important;">
                                <?php $settings_twitter = $this->db->where('info_type', 'twitter')
                                    ->get('system_settings')->row('description'); ?>
                                <a target="_blank" href="<?php echo $settings_twitter; ?>"><i id="fa"
                                                                                              class="fa fa-twitter"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Follow on Twitter</a>
                            </li>
                            <li style="min-height: 50px!important;">
                                <?php $settings_linkedin = $this->db->where('info_type', 'linkedin')
                                    ->get('system_settings')->row('description'); ?>
                                <a target="_blank" href="<?php echo $settings_linkedin; ?>"><i id="fa"
                                                                                               class="fa fa-linkedin"></i>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Follow on Linkedin</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- block -->
            <div class="uk-width-1-1 uk-width-medium-1-4">
                <div class="uk-panel">
                    <h3 class="uk-h3 uk-module-title uk-margin-bottom">Important Links</h3>
                    <ul class="uk-list list-icons">
                        <li><i class="uk-icon-angle-right"></i><a href="<?php base_url() ?>our-team">Our Team</a></li>
                        <li><i class="uk-icon-angle-right"></i><a href="<?php base_url() ?>our-prices">Our Charges</a>
                        </li>
                        <li><i class="uk-icon-angle-right"></i><a href="<?php base_url() ?>client-testimonials">Client
                                Testimonials</a></li>
                        <li><i class="uk-icon-angle-right"></i><a href="<?php base_url() ?>gallery">Our Gallery</a></li>
                        <li><i class="uk-icon-angle-right"></i><a href="<?php base_url() ?>apply-online"
                                                                  target="_blank">Apply Online</a></li>
                        <li><i class="uk-icon-angle-right"></i><a href="<?php base_url() ?>contact-us">Let us know your
                                query</a></li>

                    </ul>
                </div>
            </div>

            <!-- block -->
            <div class="uk-width-1-1 uk-width-medium-1-4">
                <div class="uk-panel">
                    <h3 class="uk-h3 uk-module-title uk-margin-bottom">About us</h3>
                    <div style="text-align: justify">
                        <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'about_us'))->row(); ?>

                        <?php echo substr($msg->description, 0, 200) . '....<a href=' . base_url() . 'about-us>Learn More</a>'; ?>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>

<!-- COPYRIGHT -->
<div class="tm-block-bottom-e uk-block uk-block-secondary" id="tm-bottom-e">
    <div class="uk-container uk-container-center">
        <section class="tm-bottom-e uk-grid" data-uk-grid-margin=""
                 data-uk-grid-match="{target:'> div > .uk-panel'}">
            <div class="uk-width-1-1 uk-width-medium-1-2">
                <div class="uk-panel">
                    <p><?php echo date('Y');?> &copy; <a href="https://techno-71.com"
                                           target="_blank">Techno-71</a>
                    </p>
                </div>
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-2">
                <div class="uk-panel">
                    <div class="uk-text-right">
                        <a class="uk-icon-button uk-icon-facebook" href="#" target="_blank"></a>
                        <a class="uk-icon-button uk-icon-twitter" href="#" target="_blank"></a>
                        <a class="uk-icon-button uk-icon-dribbble" href="#" target="_blank"></a>
                        <a class="uk-icon-button uk-icon-behance" href="#" target="_blank"></a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- modal log-in -->
<div class="uk-modal" id="modal-a">
    <div class="uk-modal-dialog uk-panel-box uk-panel uk-panel-header uk-modal-dialog-small">
        <a class="uk-modal-close uk-close"></a>
        <div class="">
            <h3 class="uk-panel-title">Sign In</h3>
            <form action="<?php echo base_url() ?>admin_login/ajax_login" class="uk-form" method="post">
                <div class="uk-form-row">
                    <input class="uk-width-1-1" name="email" placeholder="Email" size="18" type="text">
                </div>
                <div class="uk-form-row">
                    <input class="uk-width-1-1" name="password" placeholder="Password" size="18" type="password">
                </div>
                <div class="uk-form-row">
                    <label for="remember-me">Remember Me</label> <input checked id="remember-me" name="remember"
                                                                        type="checkbox" value="yes">
                </div>
                <div class="uk-form-row">
                    <button class="uk-button uk-button-primary" name="Submit" type="submit" value="Log in">Log in
                    </button>
                </div>
                <ul class="uk-list uk-margin-bottom-remove">
                    <li><a href="">Forgot your password?</a></li>
                </ul>

            </form>
        </div>
    </div>
</div>

<!-- offcanvas menu -->
<div id="offcanvas" class="uk-offcanvas">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
        <ul class="uk-nav uk-nav-parent-icon uk-nav-offcanvas" data-uk-nav>

            <!-- home menu  -->
            <li class="uk-parent <?php if (current_url() == base_url()) {
                echo 'uk-active';
            } ?>" >
                <a href="<?= base_url() ?>">Home</a>
            </li>

            <!-- About menu  -->
            <li class="uk-parent <?php if (current_url() == base_url() . 'about-us') {
                echo 'uk-active';
            } ?>" >
                <a href="<?= base_url() ?>about-us">About Us</a>
            </li>

            <!-- Study Abroad -->
            <li class="uk-parent <?php if (current_url() == base_url() . 'study-abroad/') {
                echo 'uk-active';
            } ?>"><a href="#">Study Abroad </a>

                <ul class="uk-nav-sub">
                    <?php $countries = $this->db->get_where('countries_list', array('status' => 1))->result();
                    foreach ($countries as $country) { ?>
                        <li>
                            <a href="<?php echo base_url() ?>study-abroad/<?php echo str_replace(' ', '-', $country->country_name) ?>">Study
                                in <?php echo $country->country_name ?></a></li>
                    <?php } ?>
                </ul>
            </li>

            <!-- Our Prices -->
            <li><a href="<?php echo base_url() ?>our-prices">Our Prices</a></li>

            <!-- Gallery -->
            <li><a href="<?= base_url() ?>gallery">Gallery</a></li>

            <!-- Apply Online -->
            <li><a href="<?= base_url() ?>apply-online">Apply Online</a></li>

            <!-- Contact -->
            <li><a href="<?= base_url() ?>contact-us">Contact</a></li>
        </ul>
    </div>
</div>
