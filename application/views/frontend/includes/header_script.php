<!-- meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<meta name="author" content="">

<!-- // RETURNS PAGE TITLE | COMPANY NAME // -->

<title><?php echo $title.$system_name; ?></title>

<!-- fav icon -->
<link href="<?=base_url();?>assets/frontend/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
<link rel="apple-touch-icon" href="<?=base_url();?>assets/frontend/images/apple-touch-icon.png">

<!-- css -->
<link rel="stylesheet" href="<?=base_url();?>assets/frontend/css/plyr.css">
<link rel="stylesheet" href="<?=base_url();?>assets/frontend/css/default/bootstrap.css">
<link rel="stylesheet" href="<?=base_url();?>assets/frontend/css/default/theme.css">
<link rel="stylesheet" href="<?=base_url();?>assets/frontend/css/custom.css">

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<!-- jquery -->
<script src="<?=base_url();?>assets/frontend/js/jquery/jquery.min.js" type="text/javascript"></script>