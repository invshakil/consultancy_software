<!-- uikit -->
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/uikit.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/accordion.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/autocomplete.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/datepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/grid.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/lightbox.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/parallax.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/pagination.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/slider.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/slideset.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/slideshow.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/slideshow-fx.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/sticky.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/timepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/vendor/uikit/js/components/tooltip.min.js"
        type="text/javascript"></script>

<!-- theme -->
<script src="<?= base_url(); ?>assets/frontend/js/theme.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/frontend/js/plyr.js" type="text/javascript"></script>

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>

    <?php if($this->session->flashdata('success')){ ?>
    toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php }else if($this->session->flashdata('error')){  ?>
    toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php }else if($this->session->flashdata('warning')){  ?>
    toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php }else if($this->session->flashdata('info')){  ?>
    toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    <?php } ?>

</script>

<style>
    #fa{
        width: 20px;
        height: 20px;
        /* line-height: 53px; */
        font-size: 20px;
        transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        line-height: 20px;
        text-align: center;
        display: inline-block;
        background-color: #258BCE;
        color: #ffffff;
        padding: 8px;
    }

</style>