<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title><?php echo ucwords(strtolower($page_name));?></title>

    <?php echo $top;?>


</head>
<body class="page-body page-fade skin-cafe" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <?php echo $navigation;?>

    <div class="main-content">

        <div class="row">

            <!-- Profile Info and Notifications -->
            <div class="col-md-3 col-sm-3 clearfix">

                <ul class="user-info pull-left pull-none-xsm">

                    <!-- Profile Info -->
                    <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url().$this->session->userdata('image');?>" alt="" class="img-circle" width="44" />
                            <?php echo $this->session->userdata('admin_name');?>
                        </a>

                        <ul class="dropdown-menu">

                            <!-- Reverse Caret -->
                            <li class="caret"></li>

                            <!-- Profile sub-links -->
                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_users/<?php echo $this->session->admin_id; ?>');">
                                    <i class="entypo-user"></i>
                                    Edit Account
                                </a>
                            </li>


                        </ul>
                    </li>

                </ul>


            </div>

            <div class="col-md-6 col-sm-6 clearfix">

                <h2><?php echo ucwords(strtolower($page_name));?></h2>
            </div>


            <!-- Raw Links -->
            <div class="col-md-3 col-sm-3 clearfix hidden-xs">

                <ul class="list-inline links-list pull-right">



                    <li class="sep"></li>

                    <li>
                        <a href="<?php echo base_url();?>admin_login/logout">
                            Log Out <i class="entypo-logout right"></i>
                        </a>
                    </li>
                </ul>

            </div>

        </div>

        <hr />





        <div class="col-md-12">

        <?php echo $main_content;?>
        </div>

        <!-- Footer -->
        <footer class="main">

            &copy; 2014 <strong>Software</strong> Developed by <a href="http://techno-71.com" target="_blank">Techno-71</a>

        </footer>
    </div>




</div>

<?php //echo $modal;?>
<?php include 'modal.php';?>
<?php echo $bottom;?>
</body>
</html>