<link rel="stylesheet" href="<?php echo base_url()?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/neon-core.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/neon-theme.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/neon-forms.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/skins/facebook.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="<?php echo base_url()?>assets/js/jquery-1.11.0.min.js"></script>
<script>$.noConflict();</script>

<!--[if lt IE 9]><script src="<?php echo base_url()?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    .panel-danger > .panel-heading {
        color: #f5f5f6;
        background-color: #cc2424;
        border-color: #cc2424;
    }

    .panel-danger > .panel-heading > .panel-options > a.bg {
        background-color: #cc2424;
        margin-left: 5px;
    }

    .panel-danger > .panel-heading > .panel-options > a {
        color: #f5f5f6;
    }

    .panel-warning {
        border-color: #303641;
    }

    .panel-warning > .panel-heading {
        color: #f2f2f2;
        background-color: #373e4a;
        border-color: #303641;
    }

    .panel-warning > .panel-heading > .panel-options > a.bg {
        background-color: #303641;
        margin-left: 5px;
    }

    .panel-warning > .panel-heading > .panel-options > a {
        color: #f5f5f6;
    }

    .panel-success > .panel-heading {
        color: #f5f5f6;
        background-color: #00a651;
        border-color: #00a651;
    }

    .panel-success {
        border-color: #00a651;
    }

    .panel-success > .panel-heading > .panel-options > a.bg {
        background-color: #00b458;
        margin-left: 5px;
    }

    .panel-success > .panel-heading > .panel-options > a {
        color: #f5f5f6;
    }
</style>