

<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/main-gsap.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/TableTools.min.js"></script>


<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/lodash.min.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-chat.js"></script>
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>

<script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>

    <?php if($this->session->flashdata('success')){ ?>
    toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php }else if($this->session->flashdata('error')){  ?>
    toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php }else if($this->session->flashdata('warning')){  ?>
    toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php }else if($this->session->flashdata('info')){  ?>
    toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    <?php } ?>

</script>