<div style="clear:both;"></div>
<br>
<?php
if (isset($salary_template_id)) {
    $query = $this->db->get_where('salary_template', array('salary_template_id' => $salary_template_id));
    if ($query->num_rows() > 0) {
        $gross_salary = $query->row()->gross_salary;
        $total_deduction = $query->row()->total_deduction;
        $net_salary = $query->row()->net_salary;
    }
}
?>
<div>
    <h3 align="center">
        Salary Payment for <?php echo $employee_name; ?>
        <br><br>
        Payment for <?php echo $date; ?>
    </h3>

    <hr>
    <br>
</div>
<div class="row">


    <div class="col-md-4">

        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="entypo-plus-circled"></i>
                    Payment for <?php echo $date; ?>
                </div>
            </div>

            <div class="panel-body">
                <form role="form" class="validate form-groups-bordered" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="field-1" class=" control-label">Gross salary</label>


                        <input type="number" class="form-control" name="gross_salary" id="gross_salary"
                               value="<?php echo isset($gross_salary) ? $gross_salary : ''; ?>"
                               ondrop="return false;" onpaste="return false;" step="any" autofocus readonly>

                    </div>
                    <div class="form-group">
                        <label for="field-1" class=" control-label">Total Deduction</label>


                        <input type="number" class="form-control" name="total_deduction" id="total_deduction"
                               value="<?php echo isset($total_deduction) ? $total_deduction : ''; ?>"
                               ondrop="return false;" onpaste="return false;" step="any" autofocus readonly>

                    </div>
                    <div class="form-group">
                        <label for="field-1" class=" control-label">Net salary</label>


                        <input type="number" class="form-control" name="net_salary" id="net_salary"
                               value="<?php echo isset($net_salary) ? $net_salary : ''; ?>"
                               ondrop="return false;" onpaste="return false;" step="any" autofocus readonly>

                    </div>
                    <div class="form-group">
                        <label for="field-1" class=" control-label">Fine Deduction</label>


                        <input class="form-control" name="fine_deduction" id="fine_deduction"
                               ondrop="return false;" onpaste="return false;" type="number" step="any" autofocus>

                    </div>


                    <div class="form-group">
                        <label for="field-1" class=" control-label">Payment amount</label>


                        <input class="form-control" name="payment_amount" id="payment_amount"
                               ondrop="return false;" onpaste="return false;" type="number" step="any" autofocus
                               readonly>

                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" class="btn btn-info" id="submit-button" name="pay">
                                Pay
                                <span id="preloader-form"></span>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>

    <div class="col-md-8">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="entypo-plus-circled"></i>
                    Payment History
                </div>
            </div>
            <div class="panel-body">
                <?php if (is_array($salary_history) && count($salary_history) > 0) {
                    ?>
                    <table class="table table-bordered table-striped datatable" id="table-2">
                        <thead>
                        <tr>
                            <th>Month</th>
                            <th>Pay Date</th>
                            <th>Gross salary</th>
                            <th>Total deduction</th>
                            <th>Fine deduction</th>
                            <th>Amount</th>

                        </tr>
                        </thead>

                        <tbody>
                        <?php

                        foreach ($salary_history as $salary_history):
                            ?>
                            <tr>
                                <td><?php echo $salary_history['date']; ?></td>
                                <td><?php echo $salary_history['paying_date']; ?></td>
                                <td><?php echo $salary_history['gross_salary']; ?></td>
                                <td><?php echo $salary_history['total_deduction']; ?></td>
                                <td><?php echo $salary_history['fine_deduction']; ?></td>
                                <td><?php echo $salary_history['payment_amount']; ?></td>
                            </tr>
                        <?php endforeach;

                        ?>
                        </tbody>
                    </table>
                <?php } else {
                    echo 'No payment record found for this employee!<br/><br/>';
                }
                ?>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript">
    jQuery(window).load(function () {
        var $ = jQuery;

        payment_amount = $('#net_salary').val();
        $('#payment_amount').val(payment_amount);
        fine_deduction = parseFloat(0);
        $('#fine_deduction').val(parseFloat(fine_deduction));

        $(document).on('change keyup blur', '#fine_deduction', function () {
            calculatePaymentAmount();
        });

        function calculatePaymentAmount() {
            fine = parseFloat(0);
            fine = $('#fine_deduction').val();

            if (fine != '' && typeof(fine) != "undefined") {
                payment_amount = parseFloat($('#net_salary').val()) - parseFloat(fine);

            } else {
                fine = parseFloat(0);

                payment_amount = parseFloat($('#net_salary').val()) - parseFloat(fine);

            }


            $('#payment_amount').val(payment_amount);
        }

        //datepicker only month

        $(".datepicker-month").datepicker({
            format: "mm/yyyy",
            startView: "months",
            minViewMode: "months"
        });

    });
</script>
