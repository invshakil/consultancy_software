<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Cash Memo</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Memo List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Create Cash Memo</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Cash Memo List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">Memo Code</th>
                                <th>Client Name</th>
                                <th>Bill Type</th>
                                <th>Bill Amount</th>
                                <th>Received By</th>
                                <th>Received Date</th>
                                <th>Payment Status</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->join('clients c', 'c.client_id = cb.client_id')
                                ->join('cash_memo_type m', 'm.m_t_id = cb.bill_type')
                                ->join('admin_staff a', 'a.admin_staff_id = cb.billed_by')
                                ->select('c.client_name, m.m_t_title, a.name, cb.*')
                                ->get('client_billing cb')->result();

                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->bill_code; ?></td>
                                    <td><?php echo $row->client_name; ?></td>
                                    <td><?php echo $row->m_t_title; ?></td>
                                    <td><?php echo $row->bill_amount.' BDT'; ?></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo date('d-M-y h:i A', strtotime($row->bill_date)); ?></td>
                                    <td class="center"><?php if ($row->bill_status == 1) {
                                            echo '<div class="label label-success">Paid</div>&nbsp;';
                                            echo '<a target="_blank" href="'.base_url().'admin_panel/print_cash_memo/'.$row->billing_id.'
                                            " class="btn btn-success btn-sm" style="background-color: #0a001f">Print</a>';

                                        } else {
                                            echo '<div class="label label-danger">Unpaid</div>';
                                        } ?></td>

                                    <td>
                                        <div class="btn-group">

                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle pull-left"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>/modal/popup/modal_edit_memo/<?php echo $row->billing_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>/admin_panel/cash_memo/delete/<?php echo $row->billing_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Cash Memo</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>admin_panel/cash_memo/create"
                              enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Client</label>

                                <div class="col-sm-5">
                                    <select name="client_id" class="form-control select2">
                                        <option value="1">Select client type</option>
                                        <?php $c = $this->db->get('clients')->result();
                                        foreach ($c as $item) { ?>
                                            <option value="<?php echo $item->client_id ?>"><?php echo $item->client_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Cash Memo Code</label>

                                <div class="col-sm-5">
                                    <input type="text" name="bill_code" class="form-control" id="field-1"
                                           placeholder="Enter Cash Memo Code">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Memo Type</label>

                                <div class="col-sm-5">
                                    <select name="bill_type" class="form-control">
                                        <option value="1">Select Memo Type</option>
                                        <?php $v = $this->db->get('cash_memo_type')->result();
                                        foreach ($v as $item) { ?>
                                            <option
                                                value="<?php echo $item->m_t_id ?>"><?php echo $item->m_t_title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Cash Amount</label>

                                <div class="col-sm-5">
                                    <input type="text" name="bill_amount" class="form-control" id="field-1"
                                           placeholder="Enter Amount">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Payment Status</label>

                                <div class="col-sm-5">
                                    <select name="bill_status" class="form-control">
                                        <option value="1">Paid</option>
                                        <option value="2">Unpaid</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Cash Memo</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>