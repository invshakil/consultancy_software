<?php
$this->session->flashdata('message');

/*
 * This will check if any date has been chosen or not
 * If not then from drop down automatically current date will be selected
 */

if (!isset($take_attendance)){
    $date = date("d");
    $month = date("m");
    $year = date("Y");
}
?>
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
    <thead>
    <tr>
        <th>Select Date</th>
        <th>Select Month</th>
        <th>Select Year</th>
        <th>Manage Attendance</th>
    </tr>
    </thead>
    <tbody>
    <form method="post" action="<?php echo base_url(); ?>admin_panel/emp_attendance_selector" class="form">
        <tr class="gradeA">
            <td>
                <select name="date" class="form-control">
                    <?php for ($i = 1; $i <= 31; $i++): ?>
                        <option value="<?php echo $i; ?>"
                            <?php if (isset($date) && $date == $i) echo 'selected="selected"'; ?>>
                            <?php echo $i; ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </td>
            <td>
                <select name="month" class="form-control">
                    <?php
                    for ($i = 1; $i <= 12; $i++):
                        if ($i == 1) $m = 'january';
                        else if ($i == 2) $m = 'february';
                        else if ($i == 3) $m = 'march';
                        else if ($i == 4) $m = 'april';
                        else if ($i == 5) $m = 'may';
                        else if ($i == 6) $m = 'june';
                        else if ($i == 7) $m = 'july';
                        else if ($i == 8) $m = 'august';
                        else if ($i == 9) $m = 'september';
                        else if ($i == 10) $m = 'october';
                        else if ($i == 11) $m = 'november';
                        else if ($i == 12) $m = 'december';
                        ?>
                        <option value="<?php echo $i; ?>"
                            <?php if ($month == $i) echo 'selected="selected"'; ?>>
                            <?php echo $m; ?>
                        </option>
                        <?php
                    endfor;
                    ?>
                </select>
            </td>
            <td>
                <select name="year" class="form-control">
                    <?php for ($i = 2025; $i >= 2015; $i--): ?>
                        <option value="<?php echo $i; ?>"
                            <?php if (isset($year) && $year == $i) echo 'selected="selected"'; ?>>
                            <?php echo $i; ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </td>

            <td align="center"><input type="submit" value="Manage Attendance"
                                      class="btn btn-info"/></td>
        </tr>
    </form>
    </tbody>
</table>

<?php if (isset($take_attendance)): ?>

    <center>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">

                <div class="tile-stats tile-white-gray">
                    <div class="icon"><i class="entypo-suitcase"></i></div>
                    <?php
                    $timestamp = strtotime($full_date);
                    $day = strtolower(date('l', $timestamp));
                    ?>
                    <h2><?php echo ucwords($day); ?></h2>

                    <h3>Attendance
                        of Employees
                        <p><?php echo $date . '-' . $month . '-' . $year; ?></p>
                </div>
            </div>
        </div>
    </center>
    <div class="row">
        <div class="col-sm-offset-3 col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr class="gradeA">
                    <th>Employee Name</th>
                    <th>Attendance Status</th>
                </tr>
                </thead>
                <tbody>

                <?php
                //Employee ATTENDANCE

                $emp = $this->db->get_where('employees')->result_array();


                foreach ($emp as $row) {
                ?>
                <form method="post"
                      action="<?php echo base_url(); ?>admin_panel/employee_attendance/<?php echo $date . '/' . $month . '/' . $year; ?>">
                    <tr class="gradeA">
                        <td><?php echo $row['employee_name']; ?></td>

                        <td align="center">
                            <select name="status[]" class="form-control" style="width:100px; float:left;">

                                <option value="1">Present
                                </option>
                                <option value="2">Absent
                                </option>
                            </select>

                            <input type="hidden" name="employee_id[]"
                                   value="<?php echo $row['employee_id']; ?>"/>

                            <input type="hidden" name="employee_name[]"
                                   value="<?php echo $row['employee_name']; ?>"/>

                            <input type="hidden" name="date[]" value="<?php echo $full_date; ?>"/>

                        </td>
                    </tr>

                    <?php
                    }
                    ?>
                </tbody>

            </table>
            <div class="pull-right">
                <input type="submit" class="btn btn-default" value="Save Attendance"
                       style="float:left; margin:0px 10px;">
                </form></div>

        </div>
    </div>
<?php endif; ?>
