<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Income Statement</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Income Statement List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Income Statement</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Income Statement List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">ID</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <th>Payment Method</th>
                                <th>Cheque No (if applicable)</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->select('ic.income_cat_name, i.*')
                                ->join('income_category ic', 'ic.income_cat_id = i.income_title')->get('income i')->result();
                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->income_id; ?></td>
                                    <td><?php echo $row->income_cat_name; ?></td>
                                    <td><?php echo $row->income_description; ?></td>
                                    <td><?php echo $row->income_amount.' BDT'; ?></td>
                                    <td><?php echo $row->income_method; ?></td>
                                    <td><?php echo $row->cheque_no; ?></td>
                                    <td><?php echo date('d-M-Y', $row->income_date); ?></td>
                                    <td class="center"><?php if ($row->income_status == 1) {
                                            echo '<div class="label label-success">Money Received</div>';
                                        } else {
                                            echo '<div class="label label-danger">Yet to receive</div>';
                                        } ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_income/<?php echo $row->income_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>admin_panel/income_record/delete/<?php echo $row->income_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Income Statement</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>admin_panel/income_record/create"
                              enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Title</label>

                                <div class="col-sm-5">
                                    <select name="income_title" class="form-control">
                                        <?php $type = $this->db->get_where('income_category', array('status' => 1))
                                            ->result();
                                        foreach ($type as $item){
                                        ?>
                                        <option value="<?php echo $item->income_cat_id;?>"><?php echo $item->income_cat_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Description</label>

                                <div class="col-sm-5">
                                    <textarea type="text" rows="5" name="income_description" class="form-control" id="field-1"
                                              placeholder="Enter Description"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Income Amount</label>

                                <div class="col-sm-5">
                                    <input type="number" name="income_amount" class="form-control" id="field-1"
                                           placeholder="Enter Income Amount">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Income Method</label>

                                <div class="col-sm-5">
                                    <select name="income_method" class="form-control">
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Cheque No (If applicable)</label>

                                <div class="col-sm-5">
                                    <input type="text" name="cheque_no" value="N/A" class="form-control" id="field-1"
                                           placeholder="Enter cheque no (If applicable)">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-2" class="col-sm-3 control-label">Date</label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control datepicker" name="income_date" value="" data-start-date="-4w" data-end-date="+4w">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Status</label>

                                <div class="col-sm-5">
                                    <select name="income_status" class="form-control">
                                        <option value="1">Money Received</option>
                                        <option value="0">Yet to receive</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Income Statement</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>