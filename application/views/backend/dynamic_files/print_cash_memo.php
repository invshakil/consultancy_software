<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <title>Cash Memo</title>
    <style type="text/css">

        * {
            margin: 0;
            padding: 0;
        }

        body {
            font: 14px/1.4 Georgia, serif;
        }

        #page-wrap {
            width: 800px;
            margin: 0 auto;
        }

        div {
            border: 0;
            font: 14px Georgia, Serif;
            overflow: hidden;
            resize: none;
        }

        table {
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid black;
            padding: 5px;
        }

        #header {
            height: 15px;
            width: 100%;
            margin: 20px 0;
            background: #222;
            text-align: center;
            color: white;
            font-size: 20px;
            letter-spacing: 5px;
            padding: 20px;
        }

        #address {
            width: auto;
            /*max-height: auto;*/
            max-width: 400px;
            float: left;
            padding-right: 30px;
        }

        #customer {
            overflow: hidden;
        }

        #meta {
            margin-top: 1px;
            width: 400px;
            float: right;
        }

        #meta td {
            text-align: right;
        }

        #meta td.meta-head {
            text-align: left;
            background: #eee;
        }

        #meta td div {
            width: 100%;
            height: 20px;
            text-align: right;
        }

        #items {
            clear: both;
            width: 100%;
            margin: 30px 0 0 0;
            border: 1px solid black;
        }

        #items th {
            background: #eee;
        }

        #items div {
            width: 80px;
            height: 50px;
        }

        #items tr.item-row td {
            border: 0;
            vertical-align: top;
        }

        #items td.description {
            width: 300px;
        }

        #items td.item-name {
            width: 175px;
        }

        #items td.description div, #items td.item-name div {
            width: 100%;
        }

        #items td.total-line {
            border-right: 0;
            text-align: right;
        }

        #items td.total-value {
            border-left: 0;
            padding: 10px;
        }

        #items td.total-value div {
            height: 20px;
            background: none;
        }

        #items td.balance {
            background: #eee;
        }

        #items td.blank {
            border: 0;
        }

        #terms {
            text-align: center;
            margin: 20px 0 0 0;
        }

        #terms h5 {
            text-transform: uppercase;
            font: 13px Helvetica, Sans-Serif;
            letter-spacing: 10px;
            border-bottom: 1px solid black;
            padding: 0 0 8px 0;
            margin: 0 0 8px 0;
        }

        #terms div {
            width: 100%;
            text-align: center;
            padding: 20px;
        }
    </style>

</head>

<body>

<div id="page-wrap">

    <div id="header" style="padding-bottom: 40px">
        <?php $name = $this->db->where('info_type','settings_name')
            ->get('system_settings')->row('description');?>
        <?php echo $name;?>
    </div>

    <div id="identity">

        <div id="address" class="pull-left">
            <?php $settings_logo = $this->db->where('info_type','settings_logo')
                ->get('system_settings')->row('description');?>

            <img id="image" class="img-responsive" src="<?php echo base_url().$settings_logo;?>" alt="logo"/>

        </div>

        <table id="meta">
            <tr>
                <td class="meta-head">Client Name</td>
                <td>
                    <div>
                        <?php
                        echo $this->db->get_where('clients', array('client_id' => $bill_info->client_id))->row('client_name');
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="meta-head">Bill Type</td>
                <td>
                    <div>
                        <?php
                        echo $this->db->get_where('cash_memo_type', array('m_t_id' => $bill_info->bill_type))->row('m_t_title');
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="meta-head">Memo Code #</td>
                <td>
                    <div>
                        <?php
                        echo $bill_info->bill_code;
                        ?>
                    </div>
                </td>
            </tr>
            <tr>

                <td class="meta-head">Date</td>
                <td>
                    <div id="date">
                        <?php
                        echo date('D, dS F Y, H:i A', strtotime($bill_info->bill_date));
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="meta-head">Amount Paid</td>
                <td>
                    <div class="due">৳
                        <?php
                        echo number_format($bill_info->bill_amount);
                        ?>
                    </div>
                </td>
            </tr>

        </table>


    </div>

    <div style="clear:both"></div>
    <hr style="margin-top: 10px; margin-bottom: 10px ">
    <h3 align="center">
        Total Cash Memos of <?php
        echo $this->db->get_where('clients', array('client_id' => $bill_info->client_id))->row('client_name');
        ?>
    </h3>
    <hr style="margin-top: 10px ">
    <table id="items">

        <tr>
            <th><p align="center">Memo Code</p></th>
            <th><p align="center">Bill Type</p></th>
            <th><p align="center">Received By</p></th>
            <th><p align="center">Date</p></th>
            <th><p align="center">Bill Paid</p></th>
        </tr>

        <?php
        $client_id = $bill_info->client_id;

        $memo_by_client = $this->db->order_by('billing_id','desc')
            ->get_where('client_billing', array('client_id' => $client_id))->result();

        foreach ($memo_by_client as $memo) {
            ?>
            <tr class="item-row">
                <td style="width: 12%" align="center">
                    <div>
                        <div>
                            <?php echo $memo->bill_code; ?>
                        </div>
                    </div>
                </td>
                <td style="width: 20%" align="center">

                    <?php
                    echo $this->db->get_where('cash_memo_type', array('m_t_id' => $memo->bill_type))->row('m_t_title');
                    ?>
                </td>
                <td style="width: 20%" align="center">

                        <?php
                        echo $this->db->get_where('admin_staff', array('admin_staff_id' => $memo->billed_by))->row('name');
                        ?>
                </td>
                <td style="width: 30%" align="center">

                    <?php
                    echo date('D, dS F Y, H:i A', strtotime($memo->bill_date));
                    ?>

                </td>
                <td style="width: 10%" align="center">
                <span class="price">
                    ৳<?php echo number_format($memo->bill_amount); ?>
                </span>
                </td>
            </tr>
        <?php } ?>


        <tr>

            <td colspan="3" class="blank"></td>
            <td colspan="1" class="total-line">Agreed Amount</td>
            <td class="total-value">
                <div id="total">
                    ৳ <?php
                    $agreement = $this->db->get_where('client_contract', array('client_id' => $bill_info->client_id))->row('contract_amount');
                    echo number_format($agreement);
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="blank"></td>
            <td colspan="1" class="total-line">Amount Paid</td>

            <td class="total-value">
                <div id="paid">
                    ৳ <?php
                    $sum = $this->db->select('sum(bill_amount) as total', false)
                        ->where('client_id', $client_id)->get('client_billing')->row();
                    echo number_format($sum->total);
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="blank"></td>
            <td colspan="1" class="total-line balance">Balance Due</td>
            <td class="total-value balance">
                <div class="due">
                    ৳ <?php
                    $due = $agreement - $sum->total;
                    echo number_format($due);
                    ?>
                </div>
            </td>
        </tr>

    </table>

    <div id="terms">
        <h5>Address</h5>
        <div>

            <?php $settings_address = $this->db->where('info_type','settings_address')
                ->get('system_settings')->row('description');?>
            <?php echo $settings_address;?>
            <br/>
            <?php $settings_email = $this->db->where('info_type','settings_email')
                ->get('system_settings')->row('description');?>
            <?php echo $settings_email;?>
            <br>
            <?php $settings_contact = $this->db->where('info_type','settings_contact')
                ->get('system_settings')->row('description');?>

            Phone: <?php echo $settings_contact;?>
            <br/>
            <br/>
            © 2017 Software Developed by Techno-71
            <br/>
            Website: www.techno-71.com
        </div>
    </div>

</div>

<style  media="print">
    @page {
        size: auto;
        margin: 0;
    }
</style>

<a onclick="printpage()" id="printpagebutton" class="btn btn-danger btn-lg pull-right"
   style="margin-bottom: 10px;margin-right: 20px">
    Print
    <i class="fa fa-print"></i>
</a>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden'
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print();

        printButton.style.visibility = 'visible';
    }
</script>
</body>

</html>