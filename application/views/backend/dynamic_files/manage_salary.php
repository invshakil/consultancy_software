<?php

$salary_template_info = $this->db->get('salary_template')->result_array();

?>

<?php
$this->session->flashdata('message');
?>

<div class="panel panel-primary">
    <div class="panel-body with-table">
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th>Employee name</th>
                <th>Designation</th>
                <th>Grade</th>
            </tr>
            </thead>

            <tbody>
            <form action="">

                <?php
                $employees = $this->db->get_where('employees', array('employee_status' => 1))->result_array();
                foreach ($employees as $employee):

                    ?>

                    <tr>
                        <td><?php echo $employee['employee_name']; ?></td>
                        <td>
                            <?php echo $this->db->get_where('employee_designation',
                                array('designation_id' => $employee['designation']))->row('designation_name'); ?>
                        </td>
                        <td>
                            <select class="form-control select2 salary_template_id">
                                <option value="">Select</option>
                                <?php foreach ($salary_template_info as $row) { ?>
                                    <option
                                        value="<?php echo $row['salary_template_id']; ?>" <?php if ($row['salary_template_id'] == $employee['salary_template_id']) echo 'selected'; ?>><?php echo $row['salary_grades']; ?></option>
                                <?php } ?>
                            </select>
                            <input id="employee_id" value="<?php echo $employee['employee_id']; ?>"
                                   hidden>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </form>
            </tbody>
        </table>
    </div>
</div>

<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous">

</script>

<script>

    var baseurl = '<?php echo base_url(); ?>';

    $(document).on('change', '.salary_template_id', function () {


        if ($(this).val() != "undefined" && $(this).val() != '') {
            var formData = {
                'salary_template_id': $(this).val(),
                'employee_id': $(this).next().val()
            };
        }

        var request = $.ajax({
            type: 'POST',
            url: baseurl + 'admin_panel/update_salary_grade',
            data: formData,
            dataType: 'text'

        });
        request.done(function (msg) {
            alert('Salary Grade has been assigned!');

        });
        request.fail(function (data) {
            alert('Something went Wrong!');
        });
        // event.preventDefault();


    });


</script>
