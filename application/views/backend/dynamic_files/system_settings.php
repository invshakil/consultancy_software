<div class="panel panel-dark" data-collapsed="0">

    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">System Settings</div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                    class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <!-- panel body -->
    <div class="panel-body">

        <form role="form" class="form-horizontal form-groups-bordered" method="post"
              action="<?php echo base_url() ?>admin_panel/system_settings/do_update" enctype="multipart/form-data">

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Company Name</label>

                <?php $name = $this->db->where('info_type','settings_name')
                                ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_name" class="form-control" id="field-1"
                           value="<?php echo $name;?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Company Logo</label>

                <div class="col-sm-5">

                    <?php $settings_logo = $this->db->where('info_type','settings_logo')
                        ->get('system_settings')->row('description');?>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px;"
                             data-trigger="fileinput">
                            <img src="<?php echo base_url().$settings_logo;?>" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail"
                             style="max-width: 200px; max-height: 150px"></div>
                        <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                            <a href="#" class="btn btn-orange fileinput-exists"
                               data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Company Email Address</label>

                <?php $settings_email = $this->db->where('info_type','settings_email')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_email" class="form-control" id="field-1"
                           value="<?php echo $settings_email;?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Address</label>

                <?php $settings_address = $this->db->where('info_type','settings_address')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <textarea type="text" name="settings_address" class="form-control" id="field-1"
                           placeholder="Enter Address"><?php echo $settings_address;?></textarea>
                </div>

            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Company Phone Number</label>

                <?php $settings_contact = $this->db->where('info_type','settings_contact')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_contact" class="form-control" id="field-1"
                           value="<?php echo $settings_contact;?>">
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Company Working Hours</label>

                <?php $settings_working_hours = $this->db->where('info_type','settings_working_hours')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_working_hours" class="form-control" id="field-1"
                           value="<?php echo $settings_working_hours;?>">
                </div>
            </div>


            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Company Location</label>

                <?php $settings_location = $this->db->where('info_type','location')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_location" class="form-control" id="field-1"
                           value="<?php echo $settings_location;?>">
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Facebook Link</label>

                <?php $settings_facebook = $this->db->where('info_type','facebook')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_facebook" class="form-control" id="field-1"
                           value="<?php echo $settings_facebook;?>">
                </div>
            </div>


            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Google Plus Link</label>

                <?php $settings_google = $this->db->where('info_type','google')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_google" class="form-control" id="field-1"
                           value="<?php echo $settings_google;?>">
                </div>
            </div>


            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Twitter Link</label>

                <?php $settings_twitter = $this->db->where('info_type','twitter')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_twitter" class="form-control" id="field-1"
                           value="<?php echo $settings_twitter;?>">
                </div>
            </div>


            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Linkedin Link</label>

                <?php $settings_linkedin = $this->db->where('info_type','linkedin')
                    ->get('system_settings')->row('description');?>
                <div class="col-sm-5">
                    <input type="text" name="settings_linkedin" class="form-control" id="field-1"
                           value="<?php echo $settings_linkedin;?>">
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-success">Save Settings Info</button>
                </div>
            </div>
        </form>

    </div>
</div>