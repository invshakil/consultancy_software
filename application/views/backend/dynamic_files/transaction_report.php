<div class="row">

    <?php if ($date1 == '' && $date2 == ''): ?>

        <div class="col-md-12">
            <div class="box-content">
                <form role="form" class="form-horizontal form-groups-bordered" method="get"
                      action="<?php echo base_url() ?>admin_panel/transaction_report/"
                      enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">First Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date1" value=""
                               data-start-view="1">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Last Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date2" value=""
                               data-start-view="1">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info">Get Report</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($date1 != '' && $date2 != ''): ?>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <div class="icon"><i class="entypo-suitcase"></i></div>
                    <h2><?php echo 'Transaction Report Between ' . date('d-M-Y', strtotime($date1)) . ' to ' . date('d-M-Y', strtotime($date2)); ?></h2>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $this->session->flashdata('message');
            ?>
            <br><br>
            <table class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th width="20%">
                        <div>Transaction Type</div>
                    </th>
                    <th width="20%">
                        <div>Title</div>
                    </th>
                    <th>
                        <div>Transaction Amount</div>
                    </th>
                    <th>
                        <div>Payment Method</div>
                    </th>
                    <th>
                        <div>Date</div>
                    </th>
                    <th class="span3">
                        <div>Payment Status</div>
                    </th>

                </tr>
                </thead>
                <tbody>

               <?php foreach ($income as $row) {?>
                <tr class="table-success">
                    <td><div class="label label-primary">Income</div></td>
                    <td><?php echo $row->income_cat_name;?></td>
                    <td><?php echo number_format($row->income_amount).' BDT';?></td>
                    <td><?php echo $row->income_method;?></td>
                    <td><?php echo date('d-M-Y',$row->income_date);?></td>
                    <td>
                        <?php if ($row->income_status == 1) {
                            echo '<div class="label label-success">Money Received</div>';
                        } else {
                            echo '<div class="label label-danger">Yet to receive</div>';
                        } ?>
                    </td>
                </tr>
                <?php } ?>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

               <?php foreach ($expense as $row) {?>
                   <tr>
                       <td><div class="label label-danger">Expense</div></td>
                       <td><?php echo $row->expense_cat_name;?></td>
                       <td><?php echo number_format($row->expense_amount).' BDT';?></td>
                       <td><?php echo $row->expense_method;?></td>
                       <td><?php echo date('d-M-Y',$row->expense_date);?></td>
                       <td>
                           <?php if ($row->expense_status == 1) {
                               echo '<div class="label label-success">Money Paid</div>';
                           } else {
                               echo '<div class="label label-danger">Yet to pay</div>';
                           } ?>
                       </td>
                   </tr>
               <?php } ?>

                </tbody>
            </table>

            <br/><br/>
            <div class="col-sm-6">

            </div>

            <div class="col-sm-6">

                <div class="invoice-right pull-right">

                    <ul class="list-unstyled">
                        <li style="font-size: 16px">
                            <b>TOTAL INCOME:</b>
                            <strong style="color: #0a001f"><?php echo number_format( $total_income[0]->income_amount ). ' BDT';?></strong>
                        </li>
                        <li style="font-size: 16px">
                            <b>TOTAL EXPENSE:</b>
                            <strong style="color: red"><?php echo number_format( $total_expense[0]->expense_amount ). ' BDT';?></strong>
                        </li>

                        <li><br/></li>
                        <li style="font-size: 18px">
                            <b>NET INCOME:</b>
                            <strong style="color: #00aff2">
                                <?php
                                $net = $total_income[0]->income_amount - $total_expense[0]->expense_amount;
                                echo number_format(  $net ).' BDT';
                                ?>
                            </strong>
                        </li>
                    </ul>

                    <br/>

                    <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">
                        Print This Report
                        <i class="entypo-doc-text"></i>
                    </a>

                    &nbsp;

                </div>

            </div>
        </div>
    <?php endif; ?>


</div>
