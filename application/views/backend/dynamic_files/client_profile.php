<style>
    .borderless td, .borderless th {
        border: none;
    }
</style>
<ol class="breadcrumb bc-2 hidden-print">
    <li>
        <a href="<?php echo base_url() ?>admin_panel"><i class="fa-home"></i>Home</a>
    </li>
    <li>

        <a href="<?php echo base_url() ?>admin_panel/document_check">Client Details</a>
    </li>
    <li class="active">

        <strong>Profile</strong>
    </li>
</ol>
<br class="hidden-print"/>

<div class="invoice">

    <div class="row">

        <div class="col-sm-6 invoice-left">

            <?php if (isset($client_data->client_image)) { ?>
                <a href="#">
                    <img src="<?php echo base_url() . $client_data->client_image ?>"
                         class="img-responsive img-thumbnail" width="200"/>
                </a>
            <?php } else {
                echo 'N/A';
            } ?>

        </div>

        <div class="col-sm-6 invoice-right">

            <?php if (isset($client_data->client_code)) { ?>
                <h3>Client Code: <?php echo $client_data->client_code; ?></h3>
            <?php } else {
                echo 'Client Code: N/A';
            } ?>
            <?php if (isset($client_data->last_update)) { ?>
                <span>Last Updated On: <?php echo date('d-M-Y h:i A', strtotime($client_data->last_update)); ?></span>
            <?php } else {
                echo 'Last Updated On: N/A';
            } ?>
        </div>

    </div>


    <hr class="margin"/>


    <div class="row">

        <div class="col-sm-3 invoice-left">

            <h4>Client Information</h4>
            Name: <b class="label label-info"><?php echo $client_data->client_name; ?></b>
            <hr/>
            Address: <b><?php echo $client_data->address; ?></b>
            <hr/>
            Contact: <b class="label label-info"><?php echo $client_data->phone; ?></b>
            <hr/>
            Profession: <b class="label label-info"><?php echo $client_data->profession; ?></b>


        </div>

        <div class="col-sm-4 invoice-left">

            <h4>&nbsp;</h4>
            Visa Type: <b
                class="label label-info"><?php echo $visa = $this->db->join('visa_type', 'visa_type_id = visa_type')
                    ->get_where('clients', array('visa_type' => $client_data->visa_type))->row('visa_name') ?></b>
            <hr/>
            Country: <b class="label label-info"><?php echo $country = $this->db->join('countries', 'id = country')
                    ->get_where('clients', array('country' => $client_data->country))->row('country_name') ?></b>
            <hr/>
            Destination: <?php if (isset($client_data->client_visit_place)) { ?>
                <b class="label label-info"><?php echo $client_data->client_visit_place; ?></b>
            <?php } else {
                echo 'Last Updated On: N/A';
            } ?>
            <hr/>
            Consultant: <b
                class="label label-success"><?php echo $visa = $this->db->join('admin_staff', 'admin_staff_id = reviewed_by')
                    ->get_where('client_document', array('reviewed_by' => $client_data->reviewed_by))->row('name') ?></b>
        </div>

        <div class="col-sm-5 invoice-right">

            <?php
            $agreement = $this->db
                ->get_where('client_contract', array('client_id' => $client_data->client_id))->row();

            ?>

            <h4>Agreement Details:</h4>
            <strong>Amount #:</strong> <?php if (isset($agreement->contract_amount)) {
                echo number_format($agreement->contract_amount) . ' BDT';
            } else {
                echo 'No agreement File Found!';
            } ?>
            <br/>
            <strong>Agreement on:</strong> <?php
            if (isset($agreement->contract_date)) {
                echo date('d-M-Y h:i A', strtotime($agreement->contract_date));
            } else {
                echo 'No agreement File Found!';
            }
            ?>
            <br>
            <strong>
                <?php
                if (!isset($agreement->contract_amount)) {
                    echo 'To Create Agreement: 
                    <a href="' . base_url() . 'admin_panel/client_contract/' . $client_data->client_id . '"
                    >Click Here</a>';
                } ?>
            </strong>
            <br/>

        </div>

    </div>

    <div class="margin"></div>
    <table class="table borderless">
        <thead>
        <tr>
            <th width="20%"><p align="center" style="font-weight: bold;font-size: 20px">Passport</p></th>
            <th width="20%"><p align="center" style="font-weight: bold;font-size: 20px">Academic Transcript</p></th>
            <th width="20%"><p align="center" style="font-weight: bold;font-size: 20px">Visa</p></th>
            <th width="20%"><p align="center" style="font-weight: bold;font-size: 20px">Medical</p></th>
            <th width="20%"><p align="center" style="font-weight: bold;font-size: 20px">Sponsor</p></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <a href="#" data-toggle="modal" data-target="#myModal1">
                    <img src="<?php echo base_url() . $client_data->client_passport; ?>" width="100"
                         class="img-responsive img-thumbnail">
                </a>

            </td>
            <td>
                <a href="<?php echo base_url() . $client_data->academic_transcript ?>" target="_blank"><img
                        src="<?php echo base_url() ?>uploads/transcripts.png" alt="" width="100"
                        class="img-responsive img-thumbnail"></a>
            </td>
            <td>
                <a href="#" data-toggle="modal" data-target="#myModal2">
                    <img src="<?php echo base_url() . $client_data->client_visa; ?>" width="100"
                         class="img-responsive img-thumbnail">
                </a>

            </td>
            <td>
                <a href="<?php echo base_url() . $client_data->medical_report ?>" target="_blank"><img
                        src="<?php echo base_url() ?>uploads/medical.jpg" alt="" width="100"
                        class="img-responsive img-thumbnail"></a>
            </td>
            <td>
                <a href="<?php echo base_url() . $client_data->sponsor_document; ?>" target="_blank"><img
                        src="<?php echo base_url() ?>uploads/sponsor.png" alt="" width="100"
                        class="img-responsive img-thumbnail"></a>
            </td>

        </tr>
        </tbody>
    </table>

    <div class="margin"></div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="text-center">#Memo Code</th>
            <th width="30%">Bill Type</th>
            <th width="20%">Paid Amount</th>
            <th width="30%">Date</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $id = $client_data->client_id;

        $memos = $this->db->join('cash_memo_type', 'm_t_id = bill_type')->get_where('client_billing', array('client_id' => $id))->result();

        if ($memos != NULL) {
            foreach ($memos as $memo) {
                ?>
                <tr>
                    <td class="text-center"><?php echo $memo->bill_code; ?></td>
                    <td><?php echo $memo->m_t_title; ?></td>
                    <td><?php echo number_format($memo->bill_amount) . ' BDT'; ?></td>
                    <td><?php echo date('d-M-y h:i A', strtotime($memo->bill_date)); ?></td>
                </tr>
            <?php }
        } else {
            echo '<tr> <td colspan="4"><h3>No Memo Document Found!</h3></td></tr>';
        } ?>

        </tbody>
    </table>

    <div class="margin"></div>

    <div class="row">

        <div class="col-sm-6">

            <div class="invoice-left">


            </div>

        </div>

        <div class="col-sm-6">

            <div class="invoice-right">

                <ul class="list-unstyled">
                    <li>
                        TOTAL Amount:
                        <strong
                            style="color: #0a001f">
                            <?php if (isset($agreement->contract_amount)) {
                                echo number_format($agreement->contract_amount) . ' BDT';
                            } else {
                                echo 'No agreement File Found!';
                            } ?>
                        </strong>
                    </li>
                    <li>
                        TOTAL Paid:
                        <strong style="color: red"><?php $total = $this->db->select_sum('bill_amount')
                                ->get_where('client_billing', array('client_id' => $client_data->client_id))->row();
                            echo number_format($total->bill_amount) . ' BDT';
                            ?></strong>
                    </li>
                    <li>
                        DUE Amount:
                        <strong style="color: #00aff2">
                            <?php
                            if (isset($agreement->contract_amount)) {
                                $due = $agreement->contract_amount - $total->bill_amount;
                                echo number_format($due) . ' BDT';
                            } else {
                                echo 'No agreement File Found!';
                            } ?>

                        </strong>
                    </li>

                    <li>
                        <?php
                        if (!isset($agreement->contract_amount)) {
                            echo 'To Create Agreement: 
                    <a href="' . base_url() . 'admin_panel/client_contract/' . $client_data->client_id . '"
                    >Click Here</a>';
                        } ?>
                    </li>
                </ul>


                <br/>

                <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">
                    Print This Client
                    <i class="entypo-doc-text"></i>
                </a>

                &nbsp;

            </div>

        </div>

    </div>

</div>


<!-- Modal -->
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Passport of <?php echo $client_data->client_name; ?></h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo base_url() . $client_data->client_passport; ?>"
                     class="img-responsive img-thumbnail">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Visa of <?php echo $client_data->client_name; ?></h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo base_url() . $client_data->client_visa; ?>"
                     class="img-responsive img-thumbnail">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>