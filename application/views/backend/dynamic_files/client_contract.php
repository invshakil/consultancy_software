<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Contract</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Contract ADD/MANAGE</a></li>

            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Contract ADD & LIST</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <?php if ($id) { ?>
                            <div class="panel-body col-md-12">
                                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                      action="
                        <?php echo base_url() ?>admin_panel/client_contract/<?php echo $id ?>/create"
                                      enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Contract Amount</label>

                                        <div class="col-sm-5">
                                            <input type="number" name="contract_amount" class="form-control"
                                                   id="field-1"
                                                   placeholder="Enter Amount">
                                            <input type="hidden" name="client_id" value="<?php echo $id ?>">
                                            <input type="hidden" name="consultant_id"
                                                   value="<?php echo $this->session->userdata('admin_id') ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Description</label>

                                        <div class="col-sm-5">
                                                                <textarea type="text" name="contract_description"
                                                                          class="form-control" id="field-1"
                                                                          rows="15"
                                                                          placeholder="Enter description"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" class="btn btn-success">Add Client Contract</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        <?php } else { ?>

                            <hr/>

                            <div class="panel-body col-md-12 table-responsive">
                                <table class="table table-bordered datatable" id="table-1">
                                    <thead>
                                    <tr>
                                        <th class="center">ID</th>
                                        <th>Name</th>
                                        <th>Contract Status</th>
                                        <th>Amount</th>
                                        <th>options</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $info = $this->db->get('clients')->result();
                                    foreach ($info as $row) {
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row->client_id; ?></td>
                                            <td><?php echo $row->client_name; ?></td>
                                            <td>
                                                <?php if ($row->contract_status == 0) { ?>
                                                    <a class="btn btn-danger btn-sm"
                                                       href="<?php echo base_url() ?>admin_panel/client_contract/<?php echo $row->client_id ?>">Start
                                                        Contract with this client</a>
                                                <?php } else {
                                                    echo '<b class="btn btn-success btn-sm">Already contract signed!</b>';
                                                    ?>
                                                    <a href="#" class="btn btn-info btn-sm"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_view_contract/<?php echo $row->client_id; ?>');">
                                                        <i class="fa fa-check-circle"> View Contract</i>
                                                    </a>
                                                <?php } ?>

                                            </td>
                                            <td>
                                                <?php
                                                if ($row->contract_status == 0) {
                                                    echo 'Yet to sign a contract!';
                                                } else {
                                                    $amount = $this->db
                                                        ->get_where('client_contract', array('client_id' => $row->client_id))->row('contract_amount');
                                                    echo $amount . ' BDT';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                        <!-- EDITING LINK -->
                                                        <li>
                                                            <a href="#"
                                                               onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_contract/<?php echo $row->client_id; ?>');">
                                                                <i class="entypo-pencil"></i>
                                                                Edit
                                                            </a>
                                                        </li>
                                                        <li class="divider"></li>

                                                        <!-- DELETION LINK -->
                                                        <li>
                                                            <a href="#"
                                                               onclick="confirm_modal('<?php echo base_url(); ?>admin_panel/client_contract/<?php echo $row->client_id ?>/delete/<?php echo $row->client_id; ?>');">
                                                                <i class="entypo-trash"></i>
                                                                Delete
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>

        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>