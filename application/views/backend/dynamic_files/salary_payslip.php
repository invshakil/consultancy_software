<div class="invoice">
    <?php if (!isset($date)) { ?>
        <div class="row">
            <div class="col-md-12">
                <form role="form" class="form-horizontal form-groups validate"
                      action="<?php echo base_url() ?>admin_panel/salary_payslip" method="post">
                    <div class="form-group">

                        <label for="field-1" class="col-sm-3 control-label">Select Month</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="text" class="form-control datepicker-month" name="date"
                                       value="<?php echo date("m/Y"); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Select Employee</label>
                        <div class="col-sm-5">
                            <select name="employee_id" class="form-control">
                                <option>Select</option>
                                <?php $employees = $this->db->get_where('employees', array('employee_status' => 1))->result();
                                foreach ($employees as $emp) {
                                    ?>
                                    <option
                                        value="<?php echo $emp->employee_id; ?>"><?php echo $emp->employee_name; ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-info" id="submit-button" name="go">
                                Get Payslip
                            </button>
                        </div>
                    </div>

                    <hr>

                </form>
            </div>
        </div>
    <?php } else {

        if (is_array($salary_payslip) && count($salary_payslip) > 0) {

            foreach ($salary_payslip as $total_details) {

                $payment_id = $total_details['salary_id'];
                $payment_amount = $total_details['payment_amount'];
                $gross_salary = $total_details['gross_salary'];
                $total_deduction = $total_details['total_deduction'];
                $net_salary = $total_details['net_salary'];

            }

            $earnings = $this->db->get_where('salary_template', array('salary_template_id' => $salary_template_id))->result_array();

            foreach ($earnings as $earning) {
                $salary_grades = $earning['salary_grades'];
                $basic_salary = $earning['basic_salary'];
                $allowance_entries = $earning['allowance_entries'];
                $deduction_entries = $earning['deduction_entries'];
            }

            ?>

            <div class="row">

                <div class="col-sm-6 invoice-left">

                    <a href="#">
                        <?php $settings_logo = $this->db->where('info_type', 'settings_logo')
                            ->get('system_settings')->row('description'); ?>

                        <img id="image" class="img-responsive" width="300"
                             src="<?php echo base_url() . $settings_logo; ?>"
                             alt="logo"/>
                    </a>

                </div>

                <div class="col-sm-6 invoice-right">
                    <br/>
                    <h3> Salary Payment Slip</h3>
                    <span><?php $month = explode('/', $date);
                        $date = $month[0] . '/1/' . $month[1];
                        echo date('F, Y', strtotime($date)); ?></span>
                </div>

            </div>


            <hr class="margin">


            <div class="row">

                <div class="col-sm-3 invoice-left">

                    <?php $emp_info = $this->db->get_where('employees', array('employee_id' => $employee_id))->row(); ?>

                    Employee Name : <?php echo $emp_info->employee_name; ?>
                    <br>
                    Email : <?php echo $emp_info->email; ?>
                    <br>
                    Designation : <?php echo $this->db->get_where('employee_designation',
                        array('designation_id' => $emp_info->designation))->row('designation_name'); ?>

                </div>

                <div class="col-sm-3 invoice-left">


                </div>

                <div class="col-md-6 invoice-right">


                    <strong>Payslip No #:</strong> <?php echo $payment_id; ?>
                    <br>


                </div>

            </div>

            <div class="margin"></div>

            <div class="row">
                <div class="col-lg-6">

                    <div class="panel panel-primary" data-collapsed="0">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <i class="entypo-plus-circled"></i>
                                Earnings
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>

                                    <th width="60%"><p align="center">Type of pay</p></th>
                                    <th><p align="center">Amount</p></th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td class="text-center">Salary Grades:</td>
                                    <td class="text-center"><?php echo $salary_grades; ?></td>
                                </tr>

                                <tr>
                                    <td class="text-center">Basic Salary</td>
                                    <td class="text-center"><?php echo $basic_salary; ?></td>

                                </tr>

                                <?php
                                $allowance_entries = json_decode($allowance_entries);
                                foreach ($allowance_entries as $allowance_entry) {
                                    ?>


                                    <tr>
                                        <td class="text-center"><?php echo $allowance_entry->allowance_description; ?></td>
                                        <td class="text-center"><?php echo $allowance_entry->allowance_amount; ?></td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>


                        </div>
                    </div>

                    <?php if (is_array($deduction_entries)) { ?>

                        <div class="panel panel-primary" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <i class="entypo-plus-circled"></i>
                                    Deductions
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>

                                        <th width="60%"><p>Type of pay</p></th>
                                        <th><p align="center">Amount</p></th>
                                    </tr>
                                    </thead>

                                    <tbody>


                                    <?php
                                    $deduction_entries = json_decode($deduction_entries);
                                    foreach ($deduction_entries as $deduction_entry) {
                                        ?>


                                        <tr>
                                            <td class="text-center"><?php echo $deduction_entry->deduction_description; ?></td>
                                            <td class="text-center"><?php echo $deduction_entry->deduction_amount; ?></td>

                                        </tr>
                                    <?php } ?>


                                    </tbody>
                                </table>


                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-6">

                    <div class="panel panel-primary" data-collapsed="0">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <i class="entypo-plus-circled"></i>
                                Total details
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">


                                <tbody>


                                <tr>
                                    <td class="text-center">Gross Salary</td>
                                    <td class="text-center"><?php echo $gross_salary; ?></td>

                                </tr>

                                <tr>
                                    <td class="text-center">Total Deduction</td>
                                    <td class="text-center"><?php echo $total_deduction; ?></td>

                                </tr>
                                <tr>
                                    <td class="text-center">Net Salary</td>
                                    <td class="text-center"><?php echo $net_salary; ?></td>

                                </tr>
                                <tr>
                                    <td class="text-center">Paid Amount</td>
                                    <td class="text-center"><?php echo $payment_amount; ?></td>

                                </tr>

                                </tbody>
                            </table>


                        </div>
                    </div>


                </div>
            </div>


            <div class="margin"></div>

            <div class="row">


                <div class="col-sm-6">

                    <div class="invoice-right">


                        <br>

                        <a onclick="printpage()" id="printpagebutton"
                           class="btn btn-primary btn-icon icon-left hidden-print">
                            Print
                            <i class="entypo-doc-text"></i>
                        </a>

                        &nbsp;

                    </div>

                </div>

            </div>
        <?php } else {
            echo '<h2>This Employee have not paid yet!</h2>';
        }
    } ?>

</div>

<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
</style>

<script type="text/javascript">
    jQuery(window).load(function () {
        var $ = jQuery;


        //datepicker only month

        $(".datepicker-month").datepicker({
            format: "mm/yyyy",
            startView: "months",
            minViewMode: "months"
        });

    });
</script>

<script type="text/javascript">

    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden'
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print();

        printButton.style.visibility = 'visible';
    }
</script>
