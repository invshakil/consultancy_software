<div class="invoice">
    <?php if (!isset($date)) { ?>
        <div class="row">
            <div class="col-md-12">
                <form role="form" class="form-horizontal form-groups validate"
                      action="<?php echo base_url() ?>admin_panel/payroll_summary" method="post">
                    <div class="form-group">

                        <label for="field-1" class="col-sm-3 control-label">Select Month</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="text" class="form-control datepicker-month" name="date"
                                       value="<?php echo date("m/Y"); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-info" id="submit-button">
                                Get Payroll Report
                            </button>
                        </div>
                    </div>

                    <hr>

                </form>
            </div>
        </div>
    <?php } else {?>
        <div class="row">
            <div class="col-md-12">
                <div class="tile-stats tile-white-gray">
                    <div class="icon" style="bottom: 45px!important;"><i class="fa fa-credit-card"></i></div>
                    <h2><?php $date = explode('/',$date); $new_date = $date[0].'/1/'.$date[1];
                        echo 'Payroll Summary: '.date('F Y',strtotime($new_date));?></h2>
                </div>
            </div>
            <div class="col-md-12" style="color: #4f4f4f;font-weight: bold">
                <table class="table table-bordered responsive">
                    <thead>
                    <tr>
                        <th>Payslip No#</th>
                        <th>Employee Name</th>
                        <th>Designation</th>
                        <th>Salary Grade</th>
                        <th>Net Salary</th>
                        <th>Salary Paid</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($salary_report as $report) {?>

                        <tr>
                            <td>
                                <?php echo '#'.$report->salary_id;?>
                            </td>
                            <td>
                                <?php
                                $id = $report->employee_id;
                                echo $this->db->get_where('employees',array('employee_id'=>$id))->row('employee_name');
                                ?>
                            </td>
                            <td>
                                <?php
                                $id = $report->employee_id;
                                echo $this->db->join('employee_designation','designation_id = designation')
                                    ->get_where('employees',array('employee_id'=>$id))->row('designation_name');
                                ?>
                            </td>
                            <td>
                                <?php
                                $id = $report->employee_id;
                                $grade_id = $this->db->get_where('employees',array('employee_id'=>$id))
                                    ->row('salary_template_id');
                                echo $this->db->get_where('salary_template',array('salary_template_id'=>$grade_id))->row('salary_grades');
                                ?>
                            </td>
                            <td>
                                <?php echo $report->net_salary;?>
                            </td>
                            <td>
                                <?php echo $report->payment_amount;?>
                            </td>
                        </tr>

                    <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <br/><br/>
                <div class="col-md-6">

                </div>

                <div class="col-md-6">

                    <div class="invoice-right pull-right">

                        <ul class="list-unstyled">
                            <li style="font-size: 16px">
                                <b>TOTAL PAID:</b>
                                <strong style="color: #0a001f"><?php echo number_format( $paid ). ' BDT';?></strong>
                            </li>

                        </ul>

                        <br/>

                        <a onclick="printpage()" id="printpagebutton"
                           class="btn btn-primary btn-icon icon-left hidden-print">
                            Print
                            <i class="entypo-doc-text"></i>
                        </a>

                        &nbsp;

                    </div>

                </div>
            </div>
        </div>
    <?php }?>
</div>


<script type="text/javascript">
    jQuery(window).load(function () {
        var $ = jQuery;


        //datepicker only month

        $(".datepicker-month").datepicker({
            format: "mm/yyyy",
            startView: "months",
            minViewMode: "months"
        });

    });

    jQuery("tr:odd").addClass("bg-warning");
    jQuery("tr:even").addClass("bg-info");
</script>

<script type="text/javascript">

    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden'
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print();

        printButton.style.visibility = 'visible';
    }
</script>
