<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Agents</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Agent List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Agent</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Agent List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">ID</th>
                                <th data-hide="phone">Image</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Code</th>
                                <th>Status</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->get('agents')->result();
                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->id; ?></td>
                                    <td class="center">
                                        <img src="<?php echo base_url() . $row->image; ?>" width="60px"
                                                            class="img-thumbnail">
                                    </td>

                                    <td><a href="<?php echo base_url().'admin_panel/agents_country/'.$row->id?>"><?php echo $row->name; ?></a></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo $row->phone; ?></td>
                                    <td><?php echo $row->code; ?></td>
                                    <td class="center"><?php if ($row->status == 1) {
                                            echo '<div class="label label-success">Active</div>';
                                        } else {
                                            echo '<div class="label label-danger">Inactive</div>';
                                        } ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_agents/<?php echo $row->id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>admin_panel/agents/delete/<?php echo $row->id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Agent</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>admin_panel/agents/create"
                              enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Agent Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="name" class="form-control" id="name" value=""
                                           placeholder="Enter Name">

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Image Upload</label>

                                <div class="col-sm-5">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px;"
                                             data-trigger="fileinput">
                                            <img src="http://placehold.it/500x400" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px"></div>
                                        <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                            <a href="#" class="btn btn-orange fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-5">
                                    <input type="text" name="email" class="form-control" id="email"
                                           placeholder="Enter Email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Phone</label>

                                <div class="col-sm-5">
                                    <input type="text" name="phone" class="form-control" id="field-1"
                                           placeholder="Enter Phone Number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Agent Code</label>

                                <div class="col-sm-5">
                                    <input type="text" name="code" class="form-control" id="field-1"
                                           placeholder="Enter Agent Code">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Activity Status</label>

                                <div class="col-sm-5">
                                    <select name="status" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Agent Info</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="application/javascript">
    var $ = jQuery;

    $(document).ready(function () {
        $('select[name="employee"]').on('change', function () {
            var employee_id = $(this).val();

            $.ajax({
                url: '<?php echo base_url()?>admin_panel/get_emp_info/' + employee_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('input#name').val(data.employee_name);
                    $('input#email').val(data.email);

                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });
</script>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>