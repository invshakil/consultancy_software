<?php
$edit_data = $this->db->get_where('company_clients', array('c_client_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal validate form-groups invoice-add"
                  action="<?php echo base_url(); ?>admin_panel/company_clients/do_update/<?php echo $row['c_client_id']?>" method="post"
                  enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-12">


                        <div class="panel-body">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Client Name</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name"
                                           id="name" data-validate="required"
                                           data-message-required="value required" value="<?php echo $row['name']?>" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Select Client Type</label>

                                <div class="col-sm-8">
                                    <select name="client_type" id="client_type" class="form-control">
                                        <option value="Individual Client" <?php if ($row['client_type'] == 'Individual Client') echo 'selected'?> >
                                            Individual Client</option>
                                        <option value="Company Client" <?php if ($row['client_type'] == 'Company Client') echo 'selected'?>>Company Client</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Clients Phone Number</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="phone"
                                           id="phone" data-validate="required"
                                           data-message-required="value required" value="<?php echo $row['phone']?>" autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Clients Email</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="email"
                                           id="email" data-validate="required"
                                           data-message-required="value required" value="<?php echo $row['email']?>" autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Opened Date</label>

                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" name="date" value="<?php echo $row['date']?>" class="form-control datepicker" data-format="D, dd MM yyyy">

                                        <div class="input-group-addon">
                                            <a href="#"><i class="entypo-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


                <div class="row">

                    <!--TITLE FIELD & DESCRIPTION-->

                    <div class="col-md-12">

                        <div class="panel panel-primary" data-collapsed="0">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <i class="entypo-plus-circled"></i>
                                    Clients Information
                                </div>
                            </div>
                            <div class="panel-body">
                                <!-- FORM ENTRY STARTS HERE-->

                                <?php
                                $descriptions = json_decode($row['description']);
                                foreach ($descriptions as $des) {
                                ?>
                                <div id="allowances_entry">
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-1 control-label">Data</label>

                                        <div class="col-sm-4">
                                            <input type="text" class="form-control"
                                                   name="input_title[]" value="<?php echo $des->title;?>"
                                                   placeholder="Title">
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control allowance_amount"
                                                   name="input_description[]" value="<?php echo $des->description;?>"
                                                   placeholder="Description">
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-default"
                                                    onclick="deleteParentElement(this)">
                                                <i class="entypo-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>

                                <!-- FORM ENTRY ENDS HERE-->


                                <div class="form-group">
                                    <div class="col-sm-offset-1 col-sm-8">
                                        <button type="button"
                                                class="btn btn-default btn-sm btn-icon icon-left"
                                                onClick="add_desc_entry()">
                                            Add Description entry
                                            <i class="entypo-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-success" id="submit-button">
                                        Update Information
                                        <span id="preloader-form"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        <?php endforeach; ?>
    </div>
</div>

<div id="clone_entry" style="display: none;">
        <div class="form-group">
            <label for="field-1" class="col-sm-1 control-label">Data</label>

            <div class="col-sm-4">
                <input type="text" class="form-control"
                       name="input_title[]" value=""
                       placeholder="Title">
            </div>
            <div class="col-sm-5">
                <input type="text" class="form-control allowance_amount"
                       name="input_description[]" value=""
                       placeholder="Description">
            </div>
            <div class="col-sm-1">
                <button type="button" class="btn btn-default"
                        onclick="deleteParentElement(this)">
                    <i class="entypo-trash"></i>
                </button>
            </div>
        </div>
</div>


<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>

    $ = jQuery;
    // CREATING BLANK allowance ENTRY
    var blank_desc_entry = '';
    $(document).ready(function () {
        blank_desc_entry = $('#clone_entry').html();
    });

    function add_desc_entry() {
        $("#allowances_entry").append(blank_desc_entry);
    }

    // REMOVING  ENTRY
    function deleteParentElement(n) {
        n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
    }

</script>