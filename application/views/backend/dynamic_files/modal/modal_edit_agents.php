<?php
$edit_data = $this->db->get_where('agents', array('id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/agents/do_update/<?php echo $row['id'] ?>"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Agent Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="name" class="form-control" id="field-1"
                               value="<?php echo $row['name'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Image Upload</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php if ($row['image']) {?>
                                    <img src="<?php echo base_url().$row['image']?>" alt="...">
                                <?php } else {?>
                                    <img src="http://placehold.it/500x500" alt="...">
                                <?php }?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Agent Email</label>

                    <div class="col-sm-5">
                        <input type="email" name="email" class="form-control" id="field-1"
                               value="<?php echo $row['email'] ?>">
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"> Phone</label>

                    <div class="col-sm-5">
                        <input type="text" name="phone" class="form-control" id="field-1"
                               value="<?php echo $row['phone'] ?>">
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"> Agent Code</label>

                    <div class="col-sm-5">
                        <input type="text" name="code" class="form-control" id="field-1"
                               value="<?php echo $row['code'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Agent Status</label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control">
                            <option value="1" <?php if ($row['status'] == 1) echo 'selected'; ?> >Yes</option>
                            <option value="2" <?php if ($row['status'] == 2) echo 'selected'; ?>>No</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>