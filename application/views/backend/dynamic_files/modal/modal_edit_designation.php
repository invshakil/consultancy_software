<?php
$edit_data = $this->db->get_where('employee_designation', array('designation_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/employee_designation/do_update/<?php echo $row['designation_id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Designation Title</label>

                    <div class="col-sm-5">
                        <input type="text" name="designation_name" class="form-control" id="field-1"
                               value="<?php echo $row['designation_name'] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Description</label>

                    <div class="col-sm-5">
                                        <textarea type="text" name="role_description" class="form-control"
                                                  id="field-1"
                                                  rows="5"
                                                  placeholder="Enter description"><?php echo $row['role_description'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Visibility Status</label>

                    <div class="col-sm-5">
                        <select name="visibility_status" class="form-control">
                            <option value="1" <?php if ($row['visibility_status'] == 1) echo 'selected'; ?>
                            >Active
                            </option>
                            <option value="0"<?php if ($row['visibility_status'] == 0) echo 'selected'; ?>
                            >Inactive
                            </option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>