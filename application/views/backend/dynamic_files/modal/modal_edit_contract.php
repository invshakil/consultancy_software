<?php
$edit_data = $this->db->get_where('client_contract', array('client_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/client_contract/<?php echo $row['client_id'] ?>/do_update/<?php echo $row['client_id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Contract Amount</label>

                    <div class="col-sm-5">
                        <input type="number" name="contract_amount" class="form-control"
                               id="field-1" value="<?php echo $row['contract_amount']?>"
                               placeholder="Enter Amount">
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Description</label>

                    <div class="col-sm-5">
                        <textarea type="text" name="contract_description"
                        class="form-control" id="field-1" rows="15"
                        placeholder="Enter description"><?php echo $row['contract_description']?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>