<?php
$edit_data = $this->db->get_where('expense', array('expense_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/expense_record/do_update/<?php echo $row['expense_id'] ?>"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Title</label>
                    <div class="col-sm-5">
                        <select name="expense_title" class="form-control">
                            <?php $type = $this->db->get_where('expense_category', array('status' => 1))
                                ->result();
                            foreach ($type as $item) {
                                ?>
                                <option value="<?php echo $item->expense_cat_id; ?>"
                                    <?php if ($item->expense_cat_id == $row['expense_title']) echo 'selected'; ?>>
                                    <?php echo $item->expense_cat_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Description</label>

                    <div class="col-sm-5">
                                    <textarea type="text" rows="5" name="expense_description" class="form-control"
                                              id="field-1"
                                              placeholder="Enter Description"><?php echo $row['expense_description'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Expense Amount</label>

                    <div class="col-sm-5">
                        <input type="number" name="expense_amount" class="form-control" id="field-1"
                               value="<?php echo $row['expense_amount'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Expense Method</label>

                    <div class="col-sm-5">
                        <select name="expense_method" class="form-control">
                            <option value="Cash" <?php if ($row['expense_method'] == 'Cash') echo 'selected'; ?>>Cash
                            </option>
                            <option value="Cheque" <?php if ($row['expense_method'] == 'Cheque') echo 'selected'; ?>>
                                Cheque
                            </option>
                            <option value="Others" <?php if ($row['expense_method'] == 'Others') echo 'selected'; ?>>
                                Others
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Cheque No (If applicable)</label>

                    <div class="col-sm-5">
                        <input type="text" name="cheque_no" class="form-control" id="field-1"
                               value="<?php echo $row['cheque_no'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="expense_date"
                               value="<?php echo date('m/d/Y', $row['expense_date']); ?>" data-start-date="-4w"
                               data-end-date="+4w">
                    </div>
                    <span>Month-Date-Year</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Status</label>

                    <div class="col-sm-5">
                        <select name="expense_status" class="form-control">
                            <option value="1" <?php if ($row['expense_status'] == 1) echo 'selected'; ?>>Money Paid
                            </option>
                            <option value="0" <?php if ($row['expense_status'] == 0) echo 'selected'; ?>>Yet to pay
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>