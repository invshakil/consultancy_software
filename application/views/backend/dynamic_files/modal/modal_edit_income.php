<?php
$edit_data = $this->db->get_where('income', array('income_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/income_record/do_update/<?php echo $row['income_id'] ?>"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Title</label>

                    <div class="col-sm-5">
                        <select name="income_title" class="form-control">
                            <?php $type = $this->db->get_where('income_category', array('status' => 1))
                                ->result();
                            foreach ($type as $item){
                                ?>
                                <option value="<?php echo $item->income_cat_id;?>"
                                <?php if ($item->income_cat_id == $row['income_title']) echo 'selected' ;?>>
                                    <?php echo $item->income_cat_name;?></option>
                            <?php } ?>
                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Description</label>

                    <div class="col-sm-5">
                                    <textarea type="text" rows="5" name="income_description" class="form-control" id="field-1"
                                              placeholder="Enter Description"><?php echo $row['income_description']?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Income Amount</label>

                    <div class="col-sm-5">
                        <input type="number" name="income_amount" class="form-control" id="field-1"
                               value="<?php echo $row['income_amount']?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Income Method</label>

                    <div class="col-sm-5">
                        <select name="income_method" class="form-control">
                            <option value="Cash" <?php if ($row['income_method'] == 'Cash') echo 'selected'; ?>>Cash</option>
                            <option value="Cheque" <?php if ($row['income_method'] == 'Cheque') echo 'selected'; ?>>Cheque</option>
                            <option value="Others" <?php if ($row['income_method'] == 'Others') echo 'selected'; ?>>Others</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Cheque No (If applicable)</label>

                    <div class="col-sm-5">
                        <input type="text" name="cheque_no"  class="form-control" id="field-1"
                               value="<?php echo $row['cheque_no']?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="income_date" value="<?php echo date('m/d/Y', $row['income_date']); ?>" data-start-date="-4w" data-end-date="+4w">
                    </div>
                    <span>Month-Date-Year</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Status</label>

                    <div class="col-sm-5">
                        <select name="income_status" class="form-control">
                            <option value="1" <?php if ($row['income_status'] == 1) echo 'selected'; ?>>Money Received</option>
                            <option value="0" <?php if ($row['income_status'] == 0) echo 'selected'; ?>>Yet to receive</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>