<?php
$edit_data = $this->db->get_where('clients', array('client_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/clients/do_update/<?php echo $row['client_id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Client Type</label>

                    <div class="col-sm-5">
                        <select name="client_type" class="form-control">
                            <option value="1">Select client type</option>
                            <?php $c_type = $this->db->get_where('client_type', array('status' => 1))->result();
                            foreach ($c_type as $item) { ?>
                                <option
                                    value="<?php echo $item->id ?>" <?php if ($row['client_type'] == $item->id) echo 'selected'; ?> >
                                    <?php echo $item->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Client Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="client_name" class="form-control" id="field-1"
                               value="<?php echo $row['client_name']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Visa Type</label>

                    <div class="col-sm-5">
                        <select name="visa_type" class="form-control">
                            <option value="1">Select visa type</option>
                            <?php $v_type = $this->db->get_where('visa_type', array('status' => 1))->result();
                            foreach ($v_type as $item) { ?>
                                <option
                                    value="<?php echo $item->visa_type_id ?>" <?php if ($row['visa_type'] == $item->visa_type_id) echo 'selected'; ?> >
                                    <?php echo $item->visa_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Address</label>

                    <div class="col-sm-5">
                        <input type="text" name="address" class="form-control" id="field-1"
                               value="<?php echo $row['address']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Contact Number</label>

                    <div class="col-sm-5">
                        <input type="text" name="phone" class="form-control" id="field-1"
                               value="<?php echo $row['phone']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Profession</label>

                    <div class="col-sm-5">
                        <input type="text" name="profession" class="form-control" id="field-1"
                               value="<?php echo $row['profession']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Passport Status</label>

                    <div class="col-sm-5">
                        <select name="passport_status" class="form-control">
                            <option value="1" <?php if ($row['passport_status'] == 1) echo 'selected'; ?>>
                                Yes, Client has Passport
                            </option>
                            <option value="0" <?php if ($row['passport_status'] == 0) echo 'selected'; ?>>
                                No, Client still did not get passport
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Interested Country</label>

                    <div class="col-sm-5">
                        <select name="country" class="form-control">
                            <option value="1">Select Country</option>
                            <?php $country = $this->db->get_where('countries', array('status' => 1))->result();
                            foreach ($country as $item) { ?>
                                <option
                                    value="<?php echo $item->id ?>" <?php if ($row['country'] == $item->id) echo 'selected'; ?> >
                                    <?php echo $item->country_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Sex</label>

                    <div class="col-sm-5">
                        <select name="sex" class="form-control">
                            <option value="1" <?php if ($row['sex'] == 1) echo 'selected'; ?> >
                                Male
                            </option>
                            <option value="2" <?php if ($row['sex'] == 2) echo 'selected'; ?> >
                                Female
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Clients Consultant</label>

                    <div class="col-sm-5">
                        <select name="agent_status" class="form-control">
                            <option>Select</option>
                            <option value="0" <?php if ($row['agent_status'] == 0) echo 'selected'; ?>>Employee</option>
                            <option value="1" <?php if ($row['agent_status'] == 1) echo 'selected'; ?>>Agent</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Clients Consultant</label>

                    <div class="col-sm-5">
                        <select name="added_by" id="emp" class="form-control" required>
                            <?php if ($row['agent_status'] == 0) {
                                $result = $this->db->get_where('employees', array('employee_status' => 1))->result();
                                foreach ($result as $r) { ?>

                                    <option
                                        value="<?php echo $r->employee_id ?>" <?php if ($row['added_by'] == $r->employee_id) echo 'selected'; ?>>
                                        <?php echo $r->employee_name; ?></option>

                                <?php }
                            } else {
                                $result = $this->db->get_where('agents', array('status' => 1))->result();
                                foreach ($result as $r) { ?>

                                    <option
                                        value="<?php echo $r->id ?>" <?php if ($row['added_by'] == $r->id) echo 'selected'; ?>>
                                        <?php echo $r->name; ?></option>


                                <?php }
                            } ?>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Activity Status</label>

                    <div class="col-sm-5">
                        <select name="activity_status" class="form-control">
                            <option value="visit" <?php if ($row['activity_status'] == 'visit') echo 'selected'; ?>>On
                                Visit
                            </option>
                            <option
                                value="document process" <?php if ($row['activity_status'] == 'document process') echo 'selected'; ?>>
                                Document Collect & Verify
                            </option>
                            <option
                                value="visa application" <?php if ($row['activity_status'] == 'visa application') echo 'selected'; ?>>
                                Visa application
                            </option>
                            <option
                                value="visa application" <?php if ($row['activity_status'] == 'visa reject') echo 'selected'; ?>>
                                Visa reject
                            </option>
                            <option
                                value="visa successful" <?php if ($row['activity_status'] == 'visa successful') echo 'selected'; ?>>
                                Visa successful
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>

<script type="text/javascript">
    $ = jQuery;

    $(document).ready(function () {
        $('select[name="agent_status"]').on('change', function () {
            var agent_status = $(this).val();

            if (agent_status != '') {
                $.ajax({
                    url: '<?php echo base_url()?>admin_panel/get_emp_data/' + agent_status,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        console.log(data);
                        $('select[name="added_by"]').empty();
                        $.each(data, function (key, value) {
                            if(agent_status == 0)
                            {
                                $('select[name="added_by"]').append('<option value="'+ value.employee_id +'">' + value.employee_name + '</option>');
                            }
                            else{
                                $('select[name="added_by"]').append('<option value="'+ value.id +'">' + value.name + '</option>');
                            }

                        });
                    }
                });
            }

            else {
                $('select[name="added_by"]').empty();
            }
        });
    });
</script>
