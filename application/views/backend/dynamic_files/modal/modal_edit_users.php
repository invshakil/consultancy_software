<?php
$edit_data		=	$this->db->get_where('admin_staff' , array('admin_staff_id' => $param2) )->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>admin_panel/system_users/do_update/<?php echo $row['admin_staff_id']?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"> Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="name" class="form-control" value="<?php echo $row['name']?>" id="field-1" placeholder="Enter Name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Image Upload</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php if ($row['image']) {?>
                                <img src="<?php echo base_url().$row['image']?>" alt="...">
                                <?php } else {?>
                                <img src="http://placehold.it/500x500" alt="...">
                                <?php }?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">User Email</label>

                    <div class="col-sm-5">
                        <input type="text" name="email" class="form-control" id="field-1"
                               value="<?php echo $row['email'];?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Password</label>

                    <div class="col-sm-5">
                        <input type="password" name="password" value="<?php echo $row['password'];?>" class="form-control" id="field-1"
                               placeholder="Enter Password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Access Level</label>

                    <div class="col-sm-5">
                        <select name="access_level" class="form-control">
                            <option value="1" <?php if($row['access_level'] ==1 ) echo 'selected';?>>Admin</option>
                            <option value="2" <?php if($row['access_level'] ==2 ) echo 'selected';?>>Consultant</option>
                            <option value="3" <?php if($row['access_level'] ==3 ) echo 'selected';?>>Accountant</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Activity Status</label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control">
                            <option value="1" <?php if($row['status'] ==1 ) echo 'selected';?>
                            >Published</option>
                            <option value="0"<?php if($row['status'] ==0 ) echo 'selected';?>
                            >Pending</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach;?>
    </div>
</div>