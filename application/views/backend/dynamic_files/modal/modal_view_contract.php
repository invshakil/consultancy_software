<?php
$data = $this->db->join('clients c', 'c.client_id = cc.client_id')
    ->get_where('client_contract cc', array('cc.client_id' => $param2))->row();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <div class="row">
            <div class="col-md-6">
                <h4><b><?php echo $data->client_name;?></b></h4>
                <hr style="background-color: black;opacity: .6;color: white;padding: 1px;">
                <i class="fa fa-calendar-o"></i> <?php echo date('d-M-Y h:i A', strtotime($data->contract_date));?>
               <br/><br/>
                <i class="fa fa-shopping-cart"></i> Contract Fee: <?php echo $data->contract_amount.' BDT';?>
                <hr style="background-color: black;opacity: .6;color: white;padding: 1px;">
                <br/>

            </div>
            <div class="col-md-6"></div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <?php echo $data->contract_description  ;?>
                <hr style="background-color: black;opacity: .6;color: white;padding: 1px;">
            </div>
        </div>

        <div class="row" style="padding-top: 80px; font-size: 20px; font-family: 'Comic Sans MS';">
            <div class="col-md-6 pull-right">

                <b>Consultant:</b> <b><?php $id = $data->consultant_id; echo $name = $this->db
                ->get_where('admin_staff', array('admin_staff_id' => $id))->row('name');?></b>
                <hr style="background-color: black;opacity: .6;color: white;padding: 1px;">
            </div>
        </div>
    </div>
</div>
