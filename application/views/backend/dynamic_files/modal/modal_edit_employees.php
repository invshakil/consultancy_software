<?php
$edit_data = $this->db->get_where('employees', array('employee_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/employees/do_update/<?php echo $row['employee_id'] ?>"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Employee Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="employee_name" class="form-control" id="field-1"
                               value="<?php echo $row['employee_name'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Employee Code</label>

                    <div class="col-sm-5">
                        <input type="text" name="employee_code" class="form-control" id="field-1"
                               value="<?php echo $row['employee_code'] ?>">
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"> Email</label>

                    <div class="col-sm-5">
                        <input type="text" name="email" class="form-control" id="field-1"
                               value="<?php echo $row['email'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Designation</label>

                    <div class="col-sm-5">
                        <select name="designation" class="form-control">
                            <?php $type = $this->db->get_where('employee_designation', array('visibility_status' => 1))
                                ->result();
                            foreach ($type as $item){
                                ?>
                                <option value="<?php echo $item->designation_id;?>"
                                <?php if($item->designation_id == $row['designation']) echo 'selected';?>>
                                    <?php echo $item->designation_name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Join Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="join_date" value="<?php echo date('m/d/Y', $row['join_date']) ?>" >
                        <span>Month-Date-Year</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"> Phone</label>

                    <div class="col-sm-5">
                        <input type="text" name="phone" class="form-control" id="field-1"
                               value="<?php echo $row['phone'] ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Employee Status</label>

                    <div class="col-sm-5">
                        <select name="employee_status" class="form-control">
                            <option value="1" <?php if($row['employee_status'] == 1) echo 'selected';?> >Yes</option>
                            <option value="2" <?php if($row['employee_status'] == 2) echo 'selected';?>>No</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>