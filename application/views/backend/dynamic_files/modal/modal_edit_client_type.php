<?php
$edit_data		=	$this->db->get_where('client_type' , array('id' => $param2) )->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>admin_panel/client_type/do_update/<?php echo $row['id']?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"> Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="name" class="form-control" value="<?php echo $row['name']?>" id="field-1" placeholder="Enter Name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Description</label>

                    <div class="col-sm-5">
                        <textarea type="text" name="description" class="form-control" id="field-1" rows="5"><?php echo $row['description'];?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Activity Status</label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control">
                            <option value="1" <?php if($row['status'] ==1 ) echo 'selected';?>
                            >Published</option>
                            <option value="0"<?php if($row['status'] ==0 ) echo 'selected';?>
                            >Pending</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach;?>
    </div>
</div>