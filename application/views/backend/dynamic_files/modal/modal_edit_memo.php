<?php
$edit_data = $this->db->get_where('client_billing', array('billing_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/cash_memo/do_update/<?php echo $row['billing_id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Client</label>

                    <div class="col-sm-5">
                        <select name="client_id" class="form-control select2">
                            <option value="1">Select client type</option>
                            <?php $c = $this->db->get('clients')->result();
                            foreach ($c as $item) { ?>
                                <option value="<?php echo $item->client_id ?>"
                                    <?php if ($row['client_id']==$item->client_id) echo 'selected'?>
                                ><?php echo $item->client_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Cash Memo Code</label>

                    <div class="col-sm-5">
                        <input type="text" name="bill_code" value="<?php echo $row['bill_code']; ?>" class="form-control" id="field-1"
                               placeholder="Enter Cash Memo Code">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Memo Type</label>

                    <div class="col-sm-5">
                        <select name="bill_type" class="form-control">
                            <option value="1">Select Memo Type</option>
                            <?php $v = $this->db->get('cash_memo_type')->result();
                            foreach ($v as $item) { ?>
                                <option
                                    value="<?php echo $item->m_t_id ?>" <?php if ($row['bill_type']==$item->m_t_id) echo 'selected'?>
                                ><?php echo $item->m_t_title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Cash Amount</label>

                    <div class="col-sm-5">
                        <input type="text" name="bill_amount" value="<?php echo $row['bill_amount']; ?>" class="form-control" id="field-1"
                               placeholder="Enter Amount">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Payment Status</label>

                    <div class="col-sm-5">
                        <select name="bill_status" class="form-control">
                            <option value="1" <?php if ($row['bill_status'] == 1) echo 'selected'; ?>
                            >Paid
                            </option>
                            <option value="0"<?php if ($row['bill_status'] == 0) echo 'selected'; ?>
                            >Unpaid
                            </option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>