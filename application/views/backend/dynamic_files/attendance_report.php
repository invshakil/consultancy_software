<div class="row">

    <?php if (!isset($date1) && !isset($date2)): ?>

        <div class="col-md-12">
            <h1>Attendance report by date</h1>
            <hr/>
            <div class="box-content">
                <form class='form-horizontal form-groups-bordered validate'
                      action="<?= base_url() ?>admin_panel/attendance_report" method="post">

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">First Date</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="date1" value=""
                                   data-start-view="1">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">Last Date</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="date2" value=""
                                   data-start-view="1">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info">Get Report</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php endif; ?>


    <?php if (isset($date1) && isset($date2)): ?>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <h1 class="text-center"><?php echo 'Attendance Report: ' . date('d F Y', $date1) . ' - ' . date('d F Y', $date2); ?></h1>
                </div>
            </div>
        </div>

        <div class="table-responsive">

            <?php
            $this->session->flashdata('message');
            ?>
            <br><br>
            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th width="10%">
                        <div class="pull-left">
                            Name <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                        </div>
                        <div>
                            <br/>
                        </div>
                        <div class="pull-right">
                            Date <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </div>


                    </th>
                    <?php foreach ($working_days as $date) { ?>
                        <th width="1%"><?php echo date('D d/m', $date->date);?></th>
                    <?php } ?>

                </tr>
                </thead>
                <tbody>

                <?php $emp_list = $this->db->get('employees')->result();
                foreach ($emp_list as $emp) {
                    ?>
                    <tr>

                        <td><?php echo $emp->employee_name;?></td>

                        <?php foreach ($working_days as $date) { ?>
                            <td>
                            <?php $att_check = $this->db->get_where('attendance',
                                array(
                                    'employee_id' => $emp->employee_id,
                                    'date' => $date->date,
                                )
                            )->row('status');

                            if ($att_check == 1){

                                ?>
                                <div class="label label-success">P</div>
                            <?php } elseif($att_check != 1) {?>

                                <div class="label label-danger">A</div>
                            <?php } else {?>
                                <div class="label label-warning">U</div>
                                </td>
                            <?php } } ?>

                    </tr>
                <?php }?>

                </tbody>


            </table>

        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <h3 class="text-center"><?php echo 'Total Working Days: ' .count($working_days); ?></h3>

                    <h5>*P: Present</h5>
                    <h5>*A: Absent</h5>
                    <h5>*U: Undefined</h5>
                </div>
            </div>
        </div>


            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
                Print Attendance Report
                <i class="entypo-doc-text"></i>
            </a>
            <br/><br/>


    <?php endif; ?>


</div>
