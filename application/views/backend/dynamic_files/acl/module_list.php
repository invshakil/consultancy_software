<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>MODULE LIST</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">MODULE LIST</a></li>

            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">MODULE LIST</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <div class="panel-body col-md-12 table-responsive">
                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th class="center">ID</th>
                                    <th>Module Name</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $info = $this->db->get('module_list')->result();
                                foreach ($info as $row) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $row->module_id; ?></td>
                                        <td><?php echo $row->module_name; ?></td>
                                        <td class="center"><?php if ($row->status == 1) {
                                                echo '<div class="label label-success">Active</div>';
                                            } else {
                                                echo '<div class="label label-danger">Inactive</div>';
                                            } ?>
                                        </td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>


</div>
