<div class="row">

    <?php if (!isset($admin_name)) { ?>

        <div class="col-md-12">
            <form role="form" class="form-horizontal form-groups validate"
                  action="<?php echo base_url() ?>admin_panel/manage_access_control/manage_access" method="post">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Select Admin</label>
                    <div class="col-sm-5">
                        <select name="admin_staff_id" class="form-control">
                            <option>Select</option>
                            <?php $admins = $this->db->get_where('admin_staff', array('status' => 1))->result();
                            foreach ($admins as $admin) {
                                ?>
                                <option
                                    value="<?php echo $admin->admin_staff_id; ?>"><?php echo $admin->name; ?></option>
                            <?php } ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-info" id="submit-button" name="go">
                            Get Access Control Data
                        </button>
                    </div>
                </div>

                <hr>

            </form>
        </div>
    <?php } else { ?>

        <div class="col-md-12">
            <div class="tile-stats tile-white-gray">
                <div class="icon" style="bottom: 45px!important;"><i class="fa fa-user"></i></div>
                <h2><?php echo 'Manage Access Control For: ' . $admin_name; ?></h2>
            </div>
        </div>
        <div class="col-md-12" style="color: #4f4f4f;font-weight: bold">
            <table class="table table-bordered responsive">
                <thead>

                <tr>

                    <?php $modules = $this->db->order_by('module_id', 'asc')
                        ->get_where('module_list', array('status' => 1))->result();

                    foreach ($modules as $module) {
                        ?>

                        <th>
                            <?php echo $module->module_name; ?>
                            <input type="hidden" name="module_id"
                                   value="<?php echo $module->module_id; ?>">

                        </th>
                    <?php } ?>

                </tr>


                </thead>
                <tbody>


                <tr>
                    <?php $accesses = $this->db->order_by('module_id', 'asc')->get_where('access_control', array('admin_staff_id' => $admin_id))->result();
                    foreach ($accesses as $key => $access) {
                        ?>
                        <td height="100">
                            <form action="">

                                <!--CURRENT STATUS-->
                                <?php $status_check = $this->db->get_where('access_control', array(
                                    'control_id' => $access->control_id,
                                    'admin_staff_id' => $admin_id,
                                ))->row('module_access');
                                if ($status_check == 0) {
                                    ?>
                                    <p class="alert alert-danger" id="<?php echo $access->control_id; ?>">Current
                                        Status: Disabled</p>
<!--                                    <p id="view--><?php //echo $access->control_id; ?><!--"></p>-->
                                <?php } else { ?>
                                    <p class="alert alert-success" id="<?php echo $access->control_id; ?>">Current
                                        Status: Enabled</p>
<!--                                    <p id="view--><?php //echo $access->control_id; ?><!--"></p>-->
                                    <?php
                                } ?>
                                <!--CURRENT STATUS-->

                                <br/>
                                <label for="">Change Current Status:</label>
                                <select name="module_access" class="form-control module_access" id="access<?php echo $access->control_id; ?>">
                                    <option value="0"
                                        <?php if ($status_check == 0) echo 'selected'; ?>
                                    >NO
                                    </option>
                                    <option value="1"
                                        <?php if ($status_check == 1) echo 'selected'; ?>
                                    >YES
                                    </option>
                                </select>
                                <input type="hidden" name="control_id" id="control_id" class="form-control"
                                       value="<?php echo $access->control_id; ?>">

                            </form>


                        </td>


                    <?php } ?>
                </tr>


                </tbody>
            </table>
        </div>


    <?php } ?>
</div>

<style>
    .alert-danger {
        background-color: red;
        color: white;
    }

    .alert-success {
        background-color: #00a65a;
        color: white;
    }
</style>


<script>
    $ = jQuery;

    $(document).ready(function () {


        var baseurl = '<?php echo base_url(); ?>';

        $(document).on('change', '.module_access', function () {


            if ($(this).val() != "undefined" && $(this).val() != '') {
                var formData = {
                    'module_access': $(this).val(),
                    'control_id': $(this).next().val()
                };
            }

            var request = $.ajax({
                type: 'POST',
                url: baseurl + 'admin_panel/change_access_settings',
                data: formData,
                dataType: 'text'

            });
            request.done(function (msg) {

                var module_access = $('#access'+msg).val();

                if(module_access == 1)
                {
                    $('#'+msg).removeClass('alert-danger').addClass('alert-success').html('Current Status: Enabled');
                }
                if (module_access == 0)
                {
                    $('#'+msg).removeClass('alert-success').addClass('alert-danger').html('Current Status: Disabled');
                }

                toastr.success("Access Control Changed successfully!");

            });
            request.fail(function (data) {
                toastr.error("Something went Wrong!");
            });
        });

    });


</script>
