<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Individual Client / Company Clients</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Individual Client List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Company Client List</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Individual Client List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">ID</th>
                                <th>Client Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Opened Date</th>
                                <th width="20%">Description</th>
                                <th width="15%">options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->where('client_type','Individual Client')->get('company_clients')->result();

                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td width="1%"><?php echo $row->c_client_id; ?></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->phone; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo $row->date; ?></td>
                                    <td>
                                        <?php $descriptions = json_decode($row->description);
                                        foreach ($descriptions as $description) {
                                            echo '<p><b>' . $description->title . '</b>: ' . $description->description . '</p>';
                                        } ?>
                                    </td>

                                    <td>
                                        <div class="btn-group">

                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle pull-left"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>/modal/popup/modal_edit_company_client/<?php echo $row->c_client_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>/admin_panel/company_clients/delete/<?php echo $row->c_client_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>


                                        </div>
                                        &nbsp;
                                        <a href="<?php echo base_url()?>admin_panel/profile/<?php echo $row->c_client_id;?>" target="_blank" class="btn btn-success btn-sm">Profile</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Company Client List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">ID</th>
                                <th>Client Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Opened Date</th>
                                <th width="20%">Description</th>
                                <th width="15%">options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->where('client_type','Company Client')->get('company_clients')->result();

                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td width="1%"><?php echo $row->c_client_id; ?></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->phone; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo $row->date; ?></td>
                                    <td>
                                        <?php $descriptions = json_decode($row->description);
                                        foreach ($descriptions as $description) {
                                            echo '<p><b>' . $description->title . '</b>: ' . $description->description . '</p>';
                                        } ?>
                                    </td>

                                    <td>
                                        <div class="btn-group">

                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle pull-left"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>/modal/popup/modal_edit_company_client/<?php echo $row->c_client_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>/admin_panel/company_clients/delete/<?php echo $row->c_client_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>


                                        </div>
                                        &nbsp;
                                        <a href="<?php echo base_url()?>admin_panel/profile/<?php echo $row->c_client_id;?>" target="_blank" class="btn btn-success btn-sm">Profile</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>


<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<script>

    // CREATING BLANK allowance ENTRY
    var blank_desc_entry = '';
    $(document).ready(function () {
        blank_desc_entry = $('#entry').html();
    });

    function add_desc_entry() {
        $("#entry").append(blank_desc_entry);
    }

    // REMOVING  ENTRY
    function deleteParentElement(n) {
        n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
    }


</script>


<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>