<div class="panel minimal minimal-gray">
    <?php
        $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Employees</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Employee List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Employee</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Employee List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">ID</th>
                                <th class="center">Employee Code</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Designation</th>
                                <th>Join Date</th>
                                <th>Phone</th>
                                <th>Employee Status</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->select('ed.designation_name, e.*')
                                ->join('employee_designation ed', 'ed.designation_id = e.designation')
                                ->get('employees e')->result();
                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->employee_id; ?></td>
                                    <td><?php echo $row->employee_code; ?></td>
                                    <td><?php echo $row->employee_name; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo $row->designation_name; ?></td>
                                    <td><?php echo date('d-M-Y', $row->join_date); ?></td>
                                    <td><?php echo $row->phone; ?></td>
                                    <td>
                                        <?php if ($row->employee_status == 1) {
                                            echo '<div class="label label-success">Yes</div>';
                                        }
                                         else {
                                            echo '<div class="label label-danger">No</div>';
                                        } ?>
                                    </td>

                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_employees/<?php echo $row->employee_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>admin_panel/employees/delete/<?php echo $row->employee_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Employee</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>admin_panel/employees/create" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Employee Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="employee_name" class="form-control" id="field-1"
                                           placeholder="Enter Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Employee Code</label>

                                <div class="col-sm-5">
                                    <input type="text" name="employee_code" class="form-control" id="field-1"
                                           placeholder="Enter Code">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"> Email</label>

                                <div class="col-sm-5">
                                    <input type="text" name="email" class="form-control" id="field-1"
                                           placeholder="Enter Email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Designation</label>

                                <div class="col-sm-5">
                                    <select name="designation" class="form-control">
                                        <?php $type = $this->db->get_where('employee_designation', array('visibility_status' => 1))
                                            ->result();
                                        foreach ($type as $item){
                                            ?>
                                            <option value="<?php echo $item->designation_id;?>"><?php echo $item->designation_name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-2" class="col-sm-3 control-label">Join Date</label>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control datepicker" name="join_date" value="" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"> Phone</label>

                                <div class="col-sm-5">
                                    <input type="text" name="phone" class="form-control" id="field-1"
                                           placeholder="Enter Phone Number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Employee Status</label>

                                <div class="col-sm-5">
                                    <select name="employee_status" class="form-control">
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Employee</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>