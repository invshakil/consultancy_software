<div class="row">
    <div class="col-md-12">
        <?php
        $this->session->flashdata('message');
        ?>
        <!------CONTROL TABS START------->
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    Salary template list
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    Set salary template
                </a></li>
        </ul>
        <!------CONTROL TABS END------->
        <div class="tab-content">
            <!----TABLE LISTING STARTS--->
            <div class="tab-pane box active" id="list">
                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>

                        <th>
                            <div>Salary grades</div>
                        </th>
                        <th>
                            <div>Basic salary</div>
                        </th>
                        <th>
                            <div>Overtime (Per hour)</div>
                        </th>
                        <th>
                            <div>Options</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($salary_template_info as $row) { ?>
                        <tr>
                            <td><?php echo $row['salary_grades']; ?></td>
                            <td><?php echo $row['basic_salary']; ?></td>
                            <td><?php echo $row['over_time']; ?></td>
                            <td>
                                <a href="#"
                                   class="btn btn-danger btn-sm btn-icon icon-left"
                                   onclick="confirm_modal('<?php echo base_url(); ?>admin_panel/salary_template_delete/<?php echo $row['salary_template_id'] ?>');">
                                    <i class="entypo-cancel"></i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <form role="form" class="form-horizontal validate form-groups invoice-add"
                          action="<?php echo base_url(); ?>admin_panel/salary_grades" method="post"
                          enctype="multipart/form-data">


                        <div class="row">
                            <div class="col-md-12">


                                <div class="panel-body">

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Salary grades</label>

                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="salary_grades"
                                                   id="salary_grades" data-validate="required"
                                                   data-message-required="value required" value="" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Basic salary</label>

                                        <div class="col-sm-5">
                                            <input type="number" class="form-control" name="basic_salary"
                                                   id="basic_salary" data-validate="required"
                                                   data-message-required="value required"
                                                    ondrop="return false;"
                                                   onpaste="return false;" type="number" step="any" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Overtime(Per hour)</label>

                                        <div class="col-sm-5">
                                            <input type="number" class="form-control" name="over_time" id="over_time"
                                                   data-validate="required"
                                                   data-message-required="value required" value="" autofocus>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="row">

                            <!--ALLOWANCE-->

                            <div class="col-md-6">

                                <div class="panel panel-primary" data-collapsed="0">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <i class="entypo-plus-circled"></i>
                                            Allowances
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <!-- FORM ENTRY STARTS HERE-->
                                        <div id="allowances_entry">
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-2 control-label">Amount</label>

                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="entry_allowance_description[]" value=""
                                                           placeholder="Description">
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control allowance_amount"
                                                           name="entry_allowance_amount[]" value=""
                                                           placeholder="Amount">
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn btn-default"
                                                            onclick="deleteParentElement(this)">
                                                        <i class="entypo-trash"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- FORM ENTRY ENDS HERE-->


                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <button type="button" class="btn btn-default btn-sm btn-icon icon-left"
                                                        onClick="add_allowances_entry()">
                                                    Add allowance entry
                                                    <i class="entypo-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--DEDUCTION-->

                            <div class="col-md-6">
                                <div class="panel panel-primary" data-collapsed="0">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <i class="entypo-plus-circled"></i>
                                            Deductions
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <!-- FORM ENTRY STARTS HERE-->
                                        <div id="deductions_entry">

                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-2 control-label">Amount</label>

                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control"
                                                           name="entry_deduction_description[]" value=""
                                                           placeholder="Description">
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control deduction_amount"
                                                           name="entry_deduction_amount[]" value=""
                                                           placeholder="Amount">
                                                </div>
                                                <div class="col-sm-1">
                                                    <button type="button" class="btn btn-default"
                                                            onclick="deleteParentElement(this)">
                                                        <i class="entypo-trash"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- FORM ENTRY ENDS HERE-->


                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <button type="button" class="btn btn-default btn-sm btn-icon icon-left"
                                                        onClick="add_deductions_entry()">
                                                    Add deductions entry
                                                    <i class="entypo-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-primary" data-collapsed="0">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <i class="entypo-plus-circled"></i>

                                        </div>
                                    </div>
                                    <div class="panel-body">


                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Gross salary</label>

                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="gross_salary"
                                                       id="gross_salary" data-validate="required"
                                                       data-message-required="value required"
                                                       onkeypress="return IsNumeric(event);" ondrop="return false;"
                                                       onpaste="return false;" type="number" step="any" autofocus
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Total Deduction</label>

                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="total_deduction"
                                                       id="total_deduction" data-validate="required"
                                                       data-message-required="value required"
                                                       onkeypress="return IsNumeric(event);" ondrop="return false;"
                                                       onpaste="return false;" type="number" step="any" autofocus
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Net salary</label>

                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="net_salary"
                                                       id="net_salary" data-validate="required"
                                                       data-message-required="value required"
                                                       onkeypress="return IsNumeric(event);" ondrop="return false;"
                                                       onpaste="return false;" type="number" step="any" autofocus
                                                       readonly>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <button type="submit" class="btn btn-info" id="submit-button">
                                                    Save
                                                    <span id="preloader-form"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!----CREATION FORM ENDS--->

        </div>
    </div>
</div>

<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ----->
<script type="text/javascript">

    jQuery(document).ready(function ($) {


        // var datatable = $("#table_export").dataTable();

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });


    // CREATING BLANK allowance ENTRY
    var blank_allowance_entry = '';
    $(document).ready(function () {
        blank_allowance_entry = $('#allowances_entry').html();
    });

    function add_allowances_entry() {
        $("#allowances_entry").append(blank_allowance_entry);
    }


    // CREATING BLANK deduction ENTRY
    var blank_deduction_entry = '';
    $(document).ready(function () {
        blank_deduction_entry = $('#deductions_entry').html();
    });

    function add_deductions_entry() {
        $("#deductions_entry").append(blank_deduction_entry);
    }

    // REMOVING  ENTRY
    function deleteParentElement(n) {
        n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
    }

    $(document).on('change keyup blur', '#basic_salary', function () {
        calculateTotal();
    });
    $(document).on('change keyup blur', '.deduction_amount', function () {
        calculateTotal();
    });
    $(document).on('change keyup blur', '.allowance_amount', function () {
        calculateTotal();
    });

    function calculateTotal() {
        gross_salary = 0;
        net_salary = 0;
        total_deduction = 0;
        total_allowance = 0;
        basic_salary = $('#basic_salary').val();


        if (basic_salary != '' && typeof(basic_salary) != "undefined") {
            gross_salary += parseFloat(basic_salary);
            net_salary = gross_salary;
        }

        $('.deduction_amount').each(function () {
            if ($(this).val() != '') {
                total_deduction += parseFloat($(this).val());

            }
        });
        $('.allowance_amount').each(function () {
            if ($(this).val() != '') {
                total_allowance += parseFloat($(this).val());


            }
        });
        gross_salary += total_allowance;
        net_salary += total_allowance;
        net_salary -= parseFloat(total_deduction);


        $('#total_deduction').val(total_deduction);
        $('#gross_salary').val(gross_salary);
        $('#net_salary').val(net_salary);
    }


</script>