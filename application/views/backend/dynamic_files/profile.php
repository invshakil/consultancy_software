<?php $info = $this->db->get_where('company_clients', array('c_client_id' => $id))->row();
?>

<div class="member-entry">

    <?php $settings_logo = $this->db->where('info_type', 'settings_logo')
        ->get('system_settings')->row('description'); ?>

    <div class="col-md-4 pull-left">
        <img src="<?php echo base_url() . $settings_logo; ?>" width="200" class="img-responsive">
    </div>
    <div class="col-md-8 pull-right">
        <?php $name = $this->db->where('info_type', 'settings_name')
            ->get('system_settings')->row('description'); ?>
        <h1 style="text-align: right"><?php echo $name; ?></h1>
        <p style="text-align: right"><?php echo $info->date; ?></p>
    </div>


</div>

<div class="member-entry">

    <h2 style="text-align: center;padding-bottom: 10px">
        Client Name: <?php echo $info->name; ?>
    </h2>

</div>

<div class="member-entry">

    <div class="member-details">


        <!-- Details with Icons -->
        <div class="row info-list">

            <div class="col-sm-4" style="text-align: center">
                <i class="entypo-briefcase"></i>
                Client Type: <?php echo $info->client_type; ?>
            </div>

            <div class="col-sm-4" style="text-align: center">
                <i class="entypo-phone"></i>
                <?php echo $info->phone; ?>
            </div>

            <div class="col-sm-4" style="text-align: center">
                <i class="entypo-mail"></i>
                <?php echo $info->email; ?>
            </div>


        </div>
    </div>


</div>

<div class="member-entry">
    <div class="col-sm-offset-2 col-sm-8 table-responsive">
        <?php $descriptions = json_decode($info->description); ?>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td colspan="2"><h2 style="text-align: center">More Information</h2></td>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($descriptions as $des) { ?>
                <tr>
                    <td class="text-center"><?php echo $des->title; ?></td>
                    <td class="text-center"><?php echo $des->description; ?></td>
                </tr>

            <?php } ?>


            </tbody>

        </table>
    </div>
</div>


<a onclick="printpage()" id="printpagebutton" class="btn btn-danger btn-lg pull-right"
   style="margin-bottom: 10px;margin-right: 20px">
    Print
    <i class="fa fa-print"></i>
</a>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden'
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print();

        printButton.style.visibility = 'visible';
    }
</script>