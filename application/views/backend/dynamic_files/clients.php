<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Client</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Client List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Client</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Client List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">ID</th>
                                <th>Client Name</th>
                                <th>Client Type</th>
                                <th>Visa Type</th>
                                <th>Phone</th>
                                <th>Activity Status</th>
                                <th>Last Modify Date</th>
                                <th>Added BY</th>
                                <th width="5%">options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->join('client_type', 'id = client_type')
                                ->join('visa_type', 'visa_type_id = visa_type')
                                ->get('clients')->result();

                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->client_id; ?></td>

                                    <td><a href="<?php echo base_url() ?>admin_panel/collect_documents/<?php echo $row->client_id ?>">
                                            <?php echo $row->client_name; ?>
                                        </a>
                                    </td>

                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->visa_name; ?></td>
                                    <td><?php echo $row->phone; ?></td>
                                    <td class="center"><?php if ($row->activity_status == 'visit') {
                                            echo '<div class="label label-info">On Visit</div>';
                                        } elseif ($row->activity_status == 'document process') {
                                            echo '<div class="label label-primary">Document Collect & Verify</div>';
                                        } elseif ($row->activity_status == 'visa application') {
                                            echo '<div class="label label-primary">visa application</div>';
                                        } elseif ($row->activity_status == 'visa collected') {
                                            echo '<div class="label label-danger">visa rejected</div>';
                                        } elseif ($row->activity_status == 'visa successful') {
                                            echo '<div class="label label-success">visa successful</div>';
                                        } ?>
                                        <br/><br/>
                                        <?php if ($row->activity_status == 'visit') { ?>
                                            <a href="<?php echo base_url() ?>admin_panel/collect_documents/<?php echo $row->client_id ?>"
                                               class="label label-primary">
                                                COLLECT DOCUMENT OF THIS CLIENT</a>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo date('d-M-y h:i A', strtotime($row->modify_date)); ?></td>
                                    <td>
                                        <?php
                                        if ($row->agent_status == 0){
                                            echo $name = $this->db
                                                ->get_where('employees', array('employee_id' => $row->added_by))
                                                ->row('employee_name');
                                        }else{
                                            echo 'Agent: ' .$name = $this->db
                                                ->get_where('agents', array('id' => $row->added_by))
                                                ->row('name');
                                        }


                                        ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">

                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle pull-left"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>/modal/popup/modal_edit_client/<?php echo $row->client_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>/admin_panel/clients/delete/<?php echo $row->client_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Client</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>admin_panel/clients/create"
                              enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Client Type</label>

                                <div class="col-sm-5">
                                    <select name="client_type" class="form-control">
                                        <option value="1">Select client type</option>
                                        <?php $c_type = $this->db->get_where('client_type', array('status' => 1))->result();
                                        foreach ($c_type as $item) { ?>
                                            <option value="<?php echo $item->id ?>"><?php echo $item->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Client Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="client_name" class="form-control" id="field-1"
                                           placeholder="Enter Client Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Visa Type</label>

                                <div class="col-sm-5">
                                    <select name="visa_type" class="form-control">
                                        <option value="1">Select visa type</option>
                                        <?php $v_type = $this->db->get_where('visa_type', array('status' => 1))->result();
                                        foreach ($v_type as $item) { ?>
                                            <option
                                                value="<?php echo $item->visa_type_id ?>"><?php echo $item->visa_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-5">
                                    <input type="text" name="address" class="form-control" id="field-1"
                                           placeholder="Enter Address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Contact Number</label>

                                <div class="col-sm-5">
                                    <input type="text" name="phone" class="form-control" id="field-1"
                                           placeholder="Enter Contact Number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Profession</label>

                                <div class="col-sm-5">
                                    <input type="text" name="profession" class="form-control" id="field-1"
                                           placeholder="Enter Profession">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Passport Status</label>

                                <div class="col-sm-5">
                                    <select name="passport_status" class="form-control">
                                        <option value="1">Yes, Client has Passport</option>
                                        <option value="0">No, Client still did not get passport</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Interested Country</label>

                                <div class="col-sm-5">
                                    <select name="country" class="form-control select2">
                                        <option value="1">Select Country</option>
                                        <?php $country = $this->db->get_where('countries', array('status' => 1))->result();
                                        foreach ($country as $item) { ?>
                                            <option
                                                value="<?php echo $item->id ?>"><?php echo $item->country_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Sex</label>

                                <div class="col-sm-5">
                                    <select name="sex" class="form-control">
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Clients Consultant</label>

                                <div class="col-sm-5">
                                    <select name="agent_status" class="form-control">
                                        <option value="0">Employee</option>
                                        <option value="1">Agent</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Clients Consultant</label>

                                <div class="col-sm-5">
                                    <select name="agent_status" class="form-control">
                                        <option >Select</option>
                                        <option value="0">Employee</option>
                                        <option value="1">Agent</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Clients Consultant</label>

                                <div class="col-sm-5">
                                    <select name="added_by" id="emp" class="form-control" required>


                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Activity Status</label>

                                <div class="col-sm-5">
                                    <select name="activity_status" class="form-control">
                                        <option value="visit">On Visit</option>
                                        <option value="document process">Document Collect & Verify</option>
                                        <option value="visa application">Visa application</option>
                                        <option value="visa rejected">Visa rejected</option>
                                        <option value="visa successful">Visa successful</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Clients Information</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    $ = jQuery;

    $(document).ready(function () {
        $('select[name="agent_status"]').on('change', function () {
            var agent_status = $(this).val();

            if (agent_status != '') {
                $.ajax({
                    url: '<?php echo base_url()?>admin_panel/get_emp_data/' + agent_status,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        console.log(data);
                        $('select[name="added_by"]').empty();
                        $.each(data, function (key, value) {
                            if(agent_status == 0)
                            {
                                $('select[name="added_by"]').append('<option value="'+ value.employee_id +'">' + value.employee_name + '</option>');
                            }
                            else{
                                $('select[name="added_by"]').append('<option value="'+ value.id +'">' + value.name + '</option>');
                            }

                        });
                    }
                });
            }

            else {
                $('select[name="added_by"]').empty();
            }
        });
    });
</script>


<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>