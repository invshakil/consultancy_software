<div class="row draggable-portlets">

    <?php if (count($info_country) > 0) {
        foreach ($info_country as $country) {
            ?>
            <div class="col-md-6 col-xs-12">

                <div class="sorted">

                    <div class="panel panel-success" data-collapsed="0">

                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Client List from <?php echo $this->db->get_where('countries',
                                    array('id' => $country->country))->row('country_name'); ?></div>

                        </div>

                        <!-- panel body -->
                        <div class="panel-body">

                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Last Modify Date</th>
                                    <th>Profile</th>
                                    <th width="5%">options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $info = $this->db->join('client_type', 'id = client_type')
                                    ->join('visa_type', 'visa_type_id = visa_type')
                                    ->where('country', $country->country)
                                    ->where('agent_status',1)
                                    ->get('clients')->result();

                                foreach ($info as $row) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <a href="<?php echo base_url() ?>admin_panel/collect_documents/<?php echo $row->client_id ?>">
                                                <?php echo $row->client_name; ?>
                                            </a>
                                        </td>
                                        <td><?php echo date('d-M-y h:i A', strtotime($row->modify_date)); ?></td>
                                        <td>
                                            <?php
                                            $client_id = $row->client_id;
                                            $info = $this->db->get_where('client_document',array('client_id'=>$client_id))->row();

                                            if (count($info)>0) {?>
                                            <a class="btn btn-primary btn-sm" target="_blank"
                                               href="<?php echo base_url() ?>admin_panel/client_profile/<?php echo $row->client_id ?>">
                                                Visit Profile
                                            </a>
                                                <?php } else {
                                                    echo 'No Document Found yet!';
                                            }?>
                                        </td>
                                        <td>
                                            <div class="btn-group">

                                                <button type="button"
                                                        class="btn btn-info btn-sm dropdown-toggle pull-left"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#"
                                                           onclick="showAjaxModal('<?php echo base_url(); ?>/modal/popup/modal_edit_client/<?php echo $row->client_id; ?>');">
                                                            <i class="entypo-pencil"></i>
                                                            Edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#"
                                                           onclick="confirm_modal('<?php echo base_url(); ?>/admin_panel/clients/delete/<?php echo $row->client_id; ?>');">
                                                            <i class="entypo-trash"></i>
                                                            Delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

            </div>
        <?php }
    } ?>

</div>

<style>
    .panel-success > .panel-heading {
        color: #ebebeb;
        background-color: #00a651;
        border-color: #00a651;
    }
</style>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $(function () {

            var $draggable_portlets = $(".draggable-portlets");

            $(".draggable-portlets .sorted").sortable({
                connectWith: ".draggable-portlets .sorted",
                handle: '.panel-heading',
                start: function () {
                    $draggable_portlets.addClass('dragging');
                },
                stop: function () {
                    $draggable_portlets.removeClass('dragging');
                }
            });

            $(".draggable-portlets .sorted .panel-heading").disableSelection();
        });

    });
</script>