<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Document Process</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">PROCESS ADD/MANAGE</a></li>

            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">PROCESS ADD & LIST</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <div class="panel-body col-md-12">
                            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                  action="<?php echo base_url() ?>admin_panel/document_process/create"
                                  enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Process Name</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="process_name" class="form-control" id="field-1"
                                               placeholder="Enter Process Name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Visibility Status</label>

                                    <div class="col-sm-5">
                                        <select name="status" class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-success">Add Process</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <hr/>
                        <div class="panel-body col-md-12 table-responsive">
                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th class="center">ID</th>
                                    <th>Process Name</th>
                                    <th>Status</th>
                                    <th>options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $info = $this->db->get('document_process')->result();
                                foreach ($info as $row) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $row->process_id; ?></td>
                                        <td><?php echo $row->process_name; ?></td>
                                        <td class="center"><?php if ($row->status == 1) {
                                                echo '<div class="label label-success">Active</div>';
                                            } else {
                                                echo '<div class="label label-danger">Inactive</div>';
                                            } ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#"
                                                           onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_process/<?php echo $row->process_id; ?>');">
                                                            <i class="entypo-pencil"></i>
                                                            Edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#"
                                                           onclick="confirm_modal('<?php echo base_url(); ?>admin_panel/document_process/delete/<?php echo $row->process_id; ?>');">
                                                            <i class="entypo-trash"></i>
                                                            Delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

<!--            <div class="tab-pane" id="profile-2">-->
<!---->
<!--                <div class="panel panel-dark" data-collapsed="0">-->
<!---->
<!--                    <!-- panel head -->-->
<!--                    <div class="panel-heading">-->
<!--                        <div class="panel-title">Add Visa Type</div>-->
<!---->
<!--                        <div class="panel-options">-->
<!--                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i-->
<!--                                    class="entypo-cog"></i></a>-->
<!--                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>-->
<!--                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>-->
<!--                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <!-- panel body -->-->
<!--                    <div class="panel-body">-->
<!---->
<!--                        -->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>