<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>


    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Employee Salary List</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            <div class="panel-body col-md-12 table-responsive">
                <table class="table table-bordered datatable" id="table-1">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Salary Grade</th>
                        <th>Net Salary</th>
                        <th>Over Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $info = $this->db->get('employees')->result();
                    $i = 1;
                    foreach ($info as $row) {
                        ?>
                        <tr class="odd gradeX">
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row->employee_name; ?></td>
                            <td><?php echo $this->db->get_where('employee_designation',
                                    array('designation_id'=>$row->designation))->row('designation_name'); ?></td>

                            <td>
                                <?php $salary_grade = $this->db->get_where('salary_template',
                                    array('salary_template_id'=>$row->salary_template_id))->row('salary_grades');
                                echo ($salary_grade);
                                ?>
                            </td>
                            <td class="center">
                                <?php $net_salary = $this->db->get_where('salary_template',
                                    array('salary_template_id'=>$row->salary_template_id))->row('net_salary');
                                echo number_format($net_salary);
                                ?>
                            </td>
                            <td class="center">
                                <?php $over_time = $this->db->get_where('salary_template',
                                    array('salary_template_id'=>$row->salary_template_id))->row('over_time');
                                echo number_format($over_time);
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>