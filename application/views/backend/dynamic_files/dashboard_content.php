<div class="row">
    <div class="col-sm-3 col-xs-6">

        <div class="tile-stats tile-red">
            <div class="icon"><i class="entypo-users"></i></div>
            <div class="num" data-start="0" data-end="<?php echo count($this->db->get('admin_staff')->result())?>" data-postfix="" data-duration="1500" data-delay="0">0</div>

            <h3>System Users</h3>
        </div>

    </div>

    <div class="col-sm-3 col-xs-6">

        <div class="tile-stats tile-green">
            <div class="icon"><i class="entypo-users"></i></div>
            <div class="num" data-start="0" data-end="<?php echo count($this->db->get('employees')->result())?>" data-postfix="" data-duration="1500" data-delay="1">0</div>

            <h3>Total Employee</h3>
        </div>

    </div>

    <div class="col-sm-3 col-xs-6">

        <div class="tile-stats tile-aqua">
            <div class="icon"><i class="entypo-chart-bar"></i></div>
            <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('clients', array('activity_status' => 'visit'))->result())?>" data-postfix="" data-duration="1500" data-delay="1200">0</div>

            <h3>Total Visitor</h3>
        </div>

    </div>

    <div class="col-sm-3 col-xs-6">

        <div class="tile-stats tile-blue">
            <div class="icon"><i class="entypo-rss"></i></div>
            <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('clients', array('activity_status !=' => 'visit'))->result())?>" data-postfix="" data-duration="1500" data-delay="1800">0</div>

            <h3>Total Client</h3>
        </div>

    </div>
</div>