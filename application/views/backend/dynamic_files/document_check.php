<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title"> DOCUMENT CHECK</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->

        <div class="panel-body">
            <table class="table table-bordered datatable" id="table-1">
                <thead>
                <tr>
<!--                    <th data-hide="phone">Image</th>-->
                    <th class="center">Client Code</th>
                    <th class="center">Client Name</th>
<!--                    <th>Passport</th>-->
<!--                    <th>Academic Transcript</th>-->
<!--                    <th>Visa</th>-->
                    <th>Destination</th>
                    <th>Document Process Stage</th>
                    <th>Document Collected By</th>
                    <th>Last Update On</th>
                    <th width="17%">options</th>
                </tr>
                </thead>
                <tbody>
                <?php $info = $this->db->get('client_document')->result();
                foreach ($info as $row) {
                    ?>
                    <tr class="odd gradeX">
<!--                        <td class="center"><img src="--><?php //echo base_url() . $row->client_image; ?><!--" width="60px"-->
<!--                                                class="img-thumbnail"></td>-->
                        <td><?php echo $row->client_code; ?></td>
                        <td><?php echo $name = $this->db->get_where('clients', array('client_id' => $row->client_id))->row('client_name'); ?></td>
<!--                        <td><img src="--><?php //echo base_url() . $row->client_passport; ?><!--" width="60px"-->
<!--                                 class="img-thumbnail"></td>-->
<!--                        <td><a class="btn btn-danger btn-sm"-->
<!--                               href="--><?php //echo base_url() . $row->academic_transcript; ?><!--">Check transcript</a></td>-->
<!--                        <td><img src="--><?php //echo base_url() . $row->client_visa; ?><!--" width="60px" class="img-thumbnail">-->
<!--                        </td>-->

                        <td><?php echo $row->client_visit_place; ?></td>
                        <td>
                            <div class="label label-success">
                            <?php echo $status = $this->db->get_where('document_process', array('process_id' => $row->document_status))->row('process_name'); ?>
                            </div>
                        </td>
                        <td>
                            <?php
                            echo $name = $this->db
                                ->get_where('admin_staff', array('admin_staff_id' => $row->reviewed_by))->row('name');
                            ?>
                        </td>
                        <td><?php echo date('d-M-y h:i A', strtotime($row->last_update)); ?></td>

                        <td>
                            <a class="btn btn-primary btn-sm pull-right" target="_blank"
                               href="<?php echo base_url() ?>admin_panel/client_profile/<?php echo $row->client_id ?>">
                                Visit Profile
                            </a>
                            <div class="btn-group pull-left">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                        data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="<?php echo base_url() ?>admin_panel/collect_documents/<?php echo $row->client_id ?>"<i
                                            class="entypo-pencil"></i>
                                        Edit
                                        </a>
                                    </li>
                                    <li class="divider"></li>

                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#"
                                           onclick="confirm_modal('<?php echo base_url(); ?>admin_panel/document_check/delete/<?php echo $row->document_id; ?>');">
                                            <i class="entypo-trash"></i>
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>


    </div>
</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>