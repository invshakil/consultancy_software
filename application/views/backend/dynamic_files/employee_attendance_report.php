<?php
$this->session->flashdata('message');
?>
<br><br>
<table class="table table-bordered datatable" id="table_export">
    <thead>
    <tr>
        <th>
            <div>Employee Name</div>
        </th>
        <th>
            <div>Date</div>
        </th>
        <th class="span3">
            <div>Attendance Status</div>
        </th>
        <!--        <th class="span3">-->
        <!--            <div>--><?php //echo get_phrase('options'); ?><!--</div>-->
        <!--        </th>-->
    </tr>
    </thead>
    <tbody>
    <?php
    if (is_array($attendance_info) && count($attendance_info) >= 1) {
        foreach ($attendance_info as $row) { ?>
            <tr>
                <td><?php echo $row->employee_name; ?></td>
                <td><?php $date = date("l, dS F Y", $row->date);
                    echo $date; ?></td>
                <td><?php if ($row->status == 1) {
                        echo '<div class="label label-success">Present</div>';
                    } else echo '<div class="label label-danger">Absent</div>'; ?></td>

            </tr>
        <?php }
    } ?>

    </tbody>
</table>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ----->
<script type="text/javascript">

    jQuery(document).ready(function ($) {


        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [

                    {
                        "sExtends": "xls",
                        "mColumns": [0, 2, 3, 4]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 2, 3, 4]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(1, false);
                            datatable.fnSetColumnVis(5, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(1, true);
                                    datatable.fnSetColumnVis(5, true);
                                }
                            });
                        },

                    },
                ]
            },

        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>