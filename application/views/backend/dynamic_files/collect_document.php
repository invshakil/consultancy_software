<?php
$datas = $this->db->get_where('client_document', array('client_id' => $client_id))->result_array();


if (!$datas) {
    $client_image = null;
    $client_visit_place = null;
    $client_passport = null;
    $academic_transcript = null;
    $medical_report = null;
    $sponsor_document = null;
    $client_visa = null;
    $client_code = null;
    $document_status = null;
} else {
    foreach ($datas as $data) {
        $client_image = $data['client_image'];
        $client_visit_place = $data['client_visit_place'];
        $client_passport = $data['client_passport'];
        $academic_transcript = $data['academic_transcript'];
        $medical_report = $data['medical_report'];
        $sponsor_document = $data['sponsor_document'];
        $client_visa = $data['client_visa'];
        $client_code = $data['client_code'];
        $document_status = $data['document_status'];
    }
}
?>
<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel panel-danger" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Collect document
                of <?php echo $name = $this->db->get_where('clients', array('client_id' => $client_id))->row('client_name'); ?></div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->

        <div class="panel-body">


            <?php if ($datas == NULL) { ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/collect_documents/<?php echo $client_id ?>/create"
                  enctype="multipart/form-data">
                <?php } else { ?>
                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="<?php echo base_url() ?>admin_panel/collect_documents/<?php echo $client_id ?>/do_update"
                      enctype="multipart/form-data">
                    <?php } ?>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Client's Identification Number</label>

                        <div class="col-sm-5">
                            <input type="text" name="client_code" class="form-control" id="field-1" required
                                   value="<?php echo $client_code; ?>"
                                   placeholder="Enter Clients Identification number">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Client's Destination</label>

                        <div class="col-sm-5">
                            <input type="text" name="client_visit_place" class="form-control" id="field-1" required
                                   value="<?php echo $client_visit_place; ?>"
                                   placeholder="Enter University name or Place where client want to go">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client Formal Image</label>

                        <div class="col-sm-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px;"
                                     data-trigger="fileinput">
                                    <?php if ($client_image) { ?>
                                        <img src="<?php echo base_url() . $client_image ?>" alt="...">
                                    <?php } else { ?>
                                        <img src="http://placehold.it/1024x1024" alt="...">
                                    <?php } ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px;"></div>
                                <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="formal_image" accept="image/*">
											</span>
                                    <a href="#" class="btn btn-orange fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Document's Current Status</label>

                        <div class="col-sm-5">
                            <select name="document_status" class="form-control" required>
                                <?php $status = $this->db->get_where('document_process', array('status' => 1))->result();
                                foreach ($status as $item) { ?>
                                    <option value="<?php echo $item->process_id ?>"
                                        <?php
                                        if ($document_status) {
                                            if ($document_status == $item->process_id) {
                                                echo 'selected';
                                            }
                                        }
                                        ?>

                                    >
                                        <?php echo $item->process_name ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Submit Primary Info</button>
                        </div>
                    </div>
                </form>

        </div>




    </div>
</div>


<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel panel-warning" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Save Passport & Visa Scan Copy
                of <?php echo $name = $this->db->get_where('clients', array('client_id' => $client_id))->row('client_name'); ?></div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->

        <div class="panel-body">


            <?php if ($datas == NULL) { ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/passport_visa"
                  enctype="multipart/form-data">
                <?php } else { ?>
                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/passport_visa"
                      enctype="multipart/form-data">
                    <?php } ?>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client Passport Scanned Image</label>

                        <div class="col-sm-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px;"
                                     data-trigger="fileinput">
                                    <?php if ($client_passport) { ?>
                                        <img src="<?php echo base_url() . $client_passport ?>" alt="...">
                                    <?php } else { ?>
                                        <img src="http://placehold.it/1024x720" alt="...">
                                    <?php } ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px"></div>
                                <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="passport" accept="image/*">
											</span>
                                    <a href="#" class="btn btn-orange fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client's Visa Scanned Image</label>

                        <div class="col-sm-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px;"
                                     data-trigger="fileinput">
                                    <?php if ($client_visa) { ?>
                                        <img src="<?php echo base_url() . $client_visa ?>" alt="...">
                                    <?php } else { ?>
                                        <img src="http://placehold.it/1024x720" alt="...">
                                    <?php } ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px"></div>
                                <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="visa" accept="image/*">
											</span>
                                    <a href="#" class="btn btn-orange fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Submit Documents</button>
                        </div>
                    </div>
                </form>

        </div>


    </div>
</div>



<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel panel-success" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Collect Transcript, Sponsor document
                of <?php echo $name = $this->db->get_where('clients', array('client_id' => $client_id))->row('client_name'); ?></div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->




        <div class="panel-body">


            <?php if ($datas == NULL) { ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/transcript"
                  enctype="multipart/form-data">
                <?php } else { ?>
                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/transcript"
                      enctype="multipart/form-data">
                    <?php } ?>



                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Academic Transcript</label>
                        <div class="col-sm-5">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-info btn-file">
											<span class="fileinput-new">Select file</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" name="file">

							</span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                   style="float: none">&times;</a>
                                <?php if ($academic_transcript) {
                                    ?>
                                    <br/>
                                    <br/>
                                    <a class="btn btn-info" target="_blank"
                                       href="<?php echo base_url() . $academic_transcript ?>">Download
                                        Previous File</a>
                                <?php } ?>
                            </div>
                        </div>
                        <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: PDF/Doc file can be uploaded.</span>
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Save Transcript</button>
                        </div>
                    </div>
                </form>

        </div>

        <HR>

        <div class="panel-body">


            <?php if ($datas == NULL) { ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/medical"
                  enctype="multipart/form-data">
                <?php } else { ?>
                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/medical"
                      enctype="multipart/form-data">
                    <?php } ?>



                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Medical Report</label>
                        <div class="col-sm-5">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-info btn-file">
											<span class="fileinput-new">Select file</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" required name="medical">

							</span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                   style="float: none">&times;</a>
                                <?php if ($medical_report) {
                                    ?>
                                    <br/>
                                    <br/>
                                    <a class="btn btn-info" target="_blank"
                                       href="<?php echo base_url() . $medical_report ?>">Download
                                        Previous File</a>
                                <?php } ?>
                            </div>
                        </div>
                        <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: PDF/Doc file can be uploaded.</span>
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Save Medical Report</button>
                        </div>
                    </div>

                </form>

        </div>

        <HR>

        <div class="panel-body">


            <?php if ($datas == NULL) { ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/sponsor"
                  enctype="multipart/form-data">
                <?php } else { ?>
                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="<?php echo base_url() ?>admin_panel/store_document/<?php echo $client_id ?>/sponsor"
                      enctype="multipart/form-data">
                    <?php } ?>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Upload Sponsor Document</label>
                        <div class="col-sm-5">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-info btn-file">
											<span class="fileinput-new">Select file</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" name="sponsor">

							</span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                   style="float: none">&times;</a>
                                <?php if ($sponsor_document) {
                                    ?>
                                    <br/>
                                    <br/>
                                    <a class="btn btn-info" target="_blank"
                                       href="<?php echo base_url() . $sponsor_document ?>">Download
                                        Previous File</a>
                                <?php } ?>
                            </div>
                        </div>
                        <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: PDF/Doc file can be uploaded.</span>
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Save Sponsor Document</button>
                        </div>
                    </div>

                </form>

        </div>


    </div>
</div>
