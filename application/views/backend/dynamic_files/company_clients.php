<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add Client</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Add Client</a></li>
                <!--                <li><a href="#profile-2" data-toggle="tab">Add Client</a></li>-->
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Client List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <form role="form" class="form-horizontal validate form-groups invoice-add"
                              action="<?php echo base_url(); ?>admin_panel/company_clients/create" method="post"
                              enctype="multipart/form-data">


                            <div class="row">
                                <div class="col-md-12">


                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Client Name</label>

                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="name"
                                                       id="name" data-validate="required"
                                                       data-message-required="value required" value="" autofocus>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Select Client
                                                Type</label>

                                            <div class="col-sm-5">
                                                <select name="client_type" id="client_type" class="form-control">
                                                    <option value="Individual Client">Individual Client</option>
                                                    <option value="Company Client">Company Client</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Clients Phone
                                                Number</label>

                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="phone"
                                                       id="phone" data-validate="required"
                                                       data-message-required="value required" value="" autofocus>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Clients Email</label>

                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="email"
                                                       id="email" data-validate="required"
                                                       data-message-required="value required" value="" autofocus>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Opened Date</label>

                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <input type="text" name="date" class="form-control datepicker"
                                                           data-format="D, dd MM yyyy">

                                                    <div class="input-group-addon">
                                                        <a href="#"><i class="entypo-calendar"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>


                            <div class="row">

                                <!--TITLE FIELD & DESCRIPTION-->

                                <div class="col-md-12">

                                    <div class="panel panel-primary" data-collapsed="0">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <i class="entypo-plus-circled"></i>
                                                Clients Information
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <!-- FORM ENTRY STARTS HERE-->
                                            <div id="entry">
                                                <div class="form-group">
                                                    <label for="field-1" class="col-sm-2 control-label">Data</label>

                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control"
                                                               name="input_title[]" value=""
                                                               placeholder="Title">
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control allowance_amount"
                                                               name="input_description[]" value=""
                                                               placeholder="Description">
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <button type="button" class="btn btn-default"
                                                                onclick="deleteParentElement(this)">
                                                            <i class="entypo-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- FORM ENTRY ENDS HERE-->


                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-8">
                                                    <button type="button"
                                                            class="btn btn-default btn-sm btn-icon icon-left"
                                                            onClick="add_desc_entry()">
                                                        Add Description entry
                                                        <i class="entypo-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <button type="submit" class="btn btn-success" id="submit-button">
                                                    Save Client Information
                                                    <span id="preloader-form"></span>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </form>


                    </div>
                </div>
            </div>


        </div>

    </div>


</div>


<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<script>

    // CREATING BLANK allowance ENTRY
    var blank_desc_entry = '';
    $(document).ready(function () {
        blank_desc_entry = $('#entry').html();
    });

    function add_desc_entry() {
        $("#entry").append(blank_desc_entry);
    }

    // REMOVING  ENTRY
    function deleteParentElement(n) {
        n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
    }


</script>


<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>