<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">


            <div class="panel-body">

                <form role="form" class="form-horizontal validate form-groups-bordered"
                      action="<?php echo base_url(); ?>admin_panel/salary_payment" method="post"
                      enctype="multipart/form-data">


                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Select month</label>

                        <div class="col-sm-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="text" class="form-control datepicker-month" name="date"
                                       value="<?php echo date("m/Y"); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 control-label col-sm-offset-2">
                        <input type="submit" class="btn btn-success" value="Submit" name="submit">
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>


<div style="clear:both;"></div>
<br>
<?php if (isset($employees)): ?>
    <table class="table table-bordered table-striped datatable" id="table-2">
        <thead>
        <tr>
            <th>Emp</th>
            <th>Name</th>
            <th>Salary type</th>
            <th>Basic salary</th>
            <th>Net salary</th>
            <th>Month</th>
            <th>Status</th>
            <th>Action</th>

        </tr>
        </thead>

        <tbody>
        <form action="">

            <?php foreach ($employees as $employee): ?>

                <tr>
                    <td><?php echo $employee['employee_name']; ?></td>
                    <td>
                        <?php echo $this->db->get_where('employee_designation',
                            array('designation_id' => $employee['designation']))->row('designation_name'); ?>
                    </td>
                    <td>
                        <?php $query = $this->db->get_where('salary_template', array('salary_template_id' => $employee['salary_template_id']));
                        if ($query->num_rows() > 0) {
                            echo $query->row()->salary_grades;
                        }

                        ?>
                    </td>
                    <td>
                        <?php $query = $this->db->get_where('salary_template', array('salary_template_id' => $employee['salary_template_id']));
                        if ($query->num_rows() > 0) {
                            echo $query->row()->basic_salary;
                        }

                        ?>

                    </td>
                    <td>
                        <?php $query = $this->db->get_where('salary_template', array('salary_template_id' => $employee['salary_template_id']));
                        if ($query->num_rows() > 0) {
                            echo $query->row()->net_salary;
                        }

                        ?>

                    </td>
                    <td>
                        <?php echo $date;?>
                    </td>
                    <td>
                        <?php

                        $query = $this->db->get_where('salary', array('employee_id'=>$employee['employee_id'],'date' => $date));

                        if ($query->num_rows() > 0) {
                            echo $status = '<div class="label label-success">Paid</div>';
                        } else {
                            echo $status = '<div class="label label-danger">Unpaid</div>';
                        }

                        ?>

                    </td>
                    <td>

                        <?php
                        if ($status == '<div class="label label-danger">Unpaid</div>') {
                            ?>
                            <a href="<?php echo base_url(); ?>admin_panel/make_salary_payment/<?php echo $employee['employee_id']; ?>/<?php echo $date; ?>"
                               class="btn btn-danger btn-sm btn-icon icon-left">
                                <i class="entypo-cancel"></i>
                                Make payment
                            </a>
                            <?php
                        } else {
                            ?>

                            <a href="<?php echo base_url(); ?>admin_panel/salary_payslip/<?php echo $employee['employee_id']; ?>/<?php echo $date; ?>"
                               class="btn btn-danger btn-sm btn-icon icon-left">
                                <i class="entypo-cancel"></i>
                                Pay slip
                            </a>

                            <?php

                        }
                        ?>


                    </td>

                </tr>
            <?php endforeach; ?>
        </form>
        </tbody>
    </table>
<?php endif; ?>

<?php
$this->session->flashdata('message');
?>


<script type="text/javascript">
    jQuery(window).load(function () {
        var $ = jQuery;


        //datepicker only month

        $(".datepicker-month").datepicker({
            format: "mm/yyyy",
            startView: "months",
            minViewMode: "months"
        });

    });
</script>