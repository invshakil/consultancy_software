<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env" style="padding: 25px;">

            <!-- logo -->
            <div class="logo">
                <a href="<?php echo base_url() ?>" target="_blank">
                    <h1 style="font-size: 20px;font-weight: bold;
                    font-family: 'Bookman Old Style';margin: 0;
                    color: white;">
                        Consultancy</h1>
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon">
                    <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- APPLICATION -->
            <li>
                <a href="<?php echo base_url(); ?>admin_panel">
                    <i class="fa fa-external-link fa-2x"></i>
                    <span>Switch to Application</span>
                </a>
            </li>
            <br>
            <!-- Slider Image -->
            <li class="<?php if ($page_name == 'Slider Image') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/index">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Slider Image</span>
                </a>
            </li>
            <!-- MESSAGE from Organization -->
            <li class="<?php if ($page_name == 'Message From Organization') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/message_from_organization">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Message From Organization</span>
                </a>
            </li>
            <!-- MESSAGE from CEO -->
            <li class="<?php if ($page_name == 'Message From CEO') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/message_from_ceo">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Message From CEO</span>
                </a>
            </li>
            <!-- Why Chose Us -->
            <li class="<?php if ($page_name == 'Why Choose Us?') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/why_choose_us">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Why Choose Us</span>
                </a>
            </li>
            <!-- Testimonials -->
            <li class="<?php if ($page_name == 'Testimonials') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/testimonials">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Testimonials</span>
                </a>
            </li>
            <!-- About Us -->
            <li class="<?php if ($page_name == 'About Us') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/about_us">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>About Us</span>
                </a>
            </li>
            <!-- About Us -->
            <li class="<?php if ($page_name == 'Countries' || $page_name == 'Information By Country') echo 'opened active'; ?> ">
                <a href="#">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Study Abroad</span>
                </a>
                <ul>
                    <!-- Country List -->
                    <li class="<?php if ($page_name == 'Countries') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>frontend/countries">
                            <i class="fa fa-tag fa-2x"></i>
                            <span>Manage Country</span>
                        </a>
                    </li>
                    <!-- Country List -->
                    <li class="<?php if ($page_name == 'Information By Country') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>frontend/information_by_country">
                            <i class="fa fa-tag fa-2x"></i>
                            <span>Information By Country</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Our Prices -->
            <li class="<?php if ($page_name == 'Our Prices') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/our_prices">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Our Prices</span>
                </a>
            </li>
            <!-- Our Team Members -->
            <li class="<?php if ($page_name == 'Employees Images') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/our_team_members">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Our Team Members</span>
                </a>
            </li>
            <!-- Subscribed Members -->
            <li class="<?php if ($page_name == 'Subscriber Members') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/subscribed_members">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Subscriber Members</span>
                </a>
            </li>
            <!-- Subscribed Members -->
            <li class="<?php if ($page_name == 'Mail Settings') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/mail_settings">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Mail Setup</span>
                </a>
            </li>
            <!-- Online Application -->
            <li class="<?php if ($page_name == 'Online Application') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>frontend/online_application">
                    <i class="fa fa-tag fa-2x"></i>
                    <span>Online Application</span>
                </a>
            </li>



        </ul>

    </div>

</div>