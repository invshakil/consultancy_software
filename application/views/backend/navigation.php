<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env" style="padding: 25px;">

            <!-- logo -->
            <div class="logo">
                <a href="<?php echo base_url() ?>" target="_blank">
                    <h1 style="font-size: 20px;font-weight: bold;
                    font-family: 'Bookman Old Style';margin: 0;
                    color: white;">
                        Consultancy</h1>
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon">
                    <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- APPLICATION -->
            <li>
                <a href="<?php echo base_url(); ?>frontend">
                    <i class="fa fa-external-link fa-2x"></i>
                    <span>Switch to Website</span>
                </a>
            </li>
            <br>

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'Admin Dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>admin_panel">
                    <i class="fa fa-home fa-2x"></i>
                    <span>Dashboard</span>
                </a>
            </li>


            <?php
            global $admin_module;
            if ($admin_module == '') {
                $admin_module = 1;
            }

            if ($admin_module == 1) {
                ?>


                <!-- SYSTEM USER -->
                <li class="<?php if ($page_name == 'SYSTEM USERS' || $page_name == 'MODULE LIST'
                    || $page_name == 'ACCESS CONTROL'
                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-users fa-2x"></i>
                        <span>System Users</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'SYSTEM USERS') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/system_users">
                                <i class="fa fa-user fa-2x"></i>
                                <span>User Management</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'MODULE LIST') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/module_list">
                                <i class="fa fa-gear fa-2x"></i>
                                <span>Module List</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'ACCESS CONTROL') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/manage_access_control">
                                <i class="fa fa-gears fa-2x"></i>
                                <span>Access Control</span>
                            </a>
                        </li>
                    </ul>
                </li>

            <?php } ?>

            <?php
            global $client_module;
            if ($client_module == 1) {
                ?>

                <!-- AGENTS -->

                <li class="<?php if ($page_name == 'AGENTS') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>admin_panel/agents">
                        <i class="fa fa-clipboard fa-2x"></i>
                        <span>Agents</span>
                    </a>
                </li>


                <!-- CLIENT -->
                <li class="<?php if (($page_name == 'CLIENT TYPE') ||
                    ($page_name == 'CLIENT MANAGEMENT')

                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-clipboard fa-2x"></i>
                        <span>Customer Clients</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'CLIENT TYPE') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/client_type">
                                <i class="fa fa-clipboard fa-2x"></i>
                                <span>Client Type</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'CLIENT MANAGEMENT') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/clients">
                                <i class="fa fa-user-md fa-2x"></i>
                                <span>Client Management</span>
                            </a>
                        </li>
                    </ul>
                </li>



                <!-- CLIENT -->
                <li class="<?php if ( ($page_name == 'COMPANY CLIENT') ||
                    ($page_name == 'CLIENT LIST')

                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-clipboard fa-2x"></i>
                        <span>Company Clients</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'COMPANY CLIENT') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/company_clients">
                                <i class="fa fa-user-md fa-2x"></i>
                                <span>Add Client</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'CLIENT LIST') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/client_list">
                                <i class="fa fa-user-md fa-2x"></i>
                                <span>Client List</span>
                            </a>
                        </li>
                    </ul>
                </li>






                <!-- VISA -->
                <li class="<?php if ($page_name == 'VISA TYPE' || $page_name == 'DOCUMENT PROCESS'
                    || $page_name == 'DOCUMENT CHECK'
                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-clipboard"></i>
                        <span>Document & Visa</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'DOCUMENT PROCESS') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/document_process">
                                <i class="fa fa-clipboard fa-2x"></i>
                                <span>Document Process</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'DOCUMENT CHECK') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/document_check">
                                <i class="fa fa-clipboard fa-2x"></i>
                                <span>Document Check</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'VISA TYPE') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/visa_type">
                                <i class="fa fa-clipboard fa-2x"></i>
                                <span>VISA Type</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <!-- FINANCE -->
                <li class="<?php if ($page_name == 'CASH MEMO TYPE' || $page_name == 'CLIENT CONTRACT'
                    || $page_name == 'CASH MEMO'
                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-check-circle fa-2x"></i>
                        <span>Contract & Memos</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'CLIENT CONTRACT') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/client_contract">
                                <i class="fa fa-pencil-square fa-2x"></i>
                                <span>Client Contract</span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'CASH MEMO TYPE') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/cash_memo_type">
                                <i class="fa fa-folder-o fa-2x"></i>
                                <span>Cash Memo Type</span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'CASH MEMO') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/cash_memo">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Cash Memo</span>
                            </a>
                        </li>
                    </ul>
                </li>

            <?php } ?>



            <?php
            global $accounting_module;

            if ($accounting_module == 1) { ?>

                <!-- ACCOUNTING -->
                <li class="<?php if ($page_name == 'INCOME CATEGORY' || $page_name == 'INCOME RECORD'
                    || $page_name == 'INCOME REPORT' || $page_name == 'EXPENSE CATEGORY' || $page_name == 'EXPENSE RECORD'
                    || $page_name == 'EXPENSE REPORT' || $page_name == 'TRANSACTION REPORT'
                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-arrow-circle-o-down fa-2x"></i>
                        <span>Accounting</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'INCOME CATEGORY' || $page_name == 'INCOME RECORD'
                            || $page_name == 'INCOME REPORT'
                        ) echo 'opened active'; ?> ">
                            <a href="#">
                                <i class="fa fa-check-circle fa-2x"></i>
                                <span>Income</span>
                            </a>
                            <ul>
                                <li class="<?php if ($page_name == 'INCOME CATEGORY') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>admin_panel/income_category">
                                        <i class="fa fa-folder-o fa-2x"></i>
                                        <span>Income Type</span>
                                    </a>
                                </li>
                                <li class="<?php if ($page_name == 'INCOME RECORD') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>admin_panel/income_record">
                                        <i class="fa fa-shopping-cart fa-2x"></i>
                                        <span>Income Record</span>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class="<?php if ($page_name == 'EXPENSE CATEGORY' || $page_name == 'EXPENSE RECORD'
                            || $page_name == 'EXPENSE REPORT'
                        ) echo 'opened active'; ?> ">
                            <a href="#">
                                <i class="fa fa-check-circle fa-2x"></i>
                                <span>Expense</span>
                            </a>
                            <ul>
                                <li class="<?php if ($page_name == 'EXPENSE CATEGORY') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>admin_panel/expense_category">
                                        <i class="fa fa-folder-o fa-2x"></i>
                                        <span>Expense Type</span>
                                    </a>
                                </li>
                                <li class="<?php if ($page_name == 'EXPENSE RECORD') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>admin_panel/expense_record">
                                        <i class="fa fa-shopping-cart fa-2x"></i>
                                        <span>Expense Record</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php if ($page_name == 'TRANSACTION REPORT') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/transaction_report">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Transaction Report</span>
                            </a>
                        </li>

                    </ul>
                </li>

            <?php } ?>

            <?php
            global $employee_module;
            if ($employee_module == 1) { ?>

                <!-- Employee Management -->
                <li class="<?php if ($page_name == 'DESIGNATION' || $page_name == 'EMPLOYEES'
                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-check-circle fa-2x"></i>
                        <span>Employee Management</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'DESIGNATION') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/employee_designation">
                                <i class="fa fa-pencil-square fa-2x"></i>
                                <span>Designation</span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'EMPLOYEES') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/employees">
                                <i class="fa fa-pencil-square fa-2x"></i>
                                <span>Employees</span>
                            </a>
                        </li>


                    </ul>
                </li>

            <?php } ?>

            <?php
            global $attendance_module;
            if ($attendance_module == 1) { ?>

                <!-- Attendance Management -->
                <li class="<?php if ($page_name == 'EMPLOYEES ATTENDANCE' || $page_name == 'ATTENDANCE REPORT'
                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-check-circle fa-2x"></i>
                        <span>Attendance Management</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'EMPLOYEES ATTENDANCE') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/employee_attendance/">
                                <i class="fa fa-folder-o fa-2x"></i>
                                <span>Record Attendance</span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'ATTENDANCE REPORT') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/attendance_report">
                                <i class="fa fa-folder-o fa-2x"></i>
                                <span>Attendance Report</span>
                            </a>
                        </li>


                    </ul>
                </li>
            <?php } ?>


            <?php
            global $salary_module;
            if ($salary_module) { ?>

                <!-- Payroll -->
                <li class="<?php if ($page_name == 'SALARY GRADES' || $page_name == 'MANAGE SALARY'
                    || $page_name == 'EMPLOYEE SALARY LIST' || $page_name == 'SALARY PAYMENT'
                    || $page_name == 'PAYMENT SLIP' || $page_name == 'PAYROLL SUMMARY'
                ) echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="fa fa-trash-o fa-2x"></i>
                        <span>Payroll</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'SALARY GRADES') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/salary_grades">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Salary Grades</span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'MANAGE SALARY') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/manage_salary">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Manage Salary</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'EMPLOYEE SALARY LIST') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/employee_salary_list">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Employee Salary List</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'SALARY PAYMENT') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/salary_payment">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Salary Payment</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'PAYMENT SLIP') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/salary_payslip">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Payment Slip</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'PAYROLL SUMMARY') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>admin_panel/payroll_summary">
                                <i class="fa fa-shopping-cart fa-2x"></i>
                                <span>Payroll Summary</span>
                            </a>
                        </li>

                    </ul>
                </li>

            <?php } ?>

            <?php


            if ($admin_module == 1) { ?>

                <li class="<?php if ($page_name == 'System Settings') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>admin_panel/system_settings">
                        <i class="fa fa-gears"></i>
                        <span>System Settings</span>
                    </a>
                </li>
                <?php
            } ?>


        </ul>

    </div>

</div>