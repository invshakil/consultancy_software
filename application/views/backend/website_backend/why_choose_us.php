<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Why Choose Us Icon & Message</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Add/Manage Icon & Message</a></li>

            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add/Manage Icon & Message</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">
                        <div class="panel-body col-md-12">
                            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                  action="<?php echo base_url() ?>frontend/why_choose_us/create"
                                  enctype="multipart/form-data">


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Title</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="title" class="form-control" placeholder="Enter title">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Message</label>

                                    <div class="col-sm-5">
                                        <textarea type="text" name="message" class="form-control" id="field-1"
                                                  rows="5" placeholder="Enter description"></textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-success">Add Info</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <hr/>
                        <div class="panel-body col-md-12 table-responsive">
                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th class="center">SL</th>
                                    <th>Title</th>
                                    <th>Message</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $info = $this->db->order_by('desc_id','desc')->get('why_choose_us')->result();
                                $i = 1;
                                foreach ($info as $row) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $row->title; ?></td>
                                        <td><?php echo $row->message; ?></td>

                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#"
                                                           onclick="showAjaxModal('<?php echo base_url(); ?>w_modal/popup/modal_edit_why_choose_us/<?php echo $row->desc_id; ?>');">
                                                            <i class="entypo-pencil"></i>
                                                            Edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#"
                                                           onclick="confirm_modal('<?php echo base_url(); ?>frontend/why_choose_us/delete/<?php echo $row->desc_id; ?>');">
                                                            <i class="entypo-trash"></i>
                                                            Delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>


</div>
