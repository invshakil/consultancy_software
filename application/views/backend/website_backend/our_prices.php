<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">About Us Text</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">

            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>frontend/our_prices/do_update"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-ta" class="col-sm-2 control-label">Our Course Fee Table</label>

                    <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'courses'))->row(); ?>
                    <div class="col-sm-10">
                        <textarea name="courses" id="editor1"
                                  class="form-control ckeditor"><?php echo $msg->description; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-2 control-label">Our Service Fee Table</label>

                    <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'service_charges'))->row(); ?>
                    <div class="col-sm-10">
                        <textarea name="service_charges" id="editor2"
                                  class="form-control ckeditor"><?php echo $msg->description; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-6 col-sm-5">
                        <button type="submit" class="btn btn-success">Save Tables</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>


<script src="<?= base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?= base_url() ?>assets/js/ckeditor/adapters/jquery.js"></script>

<script>
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
    CKEDITOR.config.width = "100%";
    CKEDITOR.config.height = "300px"
</script>