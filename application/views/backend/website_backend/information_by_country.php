<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Testimonial</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Testimonial List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Testimonial</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Testimonial List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th width="5%">Country</th>
                                <th>Why Study in?</th>
                                <th>Required Documents</th>
                                <th>Application Procedure</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->order_by('info_id','desc')->get('information_by_country')->result();
                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">

                                    <td>
                                        <?php
                                        $name = $this->db->get_where('countries_list',array('country_id' => $row->country_id) )->row('country_name');
                                        echo str_replace('-',' ',$name);
                                        ?>
                                    </td>
                                    <td><?php echo substr(strip_tags($row->why_study), 0, 400).'.....' ; ?></td>
                                    <td><?php echo substr(strip_tags($row->required_documents), 0, 400).'.....'; ?></td>
                                    <td><?php echo substr(strip_tags($row->application_procedure),0,400).'......';?></td>

                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>w_modal/popup/modal_edit_info_by_country/<?php echo $row->info_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>frontend/information_by_country/delete/<?php echo $row->info_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Information</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>frontend/information_by_country/create"
                              enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Select Country</label>

                                <div class="col-sm-5">
                                    <select name="country_id" class="form-control">
                                        <?php $countries = $this->db->get_where('countries_list', array('status' => 1))->result();
                                        foreach ($countries as $country) { ?>
                                            <option
                                                value="<?php echo $country->country_id ?>"><?php echo str_replace('-', ' ', $country->country_name) ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Why Study In This Country?</label>

                                <div class="col-sm-9">
                                    <textarea type="text" name="why_study" class="form-control ckeditor" value=""
                                              placeholder="Why Study in this country Message"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Required Documents For This Country</label>

                                <div class="col-sm-9">
                                    <textarea type="text" name="required_documents" class="form-control ckeditor" value=""
                                              placeholder="Enter Required Documents For This Country Message"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Application Procedure</label>

                                <div class="col-sm-9">
                                    <textarea type="text" name="application_procedure" class="form-control ckeditor" value=""
                                              placeholder="Enter Application Procedure Message"></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Info</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>


<script src="<?= base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?= base_url() ?>assets/js/ckeditor/adapters/jquery.js"></script>

<script>
    CKEDITOR.replace('editor1');
    CKEDITOR.config.width="100%";
    CKEDITOR.config.height="300"
</script>