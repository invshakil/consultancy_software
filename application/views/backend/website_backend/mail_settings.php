<div class="panel panel-dark" data-collapsed="0">

    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">System Mail Settings (Contact Software Provider For This Settings)</div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                    class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <!-- panel body -->
    <div class="panel-body">

        <form role="form" class="form-horizontal form-groups-bordered" method="post"
              action="<?php echo base_url() ?>frontend/mail_settings/do_update" enctype="multipart/form-data">

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Mail Protocol</label>

                <?php $mail_protocol = $this->db->where('info_type', 'mail_protocol')
                    ->get('system_settings')->row('description'); ?>
                <div class="col-sm-5">
                    <input type="text" name="mail_protocol" class="form-control" id="field-1"
                           value="<?php echo $mail_protocol; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Mail Path</label>

                <?php $mail_path = $this->db->where('info_type', 'mail_path')
                    ->get('system_settings')->row('description'); ?>
                <div class="col-sm-5">
                    <input type="text" name="mail_path" class="form-control" id="field-1"
                           value="<?php echo $mail_path; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Mail Host Address</label>

                <?php $mail_host = $this->db->where('info_type', 'mail_host')
                    ->get('system_settings')->row('description'); ?>
                <div class="col-sm-5">
                    <input type="text" name="mail_host" class="form-control" id="field-1"
                           value="<?php echo $mail_host; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Mail Host Port</label>

                <?php $mail_host_port = $this->db->where('info_type', 'mail_host_port')
                    ->get('system_settings')->row('description'); ?>
                <div class="col-sm-5">
                    <input type="text" name="mail_host_port" class="form-control" id="field-1"
                           value="<?php echo $mail_host_port; ?>">
                </div>
            </div>


            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">System Email Address</label>

                <?php $settings_email = $this->db->where('info_type', 'settings_email')
                    ->get('system_settings')->row('description'); ?>
                <div class="col-sm-5">
                    <input type="text" name="settings_email" class="form-control" id="field-1"
                           value="<?php echo $settings_email; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">System Email Password</label>

                <?php $settings_email_password = $this->db->where('info_type', 'settings_email_password')
                    ->get('system_settings')->row('description'); ?>
                <div class="col-sm-5">
                    <input type="password" name="settings_email_password" class="form-control" id="field-1"
                           value="<?php echo $settings_email_password; ?>">
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-success">Save Mail Settings Info</button>
                </div>
            </div>
        </form>

    </div>
</div>