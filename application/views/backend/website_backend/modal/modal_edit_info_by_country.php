<?php
$edit_data = $this->db->get_where('information_by_country', array('info_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>frontend/information_by_country/do_update/<?php echo $row['info_id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Country</label>

                    <div class="col-sm-9">
                        <select name="country_id" class="form-control">
                           <?php $countries = $this->db->get_where('countries_list',array('status'=>1))->result();
                           foreach ($countries as $country) {?>
                            <option value="<?php echo $country->country_id?>" <?php if ($row['country_id'] == $country->country_id) echo 'selected'; ?>
                            ><?php echo str_replace('-',' ',$country->country_name);?>
                            </option>
                           <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Why Study in This Country</label>

                    <div class="col-sm-9">
                        <textarea type="text" name="why_study" class="form-control ckeditor"
                               placeholder="Enter Name"><?php echo  $row['why_study'] ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Required Documents For This Country</label>

                    <div class="col-sm-9">
                                    <textarea type="text" name="required_documents" class="form-control ckeditor" value=""
                                              placeholder="Enter Required Documents For This Country Message"><?php echo  $row['required_documents'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Application Procedure</label>

                    <div class="col-sm-9">
                                    <textarea type="text" name="application_procedure" class="form-control ckeditor" value=""
                                              placeholder="Enter Application Procedure Message"><?php echo  $row['application_procedure'] ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>


<script src="<?= base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?= base_url() ?>assets/js/ckeditor/adapters/jquery.js"></script>

<script>
    CKEDITOR.replace('editor1');
    CKEDITOR.config.width="100%";
    CKEDITOR.config.height="300"
</script>