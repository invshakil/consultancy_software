<?php
$edit_data = $this->db->get_where('why_choose_us', array('desc_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>frontend/why_choose_us/do_update/<?php echo $row['desc_id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Title</label>

                    <div class="col-sm-5">
                        <input type="text" name="title" class="form-control"
                               value="<?php echo $row['title'] ?>"
                               id="field-1" placeholder="Enter Title">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Description</label>

                    <div class="col-sm-5">
                        <textarea type="text" name="message" class="form-control"
                               value=""
                               id="field-1" placeholder="Enter Message"><?php echo $row['message'] ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>