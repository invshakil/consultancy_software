<?php
$edit_data = $this->db->get_where('emp_images', array('image_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>frontend/our_team_members/do_update/<?php echo $row['image_id'] ?>"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Employee</label>

                    <div class="col-sm-5">
                        <select name="emp_id" class="form-control">
                            <?php
                            $emps = $this->db->get_where('employees', array('employee_status', 1))->result();
                            foreach ($emps as $emp) { ?>
                                <option
                                    value="<?php echo $emp->employee_id; ?>" <?php if ($emp->employee_id == $row['emp_id']) echo 'selected';?>>
                                    <?php echo $emp->employee_name; ?></option>
                            <?php } ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Employee Image</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php if ($row['emp_image']) { ?>
                                    <img src="<?php echo base_url() . $row['emp_image'] ?>" alt="...">
                                <?php } else { ?>
                                    <img src="http://placehold.it/500x500" alt="...">
                                <?php } ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image_upload" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>