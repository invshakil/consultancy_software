<?php
$edit_data = $this->db->get_where('countries_list', array('country_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>frontend/countries/do_update/<?php echo $row['country_id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Country Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="country_name" class="form-control" value="<?php echo str_replace('-', ' ', $row['country_name']) ?>"
                               id="field-1" placeholder="Enter Name">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Visibility Status</label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control">
                            <option value="1" <?php if ($row['status'] == 1) echo 'selected'; ?>
                            >Public
                            </option>
                            <option value="2"<?php if ($row['status'] == 2) echo 'selected'; ?>
                            >Private
                            </option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>