<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Testimonial</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Testimonial List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Testimonial</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Testimonial List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body table-responsive">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th class="center">SL</th>
                                <th data-hide="phone">Image</th>
                                <th width="10%">Name</th>
                                <th>Message</th>
                                <th>Status</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->order_by('testimonial_id','desc')->get('testimonials')->result();
                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->testimonial_id; ?></td>
                                    <td class="center"><img src="<?php echo base_url() . $row->image; ?>" width="60px"
                                                            class="img-thumbnail"></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->message; ?></td>
                                    <td class="center"><?php if ($row->status == 1) {
                                            echo '<div class="label label-success">Visible</div>';
                                        } else {
                                            echo '<div class="label label-danger">Private</div>';
                                        } ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>w_modal/popup/modal_edit_testimonials/<?php echo $row->testimonial_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>frontend/testimonials/delete/<?php echo $row->testimonial_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Testimonial</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>frontend/testimonials/create"
                              enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Client Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="name" class="form-control" id="name" value=""
                                           placeholder="Enter Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Client Message</label>

                                <div class="col-sm-5">
                                    <textarea type="text" name="message" class="form-control" value=""
                                              placeholder="Enter Testimonial Message [max 255 character]"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Image Upload</label>

                                <div class="col-sm-5">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px;"
                                             data-trigger="fileinput">
                                            <img src="http://placehold.it/600x400" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px"></div>
                                        <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image_upload" accept="image/*">
											</span>
                                            <a href="#" class="btn btn-orange fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Visibility Status</label>

                                <div class="col-sm-5">
                                    <select name="status" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="2">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Testimonial Info</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>