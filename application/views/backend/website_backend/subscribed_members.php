<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Our Subscribed Member List</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body table-responsive">

            <table class="table table-bordered datatable" id="table-1">
                <thead>
                <tr>
                    <th class="center">SL</th>
                    <th width="10%">Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Send Mail</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                <?php $info = $this->db->order_by('subscribe_id','desc')->get('subscribers')->result();
                foreach ($info as $row) {
                    ?>
                    <tr class="odd gradeX">
                        <td><?php echo $row->subscribe_id; ?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo $row->email; ?></td>
                        <td class="center"><?php if ($row->status == 1) {
                                echo '<div class="label label-success">Subscribed</div>';
                            } else {
                                echo '<div class="label label-danger">Unsubscribed</div>';
                            } ?></td>
                        <td>

                            <a href="#" data-email="<?php echo $row->email; ?>"
                               data-toggle="modal" data-target="#MailModal" class="btn btn-primary btn-icon">
                                Send
                                <i class="entypo-mail"></i>
                            </a>

                        </td>
                        <td>

                            <a href="#" class="btn btn-danger btn-sm"
                               onclick="confirm_modal('<?php echo base_url(); ?>frontend/testimonials/delete/<?php echo $row->subscribe_id; ?>');">
                                <i class="entypo-trash"></i>
                                Delete
                            </a>
                        </td>

                    </tr>
                <?php } ?>
                </tbody>
            </table>


        </div>
    </div>

</div>

<div class="modal fade" id="MailModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="gallery-image-edit-env">
                <img src="" id="image" class="img-responsive"/>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <form action="<?= base_url() ?>frontend/send_mail" method="post">
                <div class="modal-body">

                    <div class="mail-compose">

                        <?php $settings_email = $this->db->where('info_type','settings_email')
                            ->get('system_settings')->row('description');?>

                        <div class="form-group">
                            <label for="to">From:</label>
                            <input type="text" name="from_mail" value="<?php echo $settings_email;?>" class="form-control" id="from" tabindex="1" disabled/>

                        </div>

                        <div class="form-group">
                            <label for="to">To:</label>
                            <input type="text" name="to_mail" class="form-control" id="to" tabindex="1"/>

                        </div>


                        <div class="form-group">
                            <label for="subject">Subject:</label>
                            <input type="text" name="subject" class="form-control" id="subject" tabindex="1"/>
                        </div>


                        <div class="compose-message-editor">
                                <textarea name="mail_body" class="form-control ckeditor"></textarea>
                        </div>


                    </div>


                </div>


                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-icon">
                        <i class="entypo-check"></i>
                        Apply Changes
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $ = jQuery;
    $('#MailModal').on('show.bs.modal', function (e) {
        var email = $(e.relatedTarget).data('email');
        console.log(email);
        $('#to').val(email);
    });

</script>


<script src="<?= base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?= base_url() ?>assets/js/ckeditor/adapters/jquery.js"></script>