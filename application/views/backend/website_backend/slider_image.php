<div class="gallery-env">
    <?php
    $this->session->flashdata('message');
    ?>
    <div class="row">

        <div class="col-sm-12">

            <h3>
                Image Gallery
                &nbsp;
                <a href="#" onclick="jQuery('#album-cover-options').modal('show');"
                   class="btn btn-success btn-icon icon-left pull-right">
                    <i class="fa fa-picture-o"></i>
                    Add Image
                </a>
            </h3>

            <hr/>

        </div>

    </div>

    <div class="row">

        <?php foreach ($all_images as $image) { ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" style="min-height: 380px!important;">

                <article class="album">

                    <header>

                        <a href="#" data-id="<?php echo $image->image_id; ?>"
                           data-image="<?php echo $image->image; ?>"
                           data-caption="<?php echo $image->caption; ?>"
                           data-desc="<?php echo strip_tags($image->description); ?>"
                           data-type="<?php echo $image->image_type; ?>"
                           data-status="<?php echo $image->visibility_status; ?>"
                           data-toggle="modal" data-target="#updateModal">
                            <img src="<?php echo base_url() . $image->image ?>"/>
                        </a>

                        <a href="#"
                           data-id="<?php echo $image->image_id; ?>"
                           data-image="<?php echo $image->image; ?>"
                           data-caption="<?php echo $image->caption; ?>"
                           data-desc="<?php echo strip_tags($image->description); ?>"
                           data-type="<?php echo $image->image_type; ?>"
                           data-status="<?php echo $image->visibility_status; ?>"
                           data-toggle="modal" data-target="#updateModal"
                           class="album-options">
                            <i class="fa fa-edit"></i>
                            Edit Image Info
                        </a>
                    </header>

                    <section class="album-info" style="min-height: 100px!important;">
                        <h3><?php echo $image->caption ?></h3>

                        <p>Image Type: <?php if ($image->image_type == 1) {
                                echo 'Slider Image';
                            } else {
                                echo 'Gallery Image';
                            } ?></p>
                    </section>

                    <footer>

                        <div class="pull-left">
                            <a href="#"
                               data-id="<?php echo $image->image_id; ?>"
                               data-image="<?php echo $image->image; ?>"
                               data-caption="<?php echo $image->caption; ?>"
                               data-desc="<?php echo strip_tags($image->description); ?>"
                               data-type="<?php echo $image->image_type; ?>"
                               data-status="<?php echo $image->visibility_status; ?>"
                               data-toggle="modal" data-target="#updateModal"
                               class="btn btn-primary btn-sm">
                                <i class="fa fa-edit"></i>
                                Edit Image Info
                            </a>

                        </div>

                        <div class="pull-right">
                            <a href="#"
                               onclick="confirm_modal('<?php echo base_url(); ?>frontend/delete_image_info/<?php echo $image->image_id; ?>');"
                               class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                                Delete This Image
                            </a>
                        </div>

                    </footer>

                </article>

            </div>

        <?php } ?>

        <ul class="pagination pull-right">
            <?php foreach ($links as $link) {
                echo $link;
            } ?>
        </ul>


    </div>

</div>

<!--ADD IMAGE MODAL-->

<div class="modal fade custom-width" id="album-cover-options">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add: <strong>Slider/Gallery Image</strong></h4>
            </div>

            <form action="<?php echo base_url() ?>frontend/store_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-5">

                            <div class="row">
                                <div class="col-md-12">

                                    <h4 class="margin-top-none">Upload Image</h4>

                                    <div class="croppable-image">

                                        <div class="fileinput fileinput-new image-responsive" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail"
                                                 data-trigger="fileinput">
                                                <img src="http://placehold.it/1400x600" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                 style="max-width: 200px; max-height: 150px"></div>
                                            <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>

												<input type="file" name="image_upload" accept="image/*">
											</span>
                                                <a href="#" class="btn btn-orange fileinput-exists"
                                                   data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <br>
                            <span class="alert alert-info">For Slider: 1400x600 Pixel</span>
                            <span class="alert alert-warning">For Gallery: Any Dimension</span>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Visibility_status</label>

                                        <select name="visibility_status" class="form-control">
                                            <option value="1">Public</option>
                                            <option value="2">Private</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-7">

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Select Image Type</label>

                                        <select name="image_type" class="form-control" id="image_type">
                                            <option value="1">Slider Image</option>
                                            <option value="2">Gallery Image</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Image Caption</label>

                                        <textarea type="text" name="caption" class="form-control" id="field-1"
                                                  placeholder="Enter Image caption"></textarea>
                                    </div>

                                </div>
                            </div>

                            <div class="row" id="description" style="display:block;">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Description</label>

                                        <textarea class="form-control ckeditor" name="description"
                                                  placeholder="Enter album description"></textarea>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-icon">
                        <i class="entypo-check"></i>
                        Save Image Information
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $ = jQuery;
    $(document).ready(function () {
        $('#image_type').on('change', function () {
            var type = $(this).val();

            if (type == 1) {
                $('#description').css('display', 'block');
            }
            else {
                $('#description').css('display', 'none');
            }

        });
    });
</script>

<!-- Edit Image Settings Modal -->
<div class="modal fade" id="updateModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="gallery-image-edit-env">
                <img src="" id="image" class="img-responsive"/>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <form action="<?= base_url() ?>frontend/update_image" method="post">
                <div class="modal-body">

                    <input type="hidden" name="image_id" value="" id="id">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="field-1" class="control-label">Caption</label>

                                <input type="text" class="form-control" id="caption" placeholder="Enter image caption">
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="field-1" class="control-label">Select Image Type</label>

                                <select name="image_type" class="form-control" id="type">
                                    <option value="1">Slider Image</option>
                                    <option value="2">Gallery Image</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="row" id="updateDesc" style="display:none;">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="field-1" class="control-label">Description</label>

                                <textarea class="form-control ckeditor" name="description" id="desc"
                                          placeholder="Enter image description"
                                          style="min-height: 80px;"></textarea>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="field-1" class="control-label">Visibility_status</label>

                                <select name="visibility_status" class="form-control" id="status">
                                    <option value="1">Public</option>
                                    <option value="2">Private</option>
                                </select>
                            </div>

                        </div>
                    </div>


                </div>


                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-icon">
                        <i class="entypo-check"></i>
                        Apply Changes
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#updateModal').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        var image = $(e.relatedTarget).data('image');
        var desc = $(e.relatedTarget).data('desc');
        var caption = $(e.relatedTarget).data('caption');
        var type = $(e.relatedTarget).data('type');
        var status = $(e.relatedTarget).data('status');

        console.log(desc);

        function nl2br (str, is_xhtml) {
            desc = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ desc +'$2');
        }

        desc = nl2br(desc);

        $('#id').val(id);
        $('#caption').val(caption);
        $('textarea#desc').val(desc);
        $('select#type').val(type);
        $('select#status').val(status);

        $('img#image').attr('src', '<?php echo base_url() ?>' + image);

        if (type == 1) {
            $('#updateDesc').css('display', 'block');
        }
        else {
            $('#updateDesc').css('display', 'none');
        }

        $('#type').on('change', function () {
            var type = $(this).val();
            console.log(type);

            if (type == 1) {
                $('#updateDesc').css('display', 'block');
            }
            else {
                $('#updateDesc').css('display', 'none');
            }

        });
    });

</script>

<script src="<?= base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?= base_url() ?>assets/js/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".gallery-env").on("click", ".image-thumb .image-options a.edit", function (ev) {
            ev.preventDefault();

            // This will open sample modal
            $("#album-image-options").modal('show');

        });


    });
</script>